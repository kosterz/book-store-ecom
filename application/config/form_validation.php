<?php

$config = array(
    'staffLogin' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required'
        ),
        array(
            'field' => 'passcode',
            'label' => 'Passcode',
            'rules' => 'required'
        )
    ),
    'staffsignup' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|is_unique[tbl_staff.username]|alpha_numeric|min_length[4]'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|min_length[8]|alha_numeric'
        ),
        array(
            'field' => 'confirm_password',
            'label' => 'ช่องยืนยันรหัส',
            'rules' => 'required'
        ),
        array(
            'field' => 'passcode',
            'label' => 'ช่องรหัส 4 ตัว',
            'rules' => 'required|exact_length[4]|numeric'
        ),
        array(
            'field' => 'confirm_passcode',
            'label' => 'ช่องยืนยันรหัส 4 ตัว',
            'rules' => 'required'
        )
    ),
    'add_staff' => array(
        array(
            'field' => 'name',
            'label' => 'ชื่อ',
            'rules' => 'required'
        ),
        array(
            'field' => 'surname',
            'label' => 'นามสกุล',
            'rules' => 'required'
        ),
        array(
            'field' => 'address',
            'label' => 'ที่อยู่',
            'rules' => 'required'
        ),
        array(
            'field' => 'personal_id',
            'label' => 'รหัสบัตรประชาชน',
            'rules' => 'required|is_unique[tbl_staff.personal_id]|numeric'
        ),
        array(
            'field' => 'telNo',
            'label' => 'เบอร์โทรศัพท์',
            'rules' => 'required|numeric '
        ),
        array(
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'required|valid_email'
        )
    ),
    'staffChangePWD' => array(
        array(
            'field' => 'password',
            'label' => 'รหัสผ่าน',
            'rules' => 'required'
        ),
        array(
            'field' => 'conf_pass',
            'label' => 'ช่องยืนยันรหัส',
            'rules' => 'required'
        ),
        array(
            'field' => 'passcode',
            'label' => 'รหัสยืนยัน 4 ตัว',
            'rules' => 'required'
        ),
        array(
            'field' => 'conf_passcode',
            'label' => 'ช่องยืนยันรหัส 4 ตัว',
            'rules' => 'required'
        )
    ),
    'bookForm' => array(
        array(
            'field' => 'title',
            'label' => 'ชื่อหนังสือ',
            'rules' => 'required'
        ),
        array(
            'field' => 'isbn',
            'label' => 'ISBN',
            'rules' => 'required|integer|min_length[10]|max_length[13]|is_unique[tbl_book.isbn]'
        ),
        array(
            'field' => 'category',
            'label' => 'หมวดหมู่',
            'rules' => 'required'
        ),
        array(
            'field' => 'author[]',
            'label' => 'ชื่อผู้แต่ง',
            'rules' => 'required'
        ),
        array(
            'field' => 'detail',
            'label' => 'รายละเอียด',
            'rules' => 'required'
        ),
        array(
            'field' => 'page',
            'label' => 'จำนวนหน้า',
            'rules' => 'required|integer|greater_than[0]'
        ),
        array(
            'field' => 'price',
            'label' => 'ราคา',
            'rules' => 'required|integer|greater_than[-1]'
        ),
        array(
            'field' => 'stock',
            'label' => 'stock',
            'rules' => 'required|integer|greater_than[-1]'
        )
    ),
    'updateBook' => array(
        array(
            'field' => 'title',
            'label' => 'ชื่อหนังสือ',
            'rules' => 'required'
        ),
        array(
            'field' => 'isbn',
            'label' => 'ISBN',
            'rules' => 'required|integer|min_length[10]|max_length[13]'
        ),
        array(
            'field' => 'detail',
            'label' => 'รายละเอียด',
            'rules' => 'required'
        ),
        array(
            'field' => 'page',
            'label' => 'จำนวนหน้า',
            'rules' => 'required|integer|greater_than[0]'
        ),
        array(
            'field' => 'price',
            'label' => 'ราคา',
            'rules' => 'required|integer|greater_than[-1]'
        ),
        array(
            'field' => 'stock',
            'label' => 'stock',
            'rules' => 'required|integer|greater_than[-1]'
        )
    ),
    'authorForm' => array(
        array(
            'field' => 'name',
            'label' => 'ชื่อผู้แต่ง',
            'rules' => 'required'
        ),
        array(
            'field' => 'surname',
            'label' => 'นามสกุล',
            'rules' => 'required'
        ),
        array(
            'field' => 'telNo',
            'label' => 'เบอร์โทรศัพท์',
            'rules' => 'required|numeric'
        ),
        array(
            'field' => 'email',
            'label' => 'อีเมลล์',
            'rules' => 'valid_email'
        )
    ),
    'form_create_position' => array(
        array(
            'field' => 'position',
            'label' => 'ชื่อตำแหน่ง',
            'rules' => 'required'
        )
    ),
    'form_edit_staff' => array(
        array(
            'field' => 'name',
            'label' => 'ชื่อพนักงาน',
            'rules' => 'required'
        ),
        array(
            'field' => 'surname',
            'label' => 'นามสกุล',
            'rules' => 'required'
        ),
        array(
            'field' => 'telNo',
            'label' => 'เบอร์โทรศัพท์',
            'rules' => 'required|numeric'
        ),
        array(
            'field' => 'email',
            'label' => 'อีเมลล์',
            'rules' => 'valid_email'
        ),
        array(
            'field' => 'address',
            'label' => 'ที่อยูู่',
            'rules' => 'required'
        ),
        array(
            'field' => 'personal_id',
            'label' => 'รหัสบัตรประชาชน',
            'rules' => 'required|numeric'
        ),
    ),
    'form_register_member' => array(
        array(
            'field' => 'name',
            'label' => 'ชื่อ',
            'rules' => 'required'
        ),
        array(
            'field' => 'surname',
            'label' => 'นามสกุล',
            'rules' => 'required'
        ),
        array(
            'field' => 'tel_number',
            'label' => 'เบอร์โทรศัพท์',
            'rules' => 'required|numeric'
        ),
        array(
            'field' => 'email',
            'label' => 'อีเมลล์',
            'rules' => 'valid_email|is_unique[tbl_person.email]'
        ),
        array(
            'field' => 'address',
            'label' => 'ที่อยูู่',
            'rules' => 'required'
        ),
         array(
            'field' => 'username',
            'label' => 'Username (บัญชีผู้ใช้งาน)',
            'rules' => 'required|is_unique[tbl_member.username]|alpha_numeric|min_length[4]'
        ),
        array(
            'field' => 'password',
            'label' => 'Password ( รหัสผ่าน )',
            'rules' => 'required|min_length[8]|alpha_numeric'
        ),
        array(
            'field' => 'confirm_password',
            'label' => 'ช่องยืนยันรหัส',
            'rules' => 'required'
        ),

    ),

'form_member_login' => array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|alpha_numeric'
        ),
        array(
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'required|alpha_numeric'
        ),
    ),
'form_update_qty' => array(
        array(
            'field' => 'qty',
            'label' => 'จำนวนสินค้า',
            'rules' => 'required|numeric|greater_than[0]'
        ),
    ),

'form_sending_info' => array(
    array(
        'field' => 'name',
        'label' => 'ชื่อ',
        'rules' => 'required'
        ),
    array(
        'field' => 'surname',
        'label' => 'นามสกุล',
        'rules' => 'required'
        ),
    array(
        'field' => 'tel_number',
        'label' => 'เบอร์โทรศัพท์',
        'rules' => 'required|numeric'
        ),
    array(
        'field' => 'address',
        'label' => 'ที่อยูู่',
        'rules' => 'required'
        ),
    ),

'form_payment_info' => array(
    array(
        'field' => 'bank_name',
        'label' => 'ชื่อธนาคาร',
        'rules' => 'required'
        ),
    array(
        'field' => 'amount',
        'label' => 'จำนวนเงิน',
        'rules' => 'required|numeric'
        ),
    array(
        'field' => 'datetime_payment',
        'label' => 'วันเวลาที่ชำระเงิน',
        'rules' => 'required'
        ),
    ),

'form_update_member_profile' => array(
        array(
            'field' => 'name',
            'label' => 'ชื่อ',
            'rules' => 'required'
        ),
        array(
            'field' => 'surname',
            'label' => 'นามสกุล',
            'rules' => 'required'
        ),
        array(
            'field' => 'telNo',
            'label' => 'เบอร์โทรศัพท์',
            'rules' => 'required|numeric|min_length[7]'
        ),
        array(
            'field' => 'email',
            'label' => 'อีเมลล์',
            'rules' => 'required|valid_email'
        ),
        array(
            'field' => 'address',
            'label' => 'ที่อยูู่',
            'rules' => 'required'
        ),
    ),

'form_change_password' => array(
        array(
            'field' => 'old_pw',
            'label' => 'รหัสผ่าน',
            'rules' => 'required|min_length[7]|alpha_numeric'
        ),
        array(
            'field' => 'new_pw',
            'label' => 'รหัสผ่านใหม่',
            'rules' => 'required|min_length[7]|alpha_numeric'
        ),
        array(
            'field' => 'confirm_pw',
            'label' => 'ยืนยันรหัสผ่านใหม่',
            'rules' => 'required|min_length[7]|alpha_numeric'
        ),
    ),

);
