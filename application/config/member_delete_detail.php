<div class="container">
    <div class="alert alert-warning">
        <p>
            <h3>
                <span class="icon star">&nbsp;รายละเอียดสมาชิก </span>
            </h3>
        </p>
        <table width="100%" border="0" class="table" id="center" align="center">
                <thead>
                    <tr>
                        <th width="30%"></th>
                        <th width="60%"></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="text-right"><b>ชื่อ - นามสกุล</b></td>
                        <td><?php echo $member['name'] ?>&nbsp;<?php echo $member['surname'] ?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Username</b></td>
                        <?php if( $member['username'] == NULL ) :?>
                            <td>ยังไม่ได้กำหนด Account</td>
                        <?php else : ?>
                            <td><?php echo $member['username'] ?></td>
                        <?php endif ?>
                    </tr>
                    <tr>
                        <td class="text-right"><b>ที่อยู่</b></td>
                        <td><?php echo $member['address'] ?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Email</b></td>
                        <td><?php echo $member['email'] ?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>เบอร์โทรศัพท์</b></td>
                        <td><?php echo $member['telNo'] ?></td>
                    </tr>
                </tbody>
        </table>
    </div>

    <div class="alert alert-danger">
        <div class="row">
            <div class="col-lg-12 text-center">
                <p><h4><b>ยืนยันการลบหรือไม่ ?<b></h4></p>
                <br>
                <br>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 text-right">
                <form action="<?php echo $site . 'memberDelete/confirm'?>" method="POST">
                    <input type="submit" value="ยืนยัน" class="btn btn-danger btn-lg">
                    <input type="hidden" name="id" value="<?php echo $member['id'] ?>">
                    <input type="hidden" name="pid" value="<?php echo $member['pid'] ?>">
                </form>
            </div>
            <div class="col-lg-6">
                <form action="<?php echo $site ?>" method="POST">
                    <input type="submit" value="ยกเลิก" class="btn btn-default btn-lg">
                </form>
            </div>
        </div>
    </div>
</div>