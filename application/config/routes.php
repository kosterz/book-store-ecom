<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	http://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There area two reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router what URI segments to use if those provided
  | in the URL cannot be matched to a valid route.
  |
 */

$route['default_controller'] = "frontend/main/index";
$route['404_override'] = '';
$route['Backend/Login'] = "backend/login/index";
$route['Backend/Login/Submit'] = "backend/login/submit";
$route['Backend/Login/signup'] = "backend/login/show_create_account_form";
$route['Backend/Login/signup/submit'] = "backend/login/check_personal_id";
$route['Backend/Login/signup/step2/submit'] = "backend/login/confirm_signup";
$route['Backend/Logout'] = "backend/login/logout";

$route['Backend/BookManage'] = "backend/book/index";
$route['Backend/BookManage/CreateNewBook'] = "backend/book/form_add_book";
$route['Backend/BookManage/save'] = "backend/book/add_book";
$route['Backend/BookManage/bookItem/(:num)'] = "backend/book/show_book_detail/$1";
$route['Backend/BookManage/Update'] = "backend/book/update_book";
$route['Backend/BookManage/deleteItem/(:num)'] = "backend/book/deleteItem/$1";
$route['Backend/BookManage/delConfirm/(:num)'] = "backend/book/delConfirm/$1";
$route['Backend/BookManage/cateForm'] = "backend/book/form_add_category";
$route['Backend/BookManage/addCate'] = "backend/book/add_category";
$route['Backend/BookManage/CreateNewBook/AddAuthor'] = "backend/book/form_add_author";
$route['Backend/BookManage/bookItem/(:num)/AddAuthor'] = "backend/book/form_add_author";
$route['Backend/BookManage/AddAuthor/submit'] = "backend/book/add_author";

$route['Backend/setting/category/newCate'] = "backend/category/form_add_category";
$route['Backend/setting/category/newCate/submit'] = "backend/category/add_category";
$route['Backend/setting/category'] = "backend/category/index";
$route['Backend/setting/category/(:num)'] = "backend/category/show_edit_category/$1";
$route['Backend/setting/category/update'] = "backend/category/update_category";
$route['Backend/setting/category/delete'] = "backend/category/show_delete_confirm";

$route['Backend/setting/author'] = "backend/author/index";
$route['Backend/setting/author/page/(:num)'] = "backend/author/index/$1";
$route['Backend/setting/author/(:num)'] = "backend/author/show_edit_author/$1";
$route['Backend/setting/author/update'] = "backend/author/update_author";
$route['Backend/setting/author/delete'] = "backend/author/show_delete_confirm";

$route['Backend/OrderManage/orderItem/(:num)'] = "backend/order/orderItem/$1";
$route['Backend/OrderManage/updateItem/(:num)'] = "backend/order/updateItem/$1";
$route['Backend/OrderManage/updateStatus'] = "backend/order/updateStatus";
$route['Backend/OrderManage'] = "backend/order";

$route['Backend/StaffManage'] = "backend/staff/index";
$route['Backend/StaffManage/detail/(:num)'] = "backend/staff/show_staff_detail/$1";
$route['Backend/StaffManage/delete/(:num)'] = "backend/staff/show_delete_detail/$1";
$route['Backend/StaffManage/delete/confirm'] = "backend/staff/delete_confirm";
$route['Backend/StaffManage/addStaff'] = "backend/staff/show_add_staff_form";
$route['Backend/StaffManage/addStaff/submit'] = "backend/staff/add_staff";
$route['Backend/StaffManage/detail/update'] = "backend/staff/update_staff";
$route['Backend/StaffManage/createPosition'] = "backend/staff/show_create_position";
$route['Backend/StaffManage/createPosition/submit'] = "backend/staff/create_position";
$route['Backend/Staff/Profile/update'] = "backend/staff/update_staff_profile";
$route['Backend/Staff/Profile'] = "backend/staff/show_profile";

$route['Backend/MemberManage'] = "backend/member/index";
$route['Backend/MemberManage/memberDetail/(:num)'] = "backend/member/show_member_detail/$1";
$route['Backend/MemberManage/memberDelete/(:num)'] = "backend/member/show_delete_detail/$1";
$route['Backend/MemberManage/memberDelete/confirm'] = "backend/member/delete_confirm";

$route['Backend/PaymentManage'] = "backend/payment/index";
$route['Backend/PaymentManage/detail/(:num)'] = "backend/payment/show_payment_detail/$1";
$route['Backend/PaymentManage/detail/updateStatus'] = "backend/payment/show_update_status/$1";
$route['Backend/PaymentManage/detail/updateStatus/submit'] = "backend/payment/update_status";
$route['Backend/PaymentManage/deleteDetail/(:num)'] = "backend/payment/show_delete_detail";
$route['Backend/PaymentManage/deleteDetail/confirm'] = "backend/payment/delete_confirm";

/*
Frontend
 */
$route['login'] = "frontend/login/submit_login";
$route['Register'] = "frontend/main/show_register_form";
$route['Register/submit'] = "frontend/register/submit_register_form";
$route['logout'] = "frontend/login/logout";


$route['cart/add'] = "frontend/cart/add_product";
$route['cart'] = "frontend/cart/index";
$route['cart/update'] = "frontend/cart/update_qty";
$route['cart/delete'] = "frontend/cart/delete_item";
$route['cart/checkout'] = "frontend/cart/show_sending_info";
$route['cart/checkout/summary'] = "frontend/cart/summary_order";
$route['cart/checkout/summary/confirm'] = "frontend/cart/confirm_order";
$route['New/page/(:num)'] = "frontend/main/index/$1";

$route['payment'] = "frontend/payment/index";
$route['payment/submit'] = "frontend/payment/confirm_payment";

$route['member'] = "frontend/member/index";
$route['member/profile'] = "frontend/member/show_profile";
$route['member/profile/update'] = "frontend/member/update_profile";
$route['member/changePW'] = "frontend/member/show_change_pw";
$route['member/changePW/submit'] = "frontend/member/update_password";
$route['member/order/history'] = "frontend/order/index";

$route['Book/Item/(:num)'] = "frontend/book/show_detail/$1";

$route['show/category/(:num)'] = "frontend/book/show_by_category/$1";
$route['show/category/(:num)/page/(:num)'] = "frontend/book/show_by_category/$1";

$route['search'] = "frontend/book/show_by_search";
$route['search/(:any)'] = "frontend/book/show_by_search/$1";
// $route['show/category/(:num)/page/(:num)'] = "frontend/book/show_by_category/$1";

// $route['New/page/(:num)'] = "frontend/main/index";

/* End of file routes.php */
/* Location: ./application/config/routes.php */