<?php
	class On_Load
	{
		private $ci ;
		public function __construct()
		{
			$this->ci =& get_instance();
		}

		public function check_login()
		{
			$uri = $this->ci->uri->segment(1);
			$class = $this->ci->router->class;
			$method = $this->ci->router->method;
			if($uri === "Backend" OR $uri === "backend")
			{
				if($this->ci->session->userdata('logged_in')==NULL)
				{
					if($class != 'login')
					{
						redirect("Backend/Login","refresh");
						exit();
					}
				}
				else
				{
					if($class == 'login' && $method!='logout')
					{
						redirect("/Backend/BookManage","refresh");
						exit();
					}
				}
			}
			// if($this->ci->session->userdata('logged_in')!=NULL)
			// {
			// 	echo "Logged In";
			// }
			// else
			// {
			// 	redirect("backend/login/index","refresh");
			// 	exit();
			// }
		}
	}

 ?>