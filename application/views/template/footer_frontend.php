      <div class="row-container">
        <div class="row bck dark footer text center margin_top margin_bottom">
            <div class="col-lg-6">
                <h4><span class="glyphicon glyphicon-copyright-mark"></span><b> Copy Right By</b> <span class="text italic">KOsTErZ Noom </span></h4>
                <p class="text thin">All right reserved, <span class="glyphicon glyphicon-registration-mark"></span></p>
            </div>
            <div class="col-lg-6 ">
                <h4><b> หากมีข้อสงสัยใดกรุณาติดต่อ </b></h4>
                <p class="text thin"><span class="glyphicon glyphicon-envelope"></span> : nkosterz@gmail.com</p>
            </div>
        </div>
    </div>
</body>
</html>