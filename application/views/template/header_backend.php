<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.icons.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/myCSS.css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
    </head>


    <body class="backend">
        <aside class="sidebar"> <!-- Side Menu -->
            <header class="bck text-center bold" id="well">
                <b>Staff tools</b>
            </header>
            <div class="margin-left">
                <h4 class="green bold color theme inline text-center">&nbsp;<?php echo $this->session->userdata ( 'logged_in' )['name'];?><b></b>
                    <span class="badge"><?php echo $this->session->userdata ( 'logged_in' )['position']?></span></h4>
            </div>
            <div class="text-center">
                <div class="row">
                    <div class="col-lg-6 text-right">
                        <?php
                        echo form_open('/Backend/Staff/Profile');
                        echo form_submit(array('name' => 'submit', 'class' => 'btn btn-primary', 'value' => 'Profile'));
                        echo form_close();
                        ?>
                    </div>
                    <div class="col-lg-6 text-left">
                        <?php
                        echo form_open('/Backend/Logout');
                        echo form_submit(array('name' => 'submit', 'class' => 'btn btn-primary', 'value' => 'Logout'));
                        echo form_close();
                        ?>
                    </div>
                </div>
            </div>


            <span class="text-center bold"><h4 id="well">Menu</h4></span>
            <ul class="nav nav-pills nav-stacked">
                <li>
                    <a href="/BookStore"><span class="glyphicon glyphicon-home"></span> <b>กลับสู่เว็บไซต์หลัก</b><br></a>
                </li>

                <li>
                    <a href="<?php echo base_url() ?>Backend/MemberManage">
                        <span class="glyphicon glyphicon-user">
                        </span><b> การจัดการข้อมูลสมาชิก </b> </a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Backend/BookManage">
                        <span class="glyphicon glyphicon-barcode"></span><b> การจัดการข้อมูลหนังสือ </b>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url() ?>Backend/OrderManage">
                        <span class="glyphicon glyphicon-shopping-cart"></span><b> การจัดการข้อมูลใบสั่งซื้อ </b>
                    </a>
                </li>

                <li>
                    <a href="<?php echo base_url() ?>Backend/PaymentManage/">
                        <span class="glyphicon glyphicon-usd"></span><b> การจัดการข้อมูลแจ้งชำระเงิน </b>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url() ?>Backend/StaffManage/">
                        <span class="glyphicon glyphicon-wrench"></span><b> การจัดการข้อมูลพนักงาน </b>
                    </a>
                </li>
            </ul>
        </aside>
        <div class="main-content">


