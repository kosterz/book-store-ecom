<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.icons.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.theme.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/myCSS_2.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/jquery-ui-timepicker-addon.css" />
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/myCarousel.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/owl.carousel.css">
        <link rel="stylesheet" media="all" type="text/css" href="<?php echo base_url() ?>asset/css/jquery-ui-1.10.4.custom.css" />
        <script src="<?php echo base_url() ?>asset/js/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/owl.carousel.js"></script>
         <script src="<?php echo base_url() ?>asset/js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/jquery-ui-timepicker-addon.js" type="text/javascript"></script>
    </head>
    <body>
        <!-- Header -->
        <div class="row-container">
            <div class="row-edit">
                <div class="col-lg-5">
                    <h2 class="text center italic thin">BookStore , let's find your life</h2>
                </div>
                <div class="col-lg-3">&nbsp;</div>
                <div class="col-lg-4 text right" id="cart-info">
                    <?php if(!isset($cart)) :?>
                    <b><span class="glyphicon glyphicon-shopping-cart"></span> จำนวน</b> <small>( <?php echo $this->cart->total_items() ?> ) ชิ้น</small>
                    &nbsp;<b>รวมเงิน :</b>  <small>( <?php echo $this->cart->total() ?> ) บาท</small>
                    <p><h5><a href="/BookStore/cart" class="icon signin text book"><b>&nbsp;Check out !&nbsp;</b></a></h5></p>
                <?php endif ?>
               </div>
            </div>
        </div>

        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr class="bck dark">
                    <td width="25%">&nbsp;</td>
                    <td width="50%">
                        <section>
                            <div class="row text-center">
                                <nav data-tuktuk="menu" class="column_12 padding text bold">
                                    <a href="/BookStore" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;หน้าหลัก</a>
                                    <a href="/BookStore/payment"><span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;แจ้งการชำระเงิน</a>
                                    <a href="#"><span class="glyphicon glyphicon-bookmark"></span>&nbsp;Promotion</a>
                                    <a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;สมาชิก</a>
                                    <a href="#"><span class="glyphicon glyphicon-phone"></span>&nbsp;ติดต่อเรา</a>
                                </nav>
                            </div>
                        </section>
                    </td>
                    <td width="25%">&nbsp;</td>
                    <td></td>
                </tr>
            </tbody>
        </table>

        <!-- Search Bar -->
        <div class="row-container">
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-3">
                </div>
                <div class="col-lg-5">
                        <form id="book_search" name="book_search" action="/BookStore/search"
                            method="GET" class="navbar-form navbar-right">
                            <div class="row">
                                <div class="col-lg-3">
                                    <select name="search_option" class="form-control" id="small_field">
                                        <option value="1">ชื่อหนังสือ</option>
                                        <option value="2">ISBN</option>
                                        <option value="3">ผู้แต่ง</option>
                                        <option value="4">รายละเอียด</option>
                                    </select>
                                </div>
                                <div class="col-lg-9">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="small_field" name="search_text">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit" id="small_field"><span class="glyphicon glyphicon-search">&nbsp;&nbsp;ค้นหา</span></button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>