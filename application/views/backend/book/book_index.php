<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href=""> <span
                    class="glyphicon glyphicon-barcode"></span> Book Management
            </a>
        </div>

        <div class="collapse navbar-collapse"
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดู / แก้ไขรายละเอียดสินค้า</a></li>
                <li><a href="/BookStore/Backend/BookManage/CreateNewBook">เพิ่มสินค้า</a></li>
            </ul>

            <form id="book_search" name="book_search"
                  action="/BookStore2/backend/BookManage/book_search.action"
                  method="POST" class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" name="searchText" value=""
                           id="book_search_searchText" class="form-control"
                           placeholer="ค้นหาจาก ID , ISBN , ชื่อหนังสือ">
                </div>
                <input type="submit" id="book_search_0" value="Submit"
                       class="btn btn-default">
            </form>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <?php
    if (count($rs) == 0) {
        echo '<div class="alert alert-warning">
					        <h4>ยังไม่มีรายการหนังสือใด <a href="NewBook/form"><b>คลิกที่นีเพื่อเพิ่มหนังสื
					        </div>';
    } else {
        echo '<div class = "panelKz panel-info">
				            <div class = "panel-heading">
				            	<h3 class="panel-title">รายการหนังสือทั้งหมด</h3>
				            </div>
				            <div class = "panel-body">
				                <table width = "100%" border = "0" class = "table table-striped" id = "center" align = "center">
				                    <thead>
				                    <tr>
					                    <th width="5%">ID</th>
	                                    <th width="39%">ชื่อหนังสือ</th>
	                                    <th width="12%">หมวดหมู่</th>
	                                    <th width="5%">ราคา</th>
	                                    <th width="3%">คลัง</th>
	                                    <th width="12%">วันทีี่เพิ่ม</th>
	                                	<th width="25%">การดำเนินการ</th>
				                    </tr>
			                    </thead>
			                    <tbody>';
        foreach ($rs as $r) {
            echo '<tr>
					            <td>' . $r ["id"] . '</td>
					            <td>' . $r ["title"] . '</td>
					            <td>' . $r ["category"] . '</td>
					            <td>' . $r ["price"] . '</td>
					            <td>' . $r ["stock"] . '</td>
					            <td>' . $r ["date_add"] . '</td>
					            <td class = "text-center">
					                <a href = "' . base_url() . 'Backend/BookManage/bookItem/' . $r ["id"] . '">
					                    <button type = "button" class = "btn-xs btn-primary detail" id = "' . $r ["id"] . '">
					                        <span class = "icon search">&nbsp;แก้ไขหนังสือ</span>
					                    </button>
					                </a>
					                <a href = "' . base_url() . 'Backend/BookManage/deleteItem/' . $r ["id"] . '">
					                    <button type = "button" class = "btn-xs btn-info delete" id = "' . $r ["id"] . '">
					                        <span class = "icon trash">&nbsp;ลบหนังสือ</span>
					                    </button>
					                </a>
					            </td>
       						 </tr>';
        } //end foreach
        echo '</tbody>
						  </table>
						  </div>';
        echo '</div>';
    } // End else
    ?>
    <div class="text-right">
    	<ul class="pagination">
    		<?php echo $this->pagination->create_links(); ?>
    	</ul>
    </div>
