<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container">
    <div class="error">
        <div class="alert alert-danger">
            <div class="row">
                <div class="col-lg-3">
                    <h4><b><span class="icon ban-circle"> เกิดข้อผิดพลาด!!!</span></b></h4>
                </div>
                <div class="col-lg-9">
                    <h4>ไม่สามารถลบหนังสือเล่มนี้ได้เนื่องจากมีอยู่ในรายการสั่งซื้อ <a href="../">กลับหน้ารายการหนังสือ</a></h4>
                </div>
            </div>
            <br>
            <p>
                <span class="label label-info">
                    รายละเอียด
                </span> 
            </p>
            <br>
            <ol>
                <?php
                foreach ($orderList as $order) {
                    echo '<li>หมายเลขใบสั่งซื้อ&nbsp;'
                    . '<a href ="'.  base_url() .'backend/OrderManage/detailItem/' . $order['order_id'] . '"><b></b>' . $order['order_id'] . '</a></li>';
                }
                ?>
            </ol>

            </h4>
        </div>
    </div>
</div>