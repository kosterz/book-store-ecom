<div class="container">
    <div class="alert alert-success">
        <h4><b><span class="icon star">&nbsp;</span>เพิ่มหนังสือ : </h4><br>
        <?php
        echo 'ชื่อหนังสือ : ' . $book['title'] . '<br>';
        echo 'หมวดหมู่ : ' . $book['cat_id'] . '<br>';
        echo 'ISBN : ' . $book['isbn'] . '<br>';
        echo 'รายละเอียด : ' . $book['detail'] . '<br>';
        echo 'ผู้แต่ง : ' . $book['author_id'] . '<br>';
        echo 'จำนวนหน้า : ' . $book['page'] . '<br>';
        echo 'ราคา : ' . $book['price'] . '<br>';
        echo 'จำนวนสินค้า : ' . $book['stock'] . '<br>';
        echo 'รูปปก : ' . $book['pic'] . '<br>';
        echo 'Now : ' . $book['date_add'] . '<br>';
        ?>
        </b>
        <br>
        <a href="<?php echo $site ?>"><b>&nbsp;กลับสู่หน้าเพิ่มหนังสือ</b></a>
    </div>
</div>

