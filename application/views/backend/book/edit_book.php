<div class="container">
    <div class="bookEditForm">
        <form action="<?php echo base_url().'Backend/BookManage/Update'?>" method="POST" enctype="multipart/form-data">
            <div class="panelKz panel-info">
                <div class="panel-heading">
                    <h3><span class="label label-info">แก้ไขรายละเอียดหนังสือ</span></h3>
                </div>
                <div class="panel-body">
                    <table width="100%" border="0" class="table" id="center" align="center">
                        <tr>
                            <td width="14%"></td>
                            <td width="63%">
                                <label for="thumbnail">หน้าปก</label>
                                <a class="thumbnail">
                                    <img class="resize" src="<?php echo base_url().'images/cover/'.$book['pic'];?>" id="imgprofile"
                                         data-src="/BookStore/asset/js/holder.js/130x100">
                                </a><br>
                                <input type="file" name="pic" class="form-control"/>
                            <td width="23%"><span id="titleNotice"> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td width="14%"></td>
                            <td width="63%">
                                <label for="titleNotice">ชื่อหนังสือ</label>
                                <input type="text" name="title" value="<?php echo $book['title'];?>" class="form-control"/>
                                <!--                    <input type="text" class="form-control" name="title"
                                                           onblur="checkEmpty(this, 'titleNotice')" value="${book.title}">-->
                            </td>
                            <td width="23%"><span id="titleNotice"> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <label for="titleNotice">ISBN</label>
                                <input type="text" name="isbn" value="<?php echo $book['isbn'];?>" class="form-control"/>
                                <!--                    <input type="text" class="form-control" id="form-control"
                                                           value="${book.isbn}" onblur="checkEmpty(this, 'ISBNNotice')">-->
                            </td>
                            <td><span id="ISBNNotice"> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <label for="titleNotice">หมวดหมู่</label>
                                <?php echo form_dropdown('category', $category, $book['cat_id'],'class="form-control"');?>
                            </td>
                            <td>
                                <label for="titleNotice"></label>
                                <a href="/BookStore/Backend/BookManage/cateForm" class="btn btn-primary" >เพิ่มหมวดหมู่</a>
                            </td>
                        </tr>
                        <?php $count = 1; ?>
                        <?php foreach ($authorList AS $write) : ?>
                        <tr class="author_div_<?php echo $count ?>">
                            <td></td>
                            <td>
                                <label for="titleNotice">ผู้แต่ง</label>
                                <?php echo form_dropdown('author[]', $author, $write['author_id'],'class="form-control"');?>
                            </td>
                            <td>
                            <?php if($count===1) :?>
                                <a href="/BookStore/Backend/BookManage/bookItem/<?php echo $book['id'] ?>/AddAuthor" class="btn btn-primary" id="link_create_author">เพิ่มข้อมูลผู่แต่ง</a>
                                <div class="btn glyphicon glyphicon-plus green" id="add_more_author" data-toggle="tooltip" data-placement="top" title="เพิ่มฟิลด์ผู้แต่ง"></div>
                            <?php else : ?>
                                <div class="glyphicon glyphicon-minus" id="remove_author" data-toggle="tooltip"></div>
                            <?php endif ?>
                            <?php  $count++ ?>
                            </td>
                        <?php endforeach ?>
                        </tr>
                        <tr>
                            <td height="130"></td>
                            <td>
                                <label for="titleNotice">รายละเอียด</label>
                                    <textarea name="detail" rows="5" class="form-control" id="form-control"><?php echo $book['detail'] ?>
                                    </textarea>
                            </td>
                            <td><span id="detailNotice"> * ( required )</span></td>
                        </tr>
                         <tr>
                            <td></td>
                            <td>
                                <label for="titleNotice">จำนวนหน้า</label>
                                <input type="text" name="page" value="<?php echo $book['page'];?>" class="form-control"/>
                                <!--                    <input type="text" class="form-control" id="form-control"
                                                           value="${book.price}" name="price" onblur="checkEmpty(this, 'priceNotice')">-->
                            </td>
                            <td><span id="priceNotice"> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <label for="titleNotice">ราคา</label>
                                <input type="text" name="price" value="<?php echo $book['price'];?>" class="form-control"/>
                                <!--                    <input type="text" class="form-control" id="form-control"
                                                           value="${book.price}" name="price" onblur="checkEmpty(this, 'priceNotice')">-->
                            </td>
                            <td><span id="priceNotice"> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <label for="titleNotice">สินค้าคงคลัง</label>
                                <input type="text" name="stock" value="<?php echo $book['stock'];?>" class="form-control"/>
                                <!--                    <input type="text" class="form-control" id="form-control"
                                                           value="${book.stock}" name="quantity"
                                                           onblur="checkEmpty(this, 'stockNotice')">-->
                            </td>
                            <td><span id="stockNotice"> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td class="text-center">
                                <input type="submit" class="btn btn-primary" value="ยืนยัน"/>
                                <a href="<?php echo base_url() .'Backend/BookManage' ?>">
                                    <button type="button" class="btn btn-default">ยกเลิก</button>
                                </a>
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                    <input type="hidden" name="pic" value="<?php echo $book['pic']?>"/>
                    <input type="hidden" name="id" value="<?php echo $book['id']?>"/>
                    <input type="hidden" name="author_count" class="author_count" value="<?php echo $count-1 ?>"/>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    var count = $('.author_count').val();
    var cloned;
     $(document).on("click", "#add_more_author", function() {
        // alert(count);
        cloneData = $('.author_div_1').clone().attr('class','author_div_'+(++count));
        cloneData.find('#add_more_author').attr('id','remove_author');
        cloneData.find('#remove_author').attr({
            class : 'btn glyphicon glyphicon-remove red',
            title : 'ลบฟิลด์ผู้แต่ง'
        });
        cloneData.find('#link_create_author').remove()
        cloneData.insertAfter('.author_div_'+ (count-1));
        $('.author_count').attr('value',count);
    });

    $(document).on("click", "#remove_author", function() {
        $('.author_div_'+count).remove();
        count--;
        $('.author_count').attr('value',count);
    });
</script>