<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3><span class="icon star">&nbsp;</span> เพิ่มผู้แต่ง</h3>
        </div>
        <div class="panel-body">

           <form id="add_cate" name="addAuthor" action="<?= site_url() ?>Backend/BookManage/AddAuthor/submit" method="POST">
                <table width="100%" border="0" class="table " id="center" align="center">
                    <tbody>
                        <tr>
                            <td width="14%" class="text-right"><b>ชื่อผู้แต่ง</b></td>
                            <td width="33%">
                                <input type="text" name="name" value="" id="add_cate_category_name" class="form-control">
                            </td><td width="13%"><span> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-right"><b>นามสกุล</b></td>
                            <td>
                                <input type="text" name="surname" value="" id="add_cate_category_name" class="form-control">
                            </td><td><span> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-right"><b>เบอร์โทรศัพท์</b></td>
                            <td>
                                <input type="text" name="telNo" value="" id="add_cate_category_name" class="form-control">
                            </td><td><span> * ( Required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-right"><b>Email</b></td>
                            <td>
                                <input type="text" name="email" value="" id="add_cate_category_name" class="form-control">
                            </td><td><span> * ( Option )</span></td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3">
                                <input type="submit" id="add_cate_0" value="Submit" class="btn btn-primary">
                                <input type="reset" value="Reset" class="btn btn-default">
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?php if(isset($book_id)) :?>
                    <?php echo form_hidden('book_id', $book_id); ?>
                <?php endif ?>
            </form>
        </div> <!-- panel body -->
    </div> <!-- panel -->
</div>
