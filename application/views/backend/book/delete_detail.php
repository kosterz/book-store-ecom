<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="deleteDetail">
        <div class="alert alert-warning">
            <p><h3><b>คุณแน่ใจว่าต้องการลบหนังสือเล่มนี้ใช่หรือไม่ ?</b></h3></p>
            <ul>
                <li><b>ชื่อหนังสือ : </b><?php echo $book['title']?></li>
                <li><b>หมวดหมู่ : </b><?php echo $book['category']?></li>
                <li><b>ผู้แต่ง : </b><?php echo $book['author']?></li>
                <li><b>ISBN : </b><?php echo $book['isbn']?></li>
                <li><b>ราคา : </b><?php echo $book['price']?></li>
                <li><b>Stock : </b><?php echo $book['stock']?></li>
            </ul>
            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" class="text-center">
                        <a href="../delConfirm/<?php echo $book['id'] ?>">
                            <button type="button" class="btn btn-primary" id="<?php echo $book['id'] ?>">ยืนยัน</button>
                        </a>
                        <a href="index">
                            <button type="button" class="btn btn-default">ยกเลิก</button>
                        </a>
                    </td>
                </tr>
            </table>
        </div> <!-- panel -->
    </div> <!-- class delete Detail -->
</div>