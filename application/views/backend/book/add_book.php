<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="/BookStore2/backend/BookManage/index">
                <span class="glyphicon glyphicon-barcode"></span> Book Management
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li ><a href="../BookManage">ดู / แก้ไขรายละเอียดสินค้า</a></li>
                <li class="active"><a href="form">เพิ่มสินค้า</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>

    <div class="panelKz panel-info">
        <div class="panel-heading">
            <h3><span class="label label-info">เพิ่มหนังสือ</span></h3>
        </div>
        <div class="panel-body">
            <form name="add_book_form" action="<?= base_url() ?>Backend/BookManage/save" method="POST" enctype="multipart/form-data">
                <table width="100%" border="0" class="table" id="center" align="center">
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td width="14%">Title</td>
                        <td width="33%">
                            <input type="text" name="title" class="form-control" size="100"/>
                        </td>
                        <td width="13%"><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>Category</td>
                        <td>
                            <?php if (count($categoryList) > 0) : ?>
                                <select name="category" class="form-control">
                                    <?php
                                    foreach ($categoryList as $category) {
                                        echo '<option value = "' . $category["id"] . '">' . $category["name"] . '</option>';
                                    }
                                    ?>
                                </select>
                            <?php else: ?>
                                <select name="category" class="form-control">
                                    <option value = "0">ยังไม่มีรายการหมวดหมู่ กรุณาเพิ่มหมวดหมู่ก่อน</option>';
                                </select>
                            <?php endif; ?>
                        </td>
                        </td>
                        <td>
                            <a href="cateForm" class="btn btn-primary" >เพิ่มหมวดหมู่</a>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>ISBN</td>
                        <td>
                            <input type="text" name="isbn" class="form-control" size="13" placeholder="เป็นตัวเลขความยาว 10 หรือ 13 ตัวเท่านั้น"/>
                        </td>
                        <td><span id="ISBNNotice"> * ( required )</span></td>
                    </tr>

                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td height="80">Detail</td>
                        <td>
                            <textarea name="detail" rows="3" class="form-control"></textarea>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr class="author_div_1">
                        <td width="5%">&nbsp;</td>
                        <td>Author</td>
                        <td>
                            <?php if (count($authorList) > 0) : ?>
                                <select name="author[]" class="form-control">
                                    <?php
                                    foreach ($authorList as $author) {
                                        echo '<option value = "' . $author["id"] . '">' . $author["name"] . ' ' . $author["surname"] .'</option>';
                                    }
                                    ?>
                                </select>
                            <?php else: ?>
                                <select name="author" class="form-control">
                                    <option value = "0">ยังไม่มีรายชื่อผู้แต่ง กรุณาเพิ่มผู้แต่งก่อน</option>';
                                </select>
                            <?php endif; ?>
                        </td>
                        <td>
                            <a href="<?php echo $site ?>CreateNewBook/AddAuthor" class="btn btn-primary" id="link_add_author">เพิ่มผู้แต่ง</a>
                            <div class="btn glyphicon glyphicon-plus btn-sm" id="add_more_author"></div>
                        </td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>Total page</td>
                        <td>
                            <input type="text" name="page" class="form-control" size="4" placeholder="ตัวเลขเท่านั้น"/>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>Price</td>
                        <td>
                            <input type="text" name="price" class="form-control" size="10" placeholder="ตัวเลขเท่านั้น"/>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>Stock</td>
                        <td>
                            <input type="text" name="stock" class="form-control" size="10" placeholder="ตัวเลขเท่านั้น"/>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>Book picture </td>
                        <td>
                            <input type="file" name="pic" class="form-control"/>
                        </td>
                        <td>(Optional)</td>
                    </tr>
                    <tr>
                        <td height="68" colspan="4" align="center">
                            <input type="submit" id="submit_0" value="Save" class="btn btn-primary"/>
                            <input type="reset" value="Reset" class="btn btn-danger"/>
                        </td>
                    </tr>
                </table>
            </form>

        </div>
    </div>
</div>

<script>
    var count = 1;
    var cloned;
     $(document).on("click", "#add_more_author", function() {
        cloneData = $('.author_div_'+count).clone().attr('class','author_div_'+(++count));
        cloneData.find('#add_more_author').attr('id','remove_author');
        cloneData.find('.form-control').attr('name','author_'+count);
        cloneData.find('#remove_author').attr('class','btn glyphicon glyphicon-minus btn-sm');
        cloneData.find('#link_add_author').remove()
        cloneData.insertAfter('.author_div_'+ (count-1));
    });

    $(document).on("click", "#remove_author", function() {
        $('.author_div_'+count).remove();
        count--;
    });
</script>

