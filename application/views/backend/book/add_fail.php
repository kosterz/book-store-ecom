<div class="container">
    <div class="alert alert-danger">
        มีข้อผิดพลาดบางประการ <a href="<?= $site ?>"><b>กลับสู่หน้าเพิ่มหนังสือ</b></a>
        <?php
        if (isset($error)) {
            echo $error['msg'];
        } else {
            echo validation_errors();
        }
        ?>
    </div>
</div>