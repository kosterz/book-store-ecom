<div class="container">

    <div class="alert alert-danger">
        มีข้อผิดพลาดบางประการ <a href="<?php echo base_url() . 'Backend/BookManage/bookItem/' . $id ?>"><b>กลับสู่หน้าแก้ไขหนังสือ</b></a>
        <?php echo validation_errors(); ?>
    </div>

</div>