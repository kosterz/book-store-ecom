<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="">
                <span class="glyphicon glyphicon-barcode"></span> Payment Management
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดู / ลบรายการแจ้งชำระเงิน</a></li>
            </ul>

            <form action="search" class="navbar-form navbar-right" method="POST">
                <div class="form-group">
                    <input type="text" name="searchText" class="form-control" placeholer="ค้นหาใบสั่งซื้อ"/>
                </div>
                <input type="submit" class="btn btn-default"/>
            </form>
        </div><!-- /.navbar-collapse -->
    </nav>
    <?php if($paymentList==NULL) :?>
        <div class="alert alert-warning">
            <h4>ยังไม่มีรายการแจ้งชำระเงิน</h4> 
        </div>
    <?php else : ?>
        <div class="panelKz panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">รายการใบสั่งซื้อทั้งหมด</h3>
            </div>
            <div class="panel-body">
                <table width="100%" border="0" class="table table-striped" id="center" align="center">
                    <thead>
                        <tr>
                            <th width="5%">#</th>
                            <th width="15%">Order ID</th>
                            <th width="15%">ธนาคาร</th>
                            <th width="10%">จำนวนเงิน</th>
                            <th width="25%">วัน/เวลา</th>
                            <th width="30%" class="text-center">รายละเอียด / ลบ</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($paymentList as $payment): ?>
                            <tr>
                                <td><?php echo $payment['id'] ?></td>
                                <td><?php echo $payment['order_id'] ?></td>
                                <td><?php echo $payment['bank'] ?></td>
                                <td><?php echo $payment['amount'] ?></td>
                                <td><?php echo $payment['datetime_payment'] ?></td>
                                <td class="text-center">
                                    <a href="<?php echo $site . 'detail/' . $payment['id'] ?>">
                                        <button type="button" class="btn-xs btn-primary detail">
                                            <span class="icon search">&nbsp;รายละเอียด</span>
                                        </button>
                                    </a>
                                    <a href="<?php echo $site . 'deleteDetail/' . $payment['id'] ?>">
                                        <button type="button" class="btn-xs btn-danger delete">
                                            <span class="icon trash">&nbsp;ลบรายการ</span>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>