<div class="row-container">
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon star">&nbsp;&nbsp;</span><b>Order no.</b>
                    </div>
                    <div class="col-lg-4">
                        <?php echo $order['id'] ?>
                    </div>
                    <div class="col-lg-3">
                        <span class="icon check">&nbsp;&nbsp;</span><b>สถานะ</b> :
                    </div>
                    <div class="col-lg-3">
                        <?php if ($order['status_id'] == 1) : ?>
                            <td>
                                <span class="label label-warning"><?php echo $order['status'] ?></span>
                            </td>
                        <?php elseif ($order['status_id'] == 2) : ?>
                            <td>
                                <span class="label label-info"><?php echo $order['status'] ?></span>
                            </td>
                        <?php elseif ($order['status_id'] == 3) : ?>
                            <td>
                                <span class="label label-success"><?php echo $order['status'] ?></span>
                            </td>
                        <?php else : ?>
                            <td>
                                <span class="label label-default"><?php echo $order['status'] ?></span>
                            </td>
                        <?php endif ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon user">&nbsp;&nbsp;</span><b>ชื่อ-นามสกุล</b> :
                    </div>
                    <div class="col-lg-4">
                        <i><?php echo $member['name'] . nbs(2) . $member['surname'] ?></i>
                    </div>
                    <div class="col-lg-3">
                        <span class="icon calendar">&nbsp;&nbsp;</span><b>วันที่เปิดใบสั่งซื้อ</b> :
                    </div>
                    <div class="col-lg-3">
                        <i><?php echo $order['order_date'] ?></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon phone">&nbsp;&nbsp;</span><b>เบอร์โทรศัพท์</b> :
                    </div>
                    <div class="col-lg-4">
                        <i><?php echo $member['telNo'] ?></i>
                    </div>
                    <div class="col-lg-3">
                        <span class="icon envelope">&nbsp;&nbsp;</span><b>Email</b> :
                    </div>
                    <div class="col-lg-3">
                        <i><?php echo $member['email'] ?></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon home">&nbsp;&nbsp;</span><b>ที่อยู่ในการส่ง</b> :
                    </div>
                    <div class="col-lg-4">
                        <i><?php echo $sendinfo['name'] ?>&nbsp;<?php echo $sendinfo['surname'] ?><br>
                            <?php echo $sendinfo['address'] ?><br>
                            เบอร์ติดต่อ : <?php echo $sendinfo['telNo'] ?><br>
                        </i>
                    </div>
                    <div class="col-lg-6">

                    </div>
                </div>
                <br>
                <div class="row text-center">
                    <div class="col-lg-12">
                        <h4><span class="label label-info icon star"> รายการสินค้า</span></h4>
                        <br>
                    </div>
                </div>
            </div>

            <table class="table table-striped" width="80%">
                <tr>
                    <th width="5%">#</th>
                    <th width="30%">ชื่อหนังสือ</th>
                    <th width="10%" class="text-center">จำนวน</th>
                    <th width="10%"class="text-center">ราคา</th>
                    <th width="10%"class="text-center">รวม</th>
                </tr>
                <s:set name="sum" value="0"/>
                <s:set name="count" value="1"/>
                <?php $count = 1; ?>
                <?php $sum = 0; ?>
                <?php foreach ($bookList as $book): ?>
                    <tr>
                        <td><?php echo $count ?></td>
                        <td><?php echo $book['title'] ?></td>
                        <td class="text-center"><?php echo $book['quantity'] ?></td>
                        <td class="text-center"><?php echo $book['o_price'] ?></td>
                        <td class="text-center"><?php echo $book['total'] ?></td>
                    </tr>
                    <?php $sum = $sum + $book['total']; ?>
                <?php endforeach; ?>
                <tr>
                    <td colspan="3"></td>
                    <td class="text-center">
                        <b>รวมเป็นเงิน</b>
                    </td>
                    <td class="text-center">
                        <?php echo $sum ; ?>
                    </td>
                </tr>
            </table>