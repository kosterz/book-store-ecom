
<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="">
                <span class="glyphicon glyphicon-barcode"></span> Payment Management
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดู / ลบรายการแจ้งชำระเงิน</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </nav>
    <div class="panelKz panel-info">
        <div class="panel-body">
            <div class="alert alert-success">
                <h4><span class="label label-info icon star"> รายละเอียดใบสั่งซื้อ</span></h4>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon star">&nbsp;&nbsp;</span><b>Order no.</b>
                    </div>
                    <div class="col-lg-4">
                        <button class="order_detail btn btn-success btn-xs" id="<?php echo $order['id'] ?>">
                            <?php echo $order['id'] ?>
                        </button>
                    </div>
                    <div class="col-lg-2">
                        <span class="icon check">&nbsp;&nbsp;</span><b>สถานะ</b> :
                    </div>
                    <div class="col-lg-2">
                        <?php if($order['status_id']==1) : ?>
                            <span class="label label-warning"><?php echo $order['status'] ?></span>
                        <?php elseif($order['status_id']==2) : ?>
                            <span class="label label-info"><?php echo $order['status'] ?></span>
                        <?php elseif($order['status_id']==3) : ?>
                            <span class="label label-success"><?php echo $order['status'] ?></span>
                        <?php else : ?>
                            <span class="label label-default"><?php echo $order['status'] ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="col-lg-2">
                        <form action="<?php echo $site .'detail/updateStatus' ?>" method="post">
                            <button type="submit" class="btn btn-primary btn-xs">
                                <span class="icon pencil">&nbsp;ปรับสถานะ</span>
                            </button>
                            <input type="hidden" name="order_id" value="<?php echo $order['id'] ?>">
                            <input type="hidden" name="payment_id" value="<?php echo $payment['id'] ?>">
                            <input type="hidden" name="status_id" value="<?php echo $order['status_id'] ?>">
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon user">&nbsp;&nbsp;</span><b>ชื่อ-นามสกุล</b> :
                    </div>
                    <div class="col-lg-4">
                        <i><?php echo $member['name'] ?>&nbsp;<?php echo $member['surname'] ?></i>
                    </div>
                    <div class="col-lg-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon phone">&nbsp;&nbsp;</span><b>เบอร์โทรศัพท์</b> :
                    </div>
                    <div class="col-lg-4">
                        <i><?php echo $member['telNo'] ?></i>
                    </div>
                    <div class="col-lg-3">
                        <span class="icon envelope">&nbsp;&nbsp;</span><b>Email</b> :
                    </div>
                    <div class="col-lg-3">
                        <i><?php echo $member['email'] ?></i>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-2">
                        <span class="icon home">&nbsp;&nbsp;</span><b>ที่อยู่ในการส่ง</b> :
                    </div>
                    <div class="col-lg-4">
                        <i><?php echo $sendinfo['name'] ?>&nbsp;<?php echo $sendinfo['surname'] ?><br>
                            <?php echo $sendinfo['address'] ?><br>
                            <b>เบอร์ติดต่อ : </b><?php echo $sendinfo['telNo'] ?><br>
                        </i>
                    </div>
                    <div class="col-lg-3">
                        <span class="icon calendar">&nbsp;&nbsp;</span><b>วันที่เปิดใบสั่งซื้อ</b> :
                    </div>
                    <div class="col-lg-3">
                        <i><?php echo $order['order_date'] ?></i>
                    </div>
                </div> <!-- row-->
            </div> <!--alert alert-info -->
            <div class="alert alert-success">
                <h4><span class="label label-info icon star"> รายละเอียดการแจ้งชำระเงิน</span></h4>
                <div class="row">
                    <div class="col-lg-3">
                        <span class="icon print">&nbsp;&nbsp;</span><b>ธนาคารที่โอนเข้ามา</b> :
                    </div>
                    <div class="col-lg-3">
                        <?php echo $payment['bank'] ?>
                        </div>
                    <div class="col-lg-3" class="text-left">
                        <span class="icon credit-card">&nbsp;&nbsp;</span><b>จำนวนเงินที่ต้องชำระ</b> :
                    </div>
                    <div class="col-lg-3" class="text-left">
                        <?php echo $total['total'] ?>
                    </div>
                </div> <!-- row-->
                <div class="row">
                    <div class="col-lg-3">
                        <span class="icon time">&nbsp;&nbsp;</span><b>เวลาที่ทำรายการ</b> :
                    </div>
                    <div class="col-lg-3">
                        <?php echo $payment['datetime_payment'] ?>
                    </div>
                    <div class="col-lg-3" class="text-left">
                        <span class="icon money">&nbsp;&nbsp;</span><b>จำนวนเงินที่แจ้งเข้ามา</b> :
                    </div>
                    <div class="col-lg-3" class="text-left">
                        <?php if($total['total'] <= $payment['amount']) : ?>
                            <span class="label label-success"><?php echo $payment['amount'] ?></span>
                        <?php else : ?>
                            <span class="label label-danger"><?php echo $payment['amount'] ?></span>
                        <?php endif; ?>
                    </div>
                </div> <!-- row-->
            </div>
            <div class="row">
                <div class="col-lg-10 ">
                </div>
                <div class="col-lg-2 text-center">
                    <br>
                    <a href="<?php echo $site ?>" class="btn btn-primary">ย้อนกลับ</a>
                </div>
            </div>
        </div> <!--panel-body -->
    </div> <!-- panel-->
</div><!--container -->

<div class="modal fade" id="modal_order_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog wide-modal">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">แก้ไขรายละเอียดหนังสือ</h4>
            </div>
            <div class="modal-body">
                <div class="order_detail_content">

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
            </div>
        </div> <!--/.modal-content -->
    </div> <!--/.modal-dialog -->
</div> <!--/.modal -->


<script>
    $(document).on("click", ".order_detail", function(event) {
        event.preventDefault();
        id = $(this).attr("id");
        $.ajax({
            url: "/BookStore/backend/payment/get_order_detail",
            type: 'POST',
            data: {'id': id},
            cache: false,
            success: function(data, textStatus, jqXHR) {
                $(".order_detail_content").empty();
                $(".order_detail_content").append(data);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Error");
            }
        });
        $("#modal_order_detail").modal('show');
    });

</script>