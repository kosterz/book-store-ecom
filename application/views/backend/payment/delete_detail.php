<div class="container">
	<div class="deleteDetail">
        <div class="alert alert-warning">
            <p><h3><b>คุณแน่ใจว่าต้องการรายการแจ้งชำระเงินรายการนี้หรือไม่ ?</b></h3></p>

            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3"><span class="icon star">&nbsp;&nbsp;</span><b>รหัสใบสั่งซื้อ : </b></div>
                <div class="col-lg-8"><?php echo $payment['order_id'] ?></div>
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3"><span class="icon print">&nbsp;&nbsp;</span><b>ธนาคาร : </b></div>
                <div class="col-lg-8"><?php echo $payment['bank'] ?></div>
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3"><span class="icon time">&nbsp;&nbsp;</span><b>วัน/เวลาทำรายการโอน : </b></div>
                <div class="col-lg-8"><?php echo $payment['datetime_payment'] ?></div>
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3"><span class="icon money">&nbsp;&nbsp;</span><b>จำนวนเงินที่แจ้ง : </b></div>
                <div class="col-lg-8"><?php echo $payment['amount'] ?></div>
            </div>
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-3"><span class="icon credit-card">&nbsp;&nbsp;</span><b>จำนวนเงินที่ต้องชำระ : </b></div>
                <div class="col-lg-8"><?php echo $total['total'] ?></div>
            </div>

            <table width="100%">
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td width="100%" class="text-center">
                        <form action="<?php echo $site ?>" method="POST">
                        	<button type="submit" class="btn btn-primary">ยืนยัน</button>
                        <a href="index">
                        	<button type="button" class="btn btn-default">ยกเลิก</button>
                        </a>
                        <?php echo form_hidden('id', $payment['id']); ?>
                        </form>
                    </td>
                </tr>
            </table>
        </div> <!-- alert -->
    </div> <!-- delete container -->
</div>