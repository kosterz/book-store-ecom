<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.icons.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="container">
            <div class="jumbotron">
                <div class="alert alert-info">
                    <h3><span class="icon star">&nbsp; Create Account - สร้างบัญชีผู้ใช้ของพนักงาน</span></h3>
                </div>
                <div class="alert alert-success">

                    <form action="<?php echo $site ?>" method="POST">
                        <div class="row-container">
                            <div class="row">
                                <div class="col-lg-4 text-center">
                                    <b>ขั้นตอนที่ 1</b>
                                </div>
                                <div class="col-lg-4 text-center">
                                    ขั้นตอนที่ 2
                                </div>
                                <div class="col-lg-4 text-center">
                                    เสร็จสิ้น
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: 35%">
                                            <span class="sr-only">33% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    รหัสบัตรประชาชน : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" name="personal_id" class="form-control" />
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">

                                </div>
                                <div class="col-lg-4 text-center">
                                    <input type="submit" class="btn btn-primary" value="ยืนยัน"/>
                                    <input type="reset" class="btn btn-default" value="ล้างค่า"/>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <?php if(isset($msg)) : ?>
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <div class="alert alert-danger">
                            	<?php echo $msg ?>
                                <!-- <span class="icon info-sign">&nbsp;ไม่พบข้อมูลรหัสบัตรประชาชน หรือได้ลงทะเบียน Account ไว้แล้ว กรุณาลองใหม่หรือติดต่อผู้จัดการ</span> -->
                            </div>
                        </div>
                    </div>
                <?php endif ?>

                <div class="row">
                    <div class="col-lg-12 text-right">
                        <div class="alert alert-warning">
                            <span class="icon info-sign">&nbsp;กรณีมีข้อสงสัยกรุณาติดต่อผู้จัดการ&nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>