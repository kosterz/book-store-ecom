<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.icons.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="container">
            <div class="jumbotron">
                <div class="alert alert-info">
                    <h3><span class="icon star">&nbsp; Create Account - สร้างบัญชีผู้ใช้ของพนักงาน</span></h3>
                </div>
                <?php if(isset($msg)) : ?>
                    <div class="alert alert-danger">
                        <?php echo $msg ?>
                    </div>
                <?php endif ?>

                <div class="alert alert-success">
                    <form action="<?php echo $site ?>"method="POST">
                        <div class="row-container">
                            <div class="row">
                                <div class="col-lg-4 text-center">
                                    ขั้นตอนที่ 1
                                </div>
                                <div class="col-lg-4 text-center">
                                    <b>ขั้นตอนที่ 2</b>
                                </div>
                                <div class="col-lg-4 text-center">
                                    เสร็จสิ้น
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" style="width: 66%">
                                            <span class="sr-only">66% Complete (success)</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    รหัสพนักงาน : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?php echo $staff['id'] ?>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    รหัสบัตรประชาชน : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?php echo $staff['personal_id'] ?>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    ชื่อ : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?php echo $staff['name'] ?>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    นามสกุล : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?php echo $staff['surname'] ?>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    ตำแหน่ง : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?php echo $staff['position'] ?>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Username : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <input type="text" class="form-control" name="username" placeholder="ตัวอักษรภาษาอังกฤษ หรือตัวเลข 4 ตัวขึ้นไป"/>
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Password : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                	<input type="password" class="form-control" name="password" placeholder="ตัวอักษรภาษาอังกฤษ หรือตัวเลข 8 ตัวขึ้นไป">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Confirm password : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <input type="password" class="form-control" name="confirm_password" placeholder="กรอกข้อมูลให้ตรงกับรหัสผ่าน">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Passcode : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                	<input type="password" class="form-control" name="passcode" maxlength="4" placeholder="ตัวอักษรภาษาอังกฤษ หรือตัวเลข 4 เท่านั้น">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Confirm passcode : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <input type="password" class="form-control" name="confirm_passcode" maxlength="4" placeholder="กรอกข้อมูลให้ตรงกับ Passcode">
                                </div>
                                <div class="col-lg-4">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                </div>
                                <div class="col-lg-4 text-center">
                                    <input type="submit" class="btn btn-primary" value="ยืนยัน"/>
                                    <input type="reset" class="btn btn-default" value="ล้างค้า"/>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                        </div>
                        <?php echo form_hidden('id', $staff['id']); ?>
                    </form>
                </div>

                <div class="row">
                    <div class="col-lg-12 text-right">
                        <div class="alert alert-warning">
                            <span class="icon info-sign">&nbsp;กรณีมีข้อสงสัยกรุณาติดต่อผู้จัดการ&nbsp;</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>