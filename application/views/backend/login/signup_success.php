<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?php echo $title ?></title>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.icons.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="container">
            <div class="jumbotron">
                <div class="alert alert-info">
                    <h3><span class="icon star">&nbsp; Create Account - สร้างบัญชีผู้ใช้ของพนักงาน</span></h3>
                </div>

                <div class="alert alert-success">
                    <div class="row-container">
                        <div class="row">
                            <div class="col-lg-4 text-center">
                                ขั้นตอนที่ 1
                            </div>
                            <div class="col-lg-4 text-center">
                                ขั้นตอนที่ 2
                            </div>
                            <div class="col-lg-4 text-center">
                                <b><span class="icon check">&nbsp;เสร็จสิ้น</span></b>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-success" style="width: 100%">
                                        <span class="sr-only">100% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 text-right">
                                รหัสพนักงาน : &nbsp;
                            </div>
                            <div class="col-lg-4">
                                <?php echo $staff['id'] ?>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 text-right">
                                รหัสพนักงาน : &nbsp;
                            </div>
                            <div class="col-lg-4">
                                <?php echo $staff['id'] ?>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 text-right">
                                รหัสบัตรประชาชน : &nbsp;
                            </div>
                            <div class="col-lg-4">
                                <?php echo $staff['personal_id'] ?>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4 text-right">
                                Username : &nbsp;
                            </div>
                            <div class="col-lg-4">
                                <?php echo $staff['username'] ?>
                            </div>
                            <div class="col-lg-4">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 text-right">
                        <div class="alert alert-success">
                            <span class="icon info-sign">&nbsp;</span><a href="<?php echo $site ?>">กลับสู่หน้าเข้าสู่ระบบ</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>