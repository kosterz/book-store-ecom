<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Staff Tools</title>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/tuktuk.icons.css"/>
        <link rel="stylesheet" href="<?php echo base_url() ?>asset/css/bootstrap.min.css"/>
        <script src="<?php echo base_url() ?>asset/js/jquery-2.0.3.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>asset/js/bootstrap.min.js" type="text/javascript"></script>
    </head>

    <body>
        <div class="container">
            <div class="jumbotron">
                <div class="alert alert-info">
                    <h3><span class="icon star">&nbsp; Staff Login - เข้าสู่ระบบหลังร้าน</span></h3>
                </div>
                <div class="alert alert-success">
                    <form action="<?= base_url() ?>Backend/Login/Submit" method="POST">
                        <div class="row-container">
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Username : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?= form_input(array('name' => 'username', 'class' => 'form-control')) ?>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Password : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?= form_password(array('name' => 'password', 'class' => 'form-control')) ?>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 text-right">
                                    Passcode : &nbsp;
                                </div>
                                <div class="col-lg-4">
                                    <?= form_password(array('name' => 'passcode', 'class' => 'form-control', 'maxlength' => '4')) ?>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">

                                </div>
                                <div class="col-lg-4 text-center">
                                    <?= form_submit(array('name' => 'submit', 'class' => 'btn btn-primary', 'value' => 'Submit')) ?>
                                    <?= form_reset(array('class' => 'btn btn-default', 'value' => 'Reset')) ?>
                                </div>
                                <div class="col-lg-4">

                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if (isset($msg)) {
                    echo '<div class="row">
                          <div class="col-lg-12">
                          <div class="alert alert-danger">
                         <span class="icon info-sign">&nbsp;เกิดข้อผิดพลาด&nbsp;</span>';
                    echo validation_errors();
                    echo '<br>' . $msg;
                    echo '</div>
                          </div>
                          </div>';
                }
                ?>

                <div class="row">
                    <div class="col-lg-12 text-right">
                        <div class="alert alert-warning">
                            <span class="icon info-sign">&nbsp;กรณียังไม่ได้กำหนด Account&nbsp;</span>
                            <a href="Login/signup">คลิกที่นี้</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

