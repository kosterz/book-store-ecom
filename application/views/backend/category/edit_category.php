<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3><span class="icon star">&nbsp;</span>เพิ่มหมวดหมู่</h3>
        </div>
        <div class="panel-body">


            <table width="100%" border="0" class="table " id="center" align="center">
                <form id="add_cate" name="addForm" action="<?php echo site_url() ?>Backend/setting/category/update" method="POST">
                    <tbody>
                        <tr>
                            <td width="14%" class="text-right"><b>ชื่อหมวดหมู่</b></td>
                            <td width="33%">
                                <input type="text" name="cate_name" id="add_cate_category_name" class="form-control" value="<?php echo $category['name'] ?>"
                            </td><td width="13%"><span> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3">
                                <input type="submit" id="add_cate_0" value="Submit" class="btn btn-primary">
                                <a href="<?php echo site_url() ?>Backend/setting/category" class="btn btn-default">กลับ</a>
                                <input type="hidden" name="id" value="<?php echo $id ?>">
                                <input type="hidden" name="status" value="1">
                            </td>
                        </tr>
                    </tbody>
                </form>
            </table>

        </div> <!-- panel body -->
    </div> <!-- panel -->
</div>
