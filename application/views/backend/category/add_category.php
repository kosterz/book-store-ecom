<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3><span class="icon star">&nbsp;</span>เพิ่มหมวดหมู่</h3>
        </div>
        <div class="panel-body">


            <table width="100%" border="0" class="table " id="center" align="center">
                <form id="add_cate" name="addForm" action="<?= site_url() ?>Backend/setting/category/newCate/submit" method="POST">
                    <tbody>
                        <tr>
                            <td width="14%" class="text-right"><b>ชื่อหมวดหมู่</b></td>
                            <td width="33%">
                                <input type="text" name="cate_name" value="" id="add_cate_category_name" class="form-control">
                            </td><td width="13%"><span> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3">
                                <input type="submit" id="add_cate_0" value="Submit" class="btn btn-primary">
                                <input type="reset" value="Reset" class="btn btn-default">
                            </td>
                        </tr>
                    </tbody>
                </form>
            </table>

        </div> <!-- panel body -->
    </div> <!-- panel -->

    <div class="panel panel-success">
        <div class="panel-heading">
            <h3><span class="icon star">&nbsp;</span>รายการหมวดหมู่ที่มีอยูในระบบ</h3>
        </div>

        <div class="panel-body">
            <?php
            if (count($categoryList) == 0) {
                echo '<div class="alert alert-warning">';
                echo '<b>ยังไม่มีหมวดหมู่</b>';
                echo '</div>';
            } else {
                echo '<div class="row-container">';
                $count = 0;
                foreach ($categoryList as $cate) {
                    if ($count % 2 == 0) {
                        echo '<div class="row">';
                        echo '<div class="col-lg-1"></div>';
                        echo '<div class="col-lg-5">';
                        echo '<li>' . $cate["name"] . '</li></div>';
                    } else {
                        echo '<div class="col-lg-1"></div>';
                        echo '<div class="col-lg-5">';
                        echo '<li>' . $cate["name"] . '</li></div>';
                        echo '</div>';
                    }
                    ++$count;
                }
                echo '</div>';
            }
            ?>
        </div><!-- panel body -->
    </div><!-- panel -->
</div>
