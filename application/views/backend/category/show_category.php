<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href=""> <span
                    class="glyphicon glyphicon-wrench"></span> Setting
            </a>
        </div>

        <div class="collapse navbar-collapse"
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดู / แก้ไข</a></li>
                <li><a href="/BookStore/Backend/setting/category/newCate">เพิ่มหมวดหมู่</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <?php if (count($category_list) == 0) : ?> {
    <div class="alert alert-warning">
    	<h4>ยังไม่มีรายการหนังสือใด
    		<a href="NewBook/form"><b>คลิกที่นีเพื่อเพิ่มหนังสือ</b></a>
    	</h4>
    </div>
    <?php else :  ?>
    <div class = "panelKz panel-info">
    	<div class = "panel-heading">
    		<h3 class="panel-title">รายการหนังสือทั้งหมด</h3>
    	</div>
    	<div class = "panel-body">
    		<table width = "100%" border = "0" class = "table table-striped" id = "center" align = "center">
    			<thead>
    				<tr>
    					<th width="10%">ID</th>
    					<th width="40%" class="text-center">ชื่อหมวดหมู่</th>
    					<th width="20%" class="text-center">จำนวนหนังสือ</th>
    					<th width="30%" class="text-center">การดำเนินการ</th>
    				</tr>
    			</thead>
    			<tbody>
        		<?php foreach ($category_list as $category) :  ?>
			        <tr>
			        	<td><?php echo $category['id'] ?></td>
			        	<td><?php echo $category['name'] ?></td>
			        	<td class="text-center"><?php echo $category['total_book'] ?></td>
			        	<td class = "text-center">
			        		<div class="row">
			        			<div class="col-lg-6 text-right">
			        				<a href = "<?php echo base_url() . 'Backend/setting/category/' . $category["id"] ?>">
			        					<button type = "button" class = "btn-xs btn-primary detail" id = "<?php echo $category["id"]?>">
			        						<span class = "icon pencil">&nbsp;เปลี่ยนชื่อ</span>
			        					</button>
			        				</a>
			        			</div>
			        			<div class="col-lg-6 text-left">
			        				<?php if($category['total_book'] === '0') : ?>
					        		<form action="/BookStore/Backend/setting/category/delete" method="POST">
					        			<button type ="submit" class = "btn-xs btn-info delete" id = "<?php echo $category["id"] ?>">
					        				<span class = "icon trash">&nbsp;ลบหมวดหมู่</span>
					        			</button>
					        			<input type="hidden" name="id" value="<?php echo $category["id"] ?>">
					        			<input type="hidden" name="status" value="1">
					        		</form>
			        				<?php endif ?>
			        			</div>
			        		</div>
			        	</td>
			        </tr>
    			<?php endforeach ?>
				</tbody>
			</table>
		</div>
    </div>
    <div class="text-right">
        <ul class="pagination">
            <?php echo $pagination_category ?>
        </ul>
    </div>
    <div class="alert alert-warning text-right">
       <h5><b>หมายเหตุ - การลบหมวดหมู่ จะต้องไม่มีรายการหนังสือใดๆอยู่ในหมวดหมู่นั้น</b></h5>
   </div>
    <?php endif ?>
