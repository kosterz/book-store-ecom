<div class="container">
    <div class="alert alert-danger">
        มีข้อผิดพลาดบางประการ <a href="<?php echo $site ?>"><b>กลับสู่หน้าแก้ไขหมวดหมู่</b></a>
        <?php echo validation_errors(); ?>
    </div>
</div>