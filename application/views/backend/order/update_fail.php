<div class="container">
    <div class="alert alert-danger">
        มีข้อผิดพลาดบางประการ <a href="<?php echo $site ?>"><b>กลับสู่หน้าหลัก</b></a>
        <?php echo $error_msg; ?>
    </div>
</div>