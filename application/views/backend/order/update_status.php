<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="">
                <span class="glyphicon glyphicon-barcode"></span> Order Management
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดู / ปรับสถานะใบสั่งซื้อ</a></li>
            </ul>

        </div><!-- /.navbar-collapse -->
    </nav>
    <div class="panel panel-primary">
        <div class="panel-heading">ปรับสถานะใบสั่งซื้อ หมายเลข : <b><?php echo $order['id'] ?></b> รหัสผู้ซื้อ : <?php echo $order['member_id'] ?></div>
        <div class="panel-body">
            <form action="<?php echo $site ?>updateStatus" method="POST">
                <table class="table" width="100%">
                    <tbody>
                        <tr >
                            <td width="20%">&nbsp;</td>
                            <td width="10%" class="text-right"><b>สถานะ :&nbsp;</b></td>
                            <td width="50%" class="text-center">
                                <?php echo form_dropdown('status', $statusList, $order['status_id'], 'class="form-control"') ?>
                            </td>
                            <td width="20%">&nbsp;</td>
                        </tr>
                        <tr>

                            <td colspan="4" class="text-center"> 
                                <?php echo form_submit('submit', 'ยืนยัน', 'class="btn btn-primary"') ?>
                                <a href="<?php echo $site ?>" class="btn btn-default">ย้อนกลับ</a>
                            </td>
                        </tr>
                        <tr>

                        </tr>
                    </tbody>
                </table>
                <?php echo form_hidden('id', $order['id']) ?>
            </form>
        </div>
    </div>
</div>