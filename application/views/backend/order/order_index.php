<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href=""> <span
                    class="glyphicon glyphicon-barcode"></span> Order Management
            </a>
        </div>

        <div class="collapse navbar-collapse"
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">รายการใบสั่งซื้อ</a></li>
            </ul>

            <form id="book_search" name="book_search"
                  action="/BookStore2/backend/BookManage/book_search.action"
                  method="POST" class="navbar-form navbar-right">
                <div class="form-group">
                    <input type="text" name="searchText" value=""
                           id="book_search_searchText" class="form-control"
                           placeholer="ค้นหาจาก ID , ISBN , ชื่อหนังสือ">
                </div>
                <input type="submit" id="book_search_0" value="Submit"
                       class="btn btn-default">
            </form>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <?php
    if (count($rs) == 0) {
        echo '<div class="alert alert-warning">
					        <h4>ยังไม่มีข้อมูลใบสั่งซื้อ
					        </div>';
    } else {
        echo '<div class = "panelKz panel-info">
				            <div class = "panel-heading">
				            	<h3 class="panel-title">รายการใบสั่งซื้อทั้งหมด</h3>
				            </div>
				            <div class = "panel-body">';

        echo $this->table->generate();
        echo '</div>';
        echo '</div>';
        echo $this->pagination->create_links();
    } // End else
