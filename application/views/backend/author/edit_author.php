<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h3><span class="icon star">&nbsp;</span> แก้ไขข้อมูลผู้แต่งหนังสือ</h3>
        </div>
        <div class="panel-body">

           <form id="add_cate" name="addAuthor" action="<?php echo site_url() ?>Backend/setting/author/update" method="POST">
                <table width="100%" border="0" class="table " id="center" align="center">
                    <tbody>
                        <tr>
                            <td width="14%" class="text-right"><b>ชื่อผู้แต่ง</b></td>
                            <td width="33%">
                                <input type="text" name="name" id="add_cate_category_name" class="form-control" value="<?php echo $author['name'] ?>">
                            </td><td width="13%"><span> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-right"><b>นามสกุล</b></td>
                            <td>
                                <input type="text" name="surname" id="add_cate_category_name" class="form-control" value="<?php echo $author['surname'] ?>">
                            </td><td><span> * ( required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-right"><b>เบอร์โทรศัพท์</b></td>
                            <td>
                                <input type="text" name="telNo" id="add_cate_category_name" class="form-control" value="<?php echo $author['telNo'] ?>">
                            </td><td><span> * ( Required )</span></td>
                        </tr>
                        <tr>
                            <td class="text-right"><b>Email</b></td>
                            <td>
                                <input type="text" name="email" id="add_cate_category_name" class="form-control" value="<?php echo $author['email'] ?>">
                            </td><td><span> * ( Option )</span></td>
                        </tr>
                        <tr>
                            <td class="text-center" colspan="3">
                                <input type="submit" id="add_cate_0" value="Submit" class="btn btn-primary">
                                <a href="<?php echo $site ?>" class="btn btn-default">กลับหน้าหลัก</a>
                                <input type="hidden" name="id" value="<?php echo $author['id'] ?>">
                                <input type="hidden" name="status" value="1">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </form>
        </div> <!-- panel body -->
    </div> <!-- panel -->
</div>
