<div class="container">
    <div class="alert alert-success">
        <b><span class="icon star">&nbsp;</span> เพิ่มผู้แต่งเรียบร้อยแล้ว : </b>
        <br>
        <li>ชื่อ - นามสกุล : <?php echo $author['name'] . ' ' . $author['surname'] ?></li>
        <li>เบอร์โทรศัพท์ : <?php echo $author['telNo'] ?></li>
        <li>อีเมลล์ : <?php
            if ($author['email'] == '' OR $author['email'] == null) {
                echo 'ไม่ได้กำหนด';
            } else {
                echo $author['email'];
            }
            ?></li>
        <a href="<?php echo $site ?>"><b>&nbsp;กลับ</b></a>
    </div>
</div>