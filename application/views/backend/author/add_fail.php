<div class="container">
    <div class="alert alert-danger">
        มีข้อผิดพลาดบางประการ <a href="<?php echo $site ?>"><b>กลับ</b></a>
        <?php if(isset($msg)) :?>
        	<?php echo $msg ?>
        <?php else :?>
        	<?php echo validation_errors(); ?>
    	<?php endif ?>
    </div>
</div>