<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href=""> <span
                    class="glyphicon glyphicon-wrench"></span> Setting
            </a>
        </div>

        <div class="collapse navbar-collapse"
             id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดู / แก้ไข</a></li>
                <li><a href="/BookStore/Backend/setting/category/newCate">เพิ่มผู้แต้งหนังสือ</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </nav>
    <?php if (count($author_list) == 0) : ?> {
    <div class="alert alert-warning">
    	<h4>ยังไม่มีรายชื่อผู้แต่งหนังสือ
    		<a href="NewBook/form"><b>คลิกที่นี้เพื่อเพิ่มผู้แต้งหนังสือ</b></a>
    	</h4>
    </div>
    <?php else :  ?>
    <div class = "panelKz panel-info">
    	<div class = "panel-heading">
    		<h3 class="panel-title">รายชื่อผู้แต่งหนังสือทั้งหมด</h3>
    	</div>
    	<div class = "panel-body">
    		<table width = "100%" border = "0" class = "table table-striped" id = "center" align = "center">
    			<thead>
    				<tr>
    					<th width="10%">ID</th>
    					<th width="25%" class="text-center">ชื่อ-นามสกุล</th>
    					<th width="20%" class="text-center">เบอร์ติดต่อ</th>
    					<th width="15%" class="text-center">จำนวนหนังสือ</th>
    					<th width="30%" class="text-center">การดำเนินการ</th>
    				</tr>
    			</thead>
    			<tbody>
        		<?php foreach ($author_list as $author) :  ?>
			        <tr>
			        	<td><?php echo $author['id'] ?></td>
			        	<td><?php echo $author['name'] . '&nbsp;&nbsp;' . $author['surname']?></td>
			        	<td><?php echo $author['telNo'] ?></td>
			        	<td class="text-center"><?php echo $author['total_book'] ?></td>
			        	<td class = "text-center">
			        		<div class="row">
			        			<div class="col-lg-6 text-right">
			        				<a href = "<?php echo base_url() . 'Backend/setting/author/' . $author["id"] ?>">
			        					<button type = "button" class = "btn-xs btn-primary detail" id = "<?php echo $author["id"]?>">
			        						<span class = "icon pencil">&nbsp;แก้ไขข้อมูล</span>
			        					</button>
			        				</a>
			        			</div>
			        			<div class="col-lg-6 text-left">
			        				<?php if($author['total_book'] === '0') : ?>
					        		<form action="/BookStore/Backend/setting/author/delete" method="POST">
					        			<button type ="submit" class = "btn-xs btn-info delete" id = "<?php echo $author["id"] ?>">
					        				<span class = "icon trash">&nbsp;ลบผู้แต่ง</span>
					        			</button>
					        			<input type="hidden" name="id" value="<?php echo $author["id"] ?>">
					        			<input type="hidden" name="status" value="1">
					        		</form>
			        				<?php endif ?>
			        			</div>
			        		</div>
			        	</td>
			        </tr>
    			<?php endforeach ?>
				</tbody>
			</table>
		</div>
    </div>
    <div class="text-right">
    	<ul class="pagination">
    		<?php echo $pagination_author ?>
    	</ul>
    </div>
    <div class="alert alert-warning text-right">
    	<h5><b>หมายเหตุ - การผู้แต่งหนังสือ จะต้องไม่มีรายการหนังสือใดๆที่ผู้แต่งเป็นคนเขียน</b></h5>
    </div>
    <?php endif ?>
