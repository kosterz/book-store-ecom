
<div class="container">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="">
                <span class="glyphicon glyphicon-barcode"></span> การจัดการข้อมูลสมาชิก
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">ดูรายละเอียดสมาชิก</a></li>
            </ul>

            <form action="search" class="navbar-form navbar-right" method="POST">
                <div class="form-group">
                    <input type="text" name="searchText" class="form-control" placeholer="ค้นหาจากชื่อ,username"/>

                </div>
                <input type="submit" class="btn btn-default">
            </form>

        </div><!-- /.navbar-collapse -->
    </nav>
    <div class="panelKz panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">รายการสมาชิกทั้งหมด</h3>
        </div>
        <div class="panel-body">
            <?php if(count($memberList) == 0) : ?>
                <div class="alert alert-warning">
                    <h4>ไม่มีข้อมูลสมาชิก</h4>
                </div>
            <?php else : ?>
                <table width="100%" border="0" class="table table-striped" id="center" align="center">
                    <thead>
                        <tr>
                            <th>รหัส</th>
                            <th>ชื่อ นามสกุล</th>
                            <th>Username</th>
                            <th>เบอร์โทรศัพท์</th>
                            <th>การดำเนินการ</th>
                        </tr>
                    </thead>

                    <tbody>
                        <?php foreach ($memberList as $member) :?>
                            <tr>
                                <td><?php echo $member['id'] ?></td>
                                <td><?php echo $member['name'] ?><?php echo nbs(2) . $member['surname'] ?></td>
                                <td><?php echo $member['username'] ?></td>
                                <td><?php echo $member['telNo'] ?></td>
                                <td class="text-center">

                                	<a href="<?php echo $site . 'memberDetail/'. $member['id'] ?>">
                                        <button type="button" class="btn-xs btn-primary detail" id="<?php echo $member['id'] ?>">
                                            <span class="icon search">&nbsp;ข้อมูลเพิ่มเติม</span>
                                        </button>
                                    </a>

                                   <a href="<?php echo $site . 'memberDelete/'. $member['id'] ?>">
                                        <button type="button" class="btn-xs btn-info delete" id="<?php echo $member['id'] ?>">
                                            <span class="icon check">&nbsp;ลบข้อมูลสมาชิก</span>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <?php echo $this->pagination->create_links(); ?>
            <?php endif; ?>
        </div> <!-- panel body -->
    </div>