<div class="container">
	<div class="panel panel-info">
        <div class="panel-heading">
            <h3 class="panel-title">รายละเอียดสมาชิก</h3>
        </div>
        <div class="panel-body">
            <table width="100%" border="0" class="table table-striped" id="center" align="center">
                <thead>
                    <tr>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td><b>ชื่อ - นามสกุล</b></td>
                        <td><?php echo $member['name'] ?>&nbsp;$<?php echo $member['surname'] ?></td>
                    </tr>
                    <tr>
                        <td><b>Username</b></td>
                        <td><?php echo $member['username'] ?></td>
                    </tr>
                    <tr>
                        <td><b>ที่อยู่</b></td>
                        <td><?php echo $member['address'] ?></td>
                    </tr>
                    <tr>
                        <td><b>Email</b></td>
                        <td><?php echo $member['email'] ?></td>
                    </tr>
                    <tr>
                        <td><b>เบอร์โทรศัพท์</b></td>
                        <td><?php echo $member['telNo'] ?></td>
                    </tr>
                    <tr><td></td></tr>
                    <tr>
                    	<td colspan="2" class="text-right">
                    		<a href="<?php echo $site ?>" class="btn btn-primary">กลับสู่หน้าหลัก</a>
                    	</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>