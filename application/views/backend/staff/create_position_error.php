<div class="container">
	<div class="alert alert-danger">
		<p>
			<h3><b>เกิดข้อผิดพลาด !</b></h3>
			<?php echo validation_errors() ?>
			<a href="<?php echo $site ?> "><b>กลับสู่หน้าเพิ่มพนักงาน</b></a>
		</p>
	</div>
</div>