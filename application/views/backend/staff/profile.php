<div class="container">
	<div class="panelKz panel-info">
        <div class="panel-heading">
            <h3><span class="label label-info">ข้อมูลส่วนตัว</span></h3>
        </div>
        <div class="panel-body">
            <div class="row-container">
                <div class="row">
                    <div class="col-lg-1">&nbsp;</div>
                    <div class="col-lg-2"><b>ชื่อ - นามสกุล : </b></div>
                    <div class="col-lg-3"><?php echo $profile['name'] ?></div>
                    <div class="col-lg-2"><b>ตำแหน่ง : </b></div>
                    <div class="col-lg-3"><?php echo $profile['position'] ?></div>
                    <div class="col-lg-1">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-lg-1">&nbsp;</div>
                    <div class="col-lg-2"><b>ที่อยู่ : </b></div>
                    <div class="col-lg-3"><?php echo $profile['address'] ?></div>
                    <div class="col-lg-2"><b>รหัสประจำตัวประชาชน : </b></div>
                    <div class="col-lg-3"><?php echo $profile['personal_id'] ?></div>
                    <div class="col-lg-1">&nbsp;</div>
                </div>
                <div class="row">
                    <div class="col-lg-1">&nbsp;</div>
                    <div class="col-lg-2"><b>เบอร์โทรศัพท์ : </b></div>
                    <div class="col-lg-3"><?php echo $profile['telNo'] ?></div>
                    <div class="col-lg-2"><b>Email : </b></div>
                    <div class="col-lg-3"><?php echo $profile['email'] ?></div>
                    <div class="col-lg-1">&nbsp;</div>
                </div>
            </div>
        </div>
    </div>

    <div class="alert alert-info">
        <li>หากข้อมูลใดผิดพลาด กรุณาติดต่อผู้จัดการ</li>
    </div>
    <div class="panelKz panel-info">
        <div class="panel-heading">
            <h3><span class="label label-info">บัญชีผู้ใช้</span></h3>
        </div>
        <div class="panel-body">
            <div class="row-container">
                <form action="<?php echo base_url(); ?>Backend/Staff/Profile/update" method="post">

                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <b>Username :</b> &nbsp;
                        </div>
                        <div class="col-lg-4">
                            <?php echo $profile['username'] ?>
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <b>Password :</b> &nbsp;
                        </div>
                        <div class="col-lg-4">
                        	<input type="password" name="password" class="form-control">
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <b>Confirm password :</b> &nbsp;
                        </div>
                        <div class="col-lg-4">
                            <input type="password" name="conf_pass" class="form-control">
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <b>Passcode :</b> &nbsp;
                        </div>
                        <div class="col-lg-4">
                        	<input type="password" name="passcode" class="form-control" maxlength="4">
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-4 text-right">
                            <b>Confirm passcode :</b> &nbsp;
                        </div>
                        <div class="col-lg-4">
                           	<input type="password" name="conf_passcode" class="form-control" maxlength="4">
                        </div>
                        <div class="col-lg-4">
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-lg-4">
                        </div>
                        <div class="col-lg-4 text-center">
                        	<input type="submit" class="btn btn-primary" value="ยืนยัน">
                        	<input type="reset" class="btn btn-default" value="ล้างค่า">
                        </div>
                        <div class="col-lg-4">

                        </div>
                    </div>
                <form>

            </div>
        </div>
    </div>

</div>