<div class="container">
	<div class="panelKz panel-info">
        <div class="panel-heading">
            <h3><span class="label label-info">เพิ่มข้อมูลพนักงาน</span></h3>
        </div>
        <div class="panel-body">
            <table width="100%" border="0" class="table" id="center" align="center">
                <form action="<?php echo $site .'addStaff/submit' ?>" method="POST">
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td width="14%">ชื่อ</td>
                        <td width="33%">
                            <input type="text" name="name" class="form-control"/>
                        </td>
                        <td width="13%"><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td width="14%">นามสกุล</td>
                        <td width="33%">
                            <input type="text" name="surname" class="form-control"/>
                        </td>
                        <td width="13%"><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>ตำแหน่ง</td>
                        <td>
                            <?php
                            	echo form_dropdown('position', $positionList,'','class="form-control"');
                             ?>
                        </td>
                        </td>
                        <td>
                            <a href="createPosition" class="btn btn-primary" >เพิ่มตำแหน่ง</a>
                            </td>
                        </td>
                        <tr>
                            <td width="5%">&nbsp;</td>
                            <td>หมายเลขประจำตัวประชาชน</td>
                            <td>
                            <input type="text" name="personal_id" class="form-control" placeholder="ตัวเลขเท่านั้น เช่น 1234567890123"/>
                        </td>
                        <td><span id="personal_id"> * ( required )</span></td>
                    </tr>

                    <tr>
                        <td>&nbsp;</td>
                        <td height="80">ที่อยู่</td>
                        <td>
                            <textarea name="address" class="form-control" row="3"></textarea>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td width="5%">&nbsp;</td>
                        <td>เบอร์โทรศัพท์</td>
                        <td>
                            <input type="text" name="telNo" class="form-control"/>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                        <td>Email</td>
                        <td>
                            <input type="text" name="email" class="form-control"/>
                        </td>
                        <td><span> * ( required )</span></td>
                    </tr>
                    <tr>
                        <td height="68" colspan="4" align="center">
                            <input type="submit" value="บันทึกข้อมูล" class="btn btn-primary"/>
                            <input type="reset" value="ล้างค่า" class="btn btn-danger"/>
                        </td>
                    </tr>
                </form>
            </table>
        </div>
    </div>
</div>