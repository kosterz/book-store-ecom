<div class="container">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-6">
                    <h3 class="panel-title"><b>รายละเอียดพนักงาน</b></h3>
                </div>
                <div class="col-lg-6 text-right">
                    <h3 class="panel-title"><b>รหัสพนักงาน</b> : <?php echo $staff['id'] ?></h3>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <form action="<?php echo $site . 'detail/update'?>" method="POST">
            <table width="100%" border="0" class="table table-striped" id="center" align="center">
                <thead>
                    <tr>
                        <th width="30%"></th>
                        <th width="50%"></th>
                        <th width="20%"></th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td class="text-right"><b>ชื่อ</b></td>
                        <td>
                            <input type="text" name="name" value="<?php echo $staff['name'] ?>" class="form-control"/>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>นามสกุล</b></td>
                        <td>
                            <input type="text" name="surname" value="<?php echo $staff['surname'] ?>" class="form-control"/>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>ตำแหน่ง</b></td>
                        <td>
                            <?php echo form_dropdown('position', $positionList, $staff['position_id'] , 'class="form-control"'); ?>
                            
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>รหัสบัตรประชาชน</b></td>
                        <td>
                            <input type="text" name="personal_id" value="<?php echo $staff['personal_id'] ?>" class="form-control"/>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Username</b></td>
                        <?php if( $staff['username'] == NULL ) :?>
                            <td>ยังไม่ได้กำหนด Account</td>
                        <?php else : ?>
                            <td><?php echo $staff['username'] ?></td>
                        <?php endif ?>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>ที่อยู่</b></td>
                        <td>
                            <textarea name="address" class="form-control" row="3"/><?php echo $staff['address'] ?></textarea>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>Email</b></td>
                        <td>
                            <input type="text" name="email" value="<?php echo $staff['email'] ?>" class="form-control"/>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="text-right"><b>เบอร์โทรศัพท์</b></td>
                        <td>
                            <input type="text" name="telNo" value="<?php echo $staff['telNo'] ?>" class="form-control"/>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" class="text-center">
                            <input type="submit" value="บันทึก" class="btn btn-primary"/>
                            <a href="../">
                                <button type="button" class="btn btn-default">กลับ</button>
                            </a>
                            <input type="hidden" name="id" value="<?php echo $staff['id'] ?>"/>
                            <input type="hidden" name="pid" value="<?php echo $staff['pid'] ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
        </div>
    </div>
</div>