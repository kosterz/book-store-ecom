<div class="container">
        <div class="alert alert-danger">
            <h4>เกิดข้อผิดพลาด !</h4><br>
            <?php
            echo validation_errors();
            ?>
            <a href="<?php echo $site ?>">กลับไปยังหน้าก่อน</a>
        </div>
</div>