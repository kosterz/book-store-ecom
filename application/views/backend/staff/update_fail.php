<div class="container">
        <div class="alert alert-danger">
            <h4>เกิดข้อผิดพลาด !</h4><br>
                    <?php
                    echo validation_errors();
                    if(isset($not_match)){
                    	echo '<li>'. $not_match .'</li>';
                    	echo anchor(base_url().'backend/staff/Profile', ' กลับไปยังหน้าก่อน');
                    }
                    ?>
        </div>
</div>