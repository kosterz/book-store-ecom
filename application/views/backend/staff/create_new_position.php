<div class="container">
	<div class="panel panel-primary">
		<div class="panel-heading">
			<h4>
				<span class="icon star"><b>เพิ่มตำแหน่ง</b></span>
			</h4>
		</div>
		<div class="panel-body">
			<table width="100%" border="0" class="table" id="center" align="center">
				<form action="<?php echo $site ?>" method="POST">
					<tr>
						<td width="10%" class="text-right"></td>
						<td width="40%">
							<label for="position">ชื่อตำแหน่ง</label>
							<input type="text" name="position" class="form-control" placeholder="Requried"/>
						</td>
						<td width="10%"> </td>
					</tr>
					<tr>
						<td class="text-center" colspan="3">
							<input type="submit" value="ยืนยัน" class="btn btn-primary" />
							<a href="<?php echo $site ?>" class="btn btn-default">ยกเลิก</a>
						</td>
					</tr>
				</form>
			</table>
        </div>
    </div> <!-- panel -->
</div>