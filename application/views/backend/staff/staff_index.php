<DIV class="container">
	<nav class="navbar navbar-inverse" role="navigation">
        <div class="navbar-header">
            <a class="navbar-brand" href="">
                <span class="glyphicon glyphicon-barcode"></span> Staff Viewer
            </a>
        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li class="active"><a href="index">รายชื่อพนักงาน</a></li>
                <li ><a href="<?php echo $site ?>addStaff">เพิ่มพนักงาน</a></li>
            </ul>

            <form action="search" class="navbar-form navbar-right" method="POST">
                <div class="form-group">
                    <input type="text" name="searchText" class="form-control" placeholer="ค้นหาจากชื่อ,username"/>
                </div>
                <input type="submit" class="btn btn-default"/>
            </form>

        </div><!-- /.navbar-collapse -->
    </nav>
    <div class="panelKz panel-info">
    	<div class="panel-heading">
    		<h3 class="panel-title">รายชื่อพนักงานทั้งหมด</h3>
    	</div>
        <div class="panel-body">
        	<?php if(count($staffList) == 0) : ?>
                <div class="alert alert-warning">
                    <h4>ยังไม่มีพนักงาน</h4>
                </div>
            <?php else : ?>
                <table width="100%" border="0" class="table table-striped" id="center" align="center">
                    <thead>
                        <tr>
                            <th width="5%">รหัส</th>
                            <th width="35%">ชื่อ - นามสกุล</th>
                            <th width="20%">Username</th>
                            <th width="20%">ตำแหน่ง</th>
                            <th width="20%">Operation</th>
                        </tr>
                    </thead>

                    <tbody>
                       <?php foreach ($staffList as $staff ) : ?>
                            <tr>
                                <td><?php echo $staff['id']?></td>
                                <td><?php echo $staff['name']. nbs(3) .$staff['surname'] ?></td>
                                <?php if ($staff['username']==null) : ?>
                                    <td>ยังไม่ได้กำหนด Account</td>
                                <?php else : ?>
                                    <td><?php echo $staff['username'] ?></td>
                                <?php endif ?>
                                <td><?php echo $staff['position'] ?></td>
                                <td class="text-center">
                                    <a href="<?php echo $site . 'detail/'. $staff['id'] ?>">
                                        <button type="button" class="btn-xs btn-primary detail" id="<?php echo $staff['id'] ?>">
                                            <span class="icon search">&nbsp;เพิ่มเติม</span>
                                        </button>
                                    </a>

                                   <a href="<?php echo $site . 'delete/'. $staff['id'] ?>">
                                        <button type="button" class="btn-xs btn-info delete" id="<?php echo $staff['id'] ?>">
                                            <span class="icon check">&nbsp;ลบพนักงาน</span>
                                        </button>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                <?php echo $this->pagination->create_links(); ?>
            <?php endif ?>
        </div>
    </div>
</DIV>
