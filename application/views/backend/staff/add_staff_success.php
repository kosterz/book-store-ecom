<div class="container">
	<div class="alert alert-success">
		<p><h4><b>เพิ่มข้อมูลเรียบร้อยแล้ว</b></h4></p>
		<ul>
			<li><b>รหัสพนักงาน : </b><?php echo $id ?></li>
			<li><b>ชื่อ - นามสกุล : </b><?php echo $person['name'] . ' ' . $person['surname'] ?></li>
			<li><b>รหัสบัตรประชาชน : </b><?php echo $staff['personal_id'] ?></li>
			<li><b>ที่อยู่ : </b><?php echo $person['address'] ?></li>
			<li><b>เบอร์โทรศัพท์ : </b><?php echo $person['telNo'] ?></li>
			<li><b>อีเมลล์ : </b><?php echo $person['email'] ?></li>
		</ul>
		<a href="<?php echo $site ?>"><b>กลับสู่หน้าหลัก</b></a>
	</div>
</div>