<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-2">
            <aside class="nav_left_menu" style="padding=1%">
                <div class="panel panel-default">
                    <div class="panel-heading bck color">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;หมวดหมู่</span></h4>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p><b>ยินดีต้อนรับ !</b></p>
                            <p class="text center italic"><?php echo $member['username'] ?></p>
                            <br>
                            <p><a href="<?php echo $site ?>/member/"><b>แก้ไขข้อมูลส่วนตัว</b></a></p>
                            <p><a href="/BookStore/logout"><b>ออกจากระบบ</b></a></p>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
        <div class="col-lg-8">
            <div class="panel" id="margin-top">
                <div class="panel-heading bck color">
                    <h4><span class="glyphicon glyphicon-shopping-cart">&nbsp;</span>รายละเอียดการชำระเงิน</h4>
                </div>
                <div class="panel-body">
                    <div class="row-container" id="margin">
                        <div class="payment" style="padding-bottom: 1%;">
                            <form action="/BookStore/payment/submit" method="POST">
                                <div class="row2">
                                    <br>
                                    <div class="col-lg-4 text bold right"><b>ใบสั่งซื้อที่ต้องการแจ้ง</b></div>
                                    <div class="col-lg-8">
                                        <?php echo form_dropdown('order_id', $order_list, '' ,'class="form-control"'); ?>
                                    </div>
                                </div>
                                <div class="row2">
                                    <br>
                                    <div class="col-lg-4 text bold right"><b>ธนาคารปลายทาง</b></div>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="small_field" name="bank_name">
                                    </div>
                                </div>
                                <div class="row2">
                                    <br>
                                    <div class="col-lg-4 text bold right"><b>จำนวนเงินที่ชำระ ( ใส่ถึงจุดทศนิยม )</b></div>
                                    <div class="col-lg-8">
                                        <input type="text" class="form-control" id="small_field" name="amount">
                                    </div>
                                </div>

                                <div class="row2">
                                    <br>
                                    <div class="col-lg-4 text bold right"><b>วันเวลาที่ชำระเงิน</b></div>
                                    <div class="col-lg-8">
                                        <!-- <input type="text" name="basic_example_1" id="basic_example_1" value=""> -->
                                        <input type="text" class="form-control datepicker" id="small_field" name="datetime_payment" >
                                    </div>
                                </div>

                                <div class="row2">
                                    <br>
                                    <div class="col-lg-12 text center">
                                        <input type="hidden" name="status" value='1'>
                                        <input type="submit" value="ยืนยันข้อมูล" class="btn btn-sm bck color">
                                        <a href="/BookStore/cart">
                                            <span class="btn btn-sm btn-default">ยกเลิก</span>
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div> <!-- sendinfo -->
                    </div> <!-- Row container -->
                </div> <!-- panel body-->
            </div> <!-- panel -->
        </div>
    </div>
</div>

<script>
    $(function() {
        $('.datepicker').datetimepicker();
    });

    $(document).ready(function() {
    $("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
});

</script>