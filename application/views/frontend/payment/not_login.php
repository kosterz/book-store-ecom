<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <aside class="nav_left_menu" style="padding=1%">
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;เข้าสู่ระบบ</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <form action="/BookStore/login" method="POST">
                                            <label for="username">ชื่อผู้ใช้งาน</label>
                                            <input type="text" name="username"  class="form-control" placeholder="Username">
                                            <label for="password">รหัสผ่าน</label>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                            <br>
                                            <input type="checkbox" name="remember"> จดจำผู้ใช้งานตลอด</input>
                                            <div class="text center">
                                                <br>
                                                <input type="submit" value="Sign-in" class="btn btn-sm btn-default">
                                                <input type="reset" value="Reset" class="btn btn-sm btn-default">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                    </aside>
                </div>
                <div class="col-lg-3">
                </div>
            </div>
</div>

<script>
    $(document).ready(function() {
    $("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
});
</script>