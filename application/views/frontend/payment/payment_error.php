<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-2">
            <aside class="nav_left_menu" style="padding=1%">
                <div class="panel panel-default">
                    <div class="panel-heading bck color">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;หมวดหมู่</span></h4>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <p><b>ยินดีต้อนรับ !</b></p>
                            <p class="text center italic"><?php echo $member['username'] ?></p>
                            <br>
                            <p><a href="<?php echo $site ?>/member/profile"><b>แก้ไขข้อมูลส่วนตัว</b></a></p>
                            <p><a href="/BookStore/logout"><b>ออกจากระบบ</b></a></p>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
        <br>
        <br>
        <div class="col-lg-8">
            <div class="row-container" id="margin">
                <div class="payment" style="padding-bottom: 1%;">
                    <div class="row2">
                        <div class="col-lg-12">
                            <div class="alert alert-danger">
                                <h3><b>เกิดข้อผิดพลาด ! </b></h3>
                                <br>
                                <?php echo validation_errors(); ?>

                                <br>
                                <div class="text right">
                                    <a href="/BookStore/payment" class="btn btn-sm bck dark color white">กลับสู่หน้าการแจ้งชำระเงิน</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div> <!-- sendinfo -->
            </div> <!-- Row container -->
        </div>
    </div>
</div>

<script>
    $(function() {
        $('.datepicker').datetimepicker();
    });

    $(document).ready(function() {
    $("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
});

</script>