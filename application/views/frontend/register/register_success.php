<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="breadcrumb bck theme color white">
				<div class="badge">
					<a href="/BookStore" class="active"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
					<b>&nbsp;>&nbsp;&nbsp;&nbsp;</b><span class="glyphicon glyphicon-pencil">&nbsp;</span>Register
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<div class="color theme">
					<h4><span class="glyphicon glyphicon-remove">&nbsp;</span><b>สมัครสมาชิกสำเร็จ !</b></h4>
				</div>
				<div class="row2">
					<div class="col-lg-12">
						<div class="alert alert-success">
							<h4>
								<p><b>ข้อมูลได้ถูกบันทึกเรียบร้อยแล้ว สามารถเข้าใช้งานได้ทันที <a href="/BookStore">กลับสู่หน้าหลัก</a></b></p>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>