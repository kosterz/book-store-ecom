<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="breadcrumb bck theme color white">
				<div class="badge">
					<a href="/BookStore" class="active"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
					<b>&nbsp;>&nbsp;&nbsp;&nbsp;</b><span class="glyphicon glyphicon-pencil">&nbsp;</span>Register
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<div class="color theme">
					<h4><span class="glyphicon glyphicon-remove">&nbsp;</span><b>เกิดข้อผิดพลาด</b></h4>
				</div>
				<div class="row2">
					<div class="col-lg-12">
						<div class="alert alert-danger">
							<?php echo validation_errors() ?>
							<?php
								if(isset($msg))
									echo '<p>'. $msg . '</p>';
							?>
						</div>
					</div>
				</div>
				<div class="rows">
					<div class="col-lg-12">
						<div class="text center">
							<a href="/BookStore" class="btn btn-sm bck color color white ">กลับสู่หน้าหลัก</a>
							<a href="<?php echo $site ?>" class="btn btn-sm btn-default">กลับสู่หน้าลงทะเบียน</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>