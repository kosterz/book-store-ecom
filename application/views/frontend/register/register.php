<!-- Breadcrumb -->
<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="breadcrumb bck theme color white">
				<div class="badge">
					<a href="/BookStore" class="active"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
					<b>&nbsp;>&nbsp;&nbsp;&nbsp;</b><span class="glyphicon glyphicon-pencil">&nbsp;</span>Register
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<form action="<?php echo $site ?>" method="POST">
					<div class="color theme">
						<h5><span class="glyphicon glyphicon-user">&nbsp;</span><b>Account Information - ข้อมูลบัญชีผู้ใช้งาน</b></h5>
					</div>
					<div class="panel account_info" style="padding-bottom: 1%;">
						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Username ( บัญชีผู้ใช้งาน )</b></div>
							<div class="col-lg-4 text center">
								<input type="text" name="username" id="small_field" class="form-control">
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>
						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Password ( รหัสผ่าน )</b></div>
							<div class="col-lg-4 text center">
								<input type="password" name="password" id="small_field" class="form-control">
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>
						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Confirm Password ( ยืนยันรหัสผ่าน )</b></div>
							<div class="col-lg-4 text center">
								<input type="password" name="confirm_password" id="small_field" class="form-control">
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ต้องตรงกับรหัสผ่าน</div>
						</div>
					</div>
					<br>
					<div class="color theme">
						<h5><span class="glyphicon glyphicon-user">&nbsp;</span><b>Personal Information - ข้อมูลส่วนตัว</b></h5>
					</div>
					<div class="panel personal_info" style="padding-bottom: 1%;">
						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Name ( ชื่อ )</b></div>
							<div class="col-lg-4 text center">
								<input type="text" name="name" id="small_field" class="form-control">
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>
						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Surname ( นามสกุล )</b></div>
							<div class="col-lg-4 text center">
								<input type="text" name="surname" id="small_field" class="form-control">
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>
						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Address ( ที่อยู่ )</b></div>
							<div class="col-lg-4 text center">
								<textarea name="address" class="form-control" cols="30" rows="4"></textarea>
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>

						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Telephone No. ( เบอร์โทรศัพท์ )</b></div>
							<div class="col-lg-4 text center">
								<input type="text" name="tel_number" id="small_field" class="form-control"> 							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>

						<div class="row2">
							<br>
							<div class="col-lg-4 text bold right"><b>Email ( อีเมลล์ )</b></div>
							<div class="col-lg-4 text center">
								<input type="text" name="email" id="small_field" class="form-control">
							</div>
							<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
						</div>
					</div>
					<br>
					<div class="row2">
						<div class="col-lg-12 text center">
							<input type="submit" value="ยืนยัน" class="btn btn-sm bck theme">
							<input type="reset" value="Reset" class="btn btn-sm btn-default">
						</div>
					</div>
				</form>
			</div> <!-- Row-container form -->
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>