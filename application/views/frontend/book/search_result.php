        <!-- Carousel -->
<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>"  width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <aside class="nav_left_menu" style="padding=1%">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;หมวดหมู่</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <ul class="nav nav-pills nav-stacked bck light" id="nav_padding">
                                            <?php foreach ($category_list as $category) : ?>
                                            <li>
                                                <a href="<?php echo $site . '/Show/Category/'. $category['id'] ?>">
                                                    <span class="glyphicon glyphicon-play">
                                                    </span><b> <?php echo $category['name'] ?></b>
                                                </a>
                                            </li>
                                            <?php endforeach ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                          <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-pencil">&nbsp;วิธีการสั่งซื้อ</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <li>เลือกหนังสือที่ต้องการ</li>
                                        <li>เลือก Check out!</li>
                                        <li>เข้าสู่ระบบ</li>
                                        <li>ชำระเงินตามจำนวนเงิน</li>
                                        <li>แจ้งชำระเงิน</li>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading bck light">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-thumbs-up">&nbsp;ติดตามเรา</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body nav text center">
                                        <img class="resize" src="<?php echo base_url() ?>images/bg/facebook.png" width="150" >
                                        <br>
                                        <img class="resize" src="<?php echo base_url() ?>images/bg/instagram.png" width="150" >
                                        <br>
                                        <img class="resize" src="<?php echo base_url() ?>images/bg/whatsapp.png" width="75" >
                                        <img class="resize" src="<?php echo base_url() ?>images/bg/line.png" width="75" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <a href="http://track.thailandpost.com/trackinternet/Default.aspx">
                            <img class="resize" src="<?php echo base_url() ?>images/bg/logopost2.jpg" width="194" >
                        </a>
                    </aside>
                </div>
                <div class="col-lg-6">
                    <div class="panel-no_border bs-example bs-example-tabs">
                        <ul id="myTab" class="nav nav-tabs">
                          <li class="active"><a href="#book_new" data-toggle="tab"><b>ผลการค้นหา "<?php echo $search_text ?>" :</b> พบ <?php echo $row ?> รายการ</a></li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                            <div class="tab-pane fade active in" id="book_new">
                                <?php if($search_result == NULL) : ?>
                                        <div class="row-container text center" id="margin">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h2>ไม่มีรายการหนังสือต้องการค้นหา</h2>
                                                </div>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <?php $count = 1; ?>
                                        <table width="100%">
                                            <thead>
                                                <th style="background-color: #fff;"></th>
                                                <th style="background-color: #fff;"></th>
                                                <th style="background-color: #fff;"></th>
                                            </thead>
                                            <tbody>
                                        <?php foreach($search_result AS $book) :?>
                                            <?php if( ($count-1) % 3 === 0) :?>
                                                <tr>
                                            <?php endif ?>
                                            <td width="33%">
                                                <div class="book_cart bck color" id="border">
                                                    <div class="cover text center">
                                                        <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>"
                                                        alt="<?php echo $book['title'] ?>" width="110">
                                                    </div>
                                                    <div class="detail">
                                                        <br>
                                                        <div class="text center"><?php echo $book['title'] ?></div>
                                                        <br>
                                                        <div class="bottom">
                                                            <div class="text right">
                                                                <b class="text right">ราคา : </b><?php echo $book['price'] ?> บาท
                                                            </div>
                                                            <br>
                                                            <form action="/BookStore/cart/add" method="POST">
                                                                <a href="<?php echo $site . '/Book/Item/'.$book['id'] ?>"
                                                                    class="btn btn-sm bck theme">รายละเอียด</a>
                                                                    <input type="submit" value="เลือกหนังสือ" class="btn btn-sm bck theme" id="add_to_cart">
                                                                    <input type="hidden" name="id" value="<?php echo $book['id'] ?>" id="hidden_add_cart">
                                                                </form>
                                                            </div>
                                                        </div>
                                                </div>
                                            </td>
                                            <?php if($count % 3 === 0) : ?>
                                                </tr>
                                            <?php endif ?>
                                            <?php $count++ ?>
                                        <?php endforeach ?>
                                        <?php $count-- ?>
                                        <?php if($count % 3 != 0) : ?>
                                            </tr>
                                        <?php endif ?>
                                        </tbody>
                                        </table>
                                        <div class="pagination">
                                            <?php echo $pagination_search ?>
                                        </div>
                                    <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="nav_right_menu">
                        <div class="member_form panel">
                            <div class="panel-heading bck color">
                                <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;สมาชิก</span></h4>
                            </div> <!-- header -->
                            <div class="panel-body">
                                <?php if($this->session->userdata('member_logged_in') == NULL) :?>
                                    <form action="/BookStore/login" method="POST">
                                        <label for="username">ชื่อผู้ใช้งาน</label>
                                        <input type="text" name="username" id="small_field" class="form-control" placeholder="ชื่อผู้ใช้งาน">
                                        <label for="password">รหัสผ่าน</label>
                                        <input type="password" name="password" id="small_field" class="form-control" placeholder="รหัสผ่าน">
                                        <br>
                                        <input type="checkbox" name="remember"> จดจำผู้ใช้งานตลอด</input>
                                        <div class="text center">
                                            <br>
                                            <input type="submit" value="Sign-in" class="btn btn-sm btn-default">
                                            <input type="reset" value="Reset" class="btn btn-sm btn-default">
                                        </div>
                                    </form>
                                    <div class="text center">
                                        <br>
                                        <a href="<?php echo $site ?>/Register"><b> สมัครสมาชิก</b></a>
                                        <a href="<?php echo $site ?>/recoveryPassword"><b> ลืมรหัสผ่าน</b></a>
                                    </div>
                                <?php else : ?>
                                <?php $member = $this->session->userdata('member_logged_in')?>
                                    <p><b>ยินดีต้อนรับ !</b></p>
                                    <p class="text center italic"><?php echo $member['username'] ?></p>
                                    <br>
                                    <p><a href="<?php echo $site ?>/member"><b>ข้อมูลส่วนตัว</b></a></p>
                                    <p><a href="<?php echo $site ?>/logout"><b>ออกจากระบบ</b></a></p>
                                <?php endif ?>
                                <br>
                            </div>
                        </div> <!-- member_form-->
                    </div> <!-- nav_right_menu -->

                    </div>
                </div>
                <div class="col-lg-1"></div>
</div>

<!-- Add to cart Comfirm Modal  -->
<div class="modal fade" id="modal_add_to_cart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bck dark">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">ยืนยันการสิ้นค้าลงตะกร้าสินค้า</h4>
            </div>
            <div class="modal-body text center">
                <h5>หยิบสินค้านี้ลงตะกร้าใช่หรือไม่</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">ยกเลิก</button>
                <button type="button" class="btn btn-sm bck theme white" id="confirm_add_to_cart">ยืนยัน</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- .modal -->

<script>
    $(document).ready(function() {
        $("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
        });
      });

    $(document).on("click", "#add_to_cart", function() {
        event.preventDefault();
        $("#modal_add_to_cart").modal('show');
        var id = $(this).siblings('#hidden_add_cart').val();
        $(document).on("click", "#confirm_add_to_cart", function() {
            $("#modal_add_to_cart").modal('hide');
            $.ajax({
                url: "/BookStore/cart/add",
                type: 'POST',
                data: {'id': id , 'ajax' : 'TRUE'},
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#cart-info').empty();
                    $('#cart-info').append(data);
                }
            });
        });
    });

    // $(document).on("click", ".pagination a", function() {
    //     event.preventDefault();
    //     var start = $(this).attr('href');
    //     start = start.substring(start.lastIndexOf("/")+1);
    //     // alert('Value : ' + start);
    //         $.ajax({
    //             url: "/BookStore/frontend/main/index/",
    //             type: 'POST',
    //             data: {'start': start , 'ajax' : 'TRUE'},
    //             cache: false,
    //             success: function(data, textStatus, jqXHR) {
    //                 // alert(data);
    //                 $('#book_new').empty();
    //                 $('#book_new').append(data);
    //             }
    //         });
    //     // });
    // });
</script>
