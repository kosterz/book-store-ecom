        <!-- Carousel -->
<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                	<aside class="nav_left_menu" style="padding=1%">
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;เมนู</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        	<p><span class="glyphicon glyphicon-pencil">&nbsp;<a href="<?php echo $site ?>/profile">แก้ไขข้อมูลส่วนตัว</a></span></p>
                                        	<p><span class="glyphicon glyphicon-lock">&nbsp;<a href="<?php echo $site ?>/changePW">เปลี่ยนรหัสผ่าน</a></span></p>
                                        	<p><span class="glyphicon glyphicon-shopping-cart">&nbsp;<a href="#">ประวัติการสั่งซื้อ</a></span></p>
                                    </div>
                                </div>
                            </div>
                       </aside>
                </div>
                <div class="col-lg-8">
                	<div class="panel" id="margin-top">
                		<div class="panel-heading bck color">
                			<h4><span class="glyphicon glyphicon-shopping-cart">&nbsp;</span>ประวัติการสั่งซื้อ</h4>
                		</div>
                		<div class="panel-body">
                			<div class="row-container" id="margin">
								<div class="account_info" style="padding-bottom: 1%;">
									<div class="row2">
										<div class="col-lg-12">
											<?php if( $order_list != NULL ) : ?>
											<table class="table table-striped" width="80%">
												<tr>
													<th width="5%" style="color:#707070;">#</th>
													<th width="30%" class="text-center" style="color:#707070;">สถานะใบสั่งซื้อ</th>
													<th width="30%" class="text-center" style="color:#707070;">วันที่สั่งซื้อ</th>
													<th width="15%" class="text-center" style="color:#707070;">ราคารวม</th>
													<th width="25%" class="text-center" style="color:#707070;">รายละเอียดเพิ่มเติม</th>
												</tr>
												<?php $count = 1; ?>
												<?php foreach ($order_list as $order): ?>
												<tr>
													<td><?php echo $count ?></td>
													<td class="text center">
													<?php if ($order['status_id'] == 1) : ?>
														<span class="label label-warning"><?php echo $order['status'] ?></span>
													<?php elseif ($order['status_id'] == 2) : ?>
														<span class="label label-info"><?php echo $order['status'] ?></span>
													<?php elseif ($order['status_id'] == 3) : ?>
														<span class="label label-success"><?php echo $order['status'] ?></span>
													<?php else : ?>
														<span class="label label-default"><?php echo $order['status'] ?></span>
													<?php endif ?>
													</td>
													<td class="text-center"><?php echo $order['order_date'] ?></td>
													<td class="text-center"><?php echo $order['total'] ?></td>
													<td class="text-center">
														<form action="/BookStore/member/order_history/detail?>">
															<input type="submit" value="รายละเอียด" class="btn btn-sm bck color">
															<input type="hidden" name="id" value="<?php echo $order['id'] ?>">
														</form>
													</td>
												</tr>
												<?php $count++; ?>
												<?php endforeach ?>
											</table>
											<?php else : ?>
											<div class="text center">
												<h4><b>ไม่มีรายการใบสั่งซื้อ</b></h4>
											</div>
											<?php endif ?>
										</div>
									</div>
                				</div>
                			</div>
                			<div class="row2">
                				<div class="col-lg-12">
                					<div class="text right">
                						<a href="/BookStore/member"><b>ย้อนกลับ</b></a>
									</div>
                				</div>
                			</div>
                		</div>
                	</div>
                	<div class="text right">
                		<b>**** ระบบจะทำการลบใบสั่งซื้อที่ไม่ได้รับการชำระเงินภายใน 1 สัปดาห์ออกจากระบบโดยอัตโนมัติ</b>
                	</div>
                </div>
				<div class="col-lg-1"></div>
            </div>
</div>

<script>
    $(document).ready(function() {
        $("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
        });
      });
</script>