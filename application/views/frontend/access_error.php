<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<div class="color dark">
					<h4><span class="glyphicon glyphicon-remove">&nbsp;</span><b>เกิดข้อผิดพลาดในการเข้าถึง URL !</b></h4>
				</div>
				<div class="row2">
					<div class="col-lg-12">
						<div class="alert alert-danger">
							<h4>
								<p><b>การเข้าถึง URL ไม่เป็นไม่ตามขั้นตอน กรุณาทำตามขั้นตอน <a href="/BookStore">กลับหน้าหลัก</a></b></p>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<script >
$(document).ready(function() {
	window.setTimeout(function() {
		window.location.href = '/BookStore';
	}, 10000);
});
</script>