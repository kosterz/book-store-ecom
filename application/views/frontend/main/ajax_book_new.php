<?php if(count($book_new)=== 0) : ?>
	<div class="row-container text center" id="margin">
		<div class="row">
			<div class="col-lg-12">
				<h2>ไม่มีรายการหนังสือ</h2>
			</div>
		</div>
	</div>
<?php else : ?>
	<?php $count = 1; ?>
	<table width="100%">
		<thead>
			<th style="background-color: #fff;"></th>
			<th style="background-color: #fff;"></th>
			<th style="background-color: #fff;"></th>
		</thead>
		<tbody>
			<?php foreach($book_new AS $book) :?>
			<?php if( ($count-1) % 3 === 0) :?>
			<tr>
			<?php endif ?>
			<td width="33%">
				<div class="book_cart bck color" id="border">
					<div class="cover text center">
						<img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>"
						alt="<?php echo $book['title'] ?>" width="110">
					</div>
					<div class="detail">
						<br>
						<div class="text center"><?php echo $book['title'] ?></div>
						<br>
						<div class="bottom">
							<div class="text right">
								<b class="text right">ราคา : </b><?php echo $book['price'] ?> บาท

							</div>
							<br>
							<form action="/BookStore/cart/add" method="POST">
								<a href="<?php echo $site . '/Book/Item/'.$book['id'] ?>"
									class="btn btn-sm bck theme">รายละเอียด</a>
									<input type="submit" value="เลือกหนังสือ" class="btn btn-sm bck theme" id="add_to_cart">
									<input type="hidden" name="id" value="<?php echo $book['id'] ?>" id="hidden_add_cart">
								</form>
							</div>
						</div>
					</div>
				</td>
				<?php if($count % 3 === 0) : ?>
			</tr>
		<?php endif ?>
		<?php $count++ ?>
	<?php endforeach ?>
	<?php $count-- ?>
	<?php if($count % 3 != 0) : ?>
</tr>
<?php endif ?>
<?php endif ?>
</tbody>
</table>
<div class="pagination">
	<?php echo $pagination_new_book ?>
</div>