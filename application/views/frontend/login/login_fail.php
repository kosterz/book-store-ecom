<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="breadcrumb bck theme color white">
				<div class="badge">
					<a href="/BookStore" class="active"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
					<b>&nbsp;>&nbsp;&nbsp;&nbsp;</b><span class="glyphicon glyphicon-pencil">&nbsp;</span>Register
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<div class="color theme">
					<h4><span class="glyphicon glyphicon-remove">&nbsp;</span><b>เกิดข้อผิดพลาด</b></h4>
				</div>
				<div class="rows">
					<div class="col-lg-3"></div>
					<div class="col-lg-6 panel">
						<br>
						<form action="/BookStore/login" method="POST">
							<label for="username">ชื่อผู้ใช้งาน</label>
							<input type="text" name="username" id="small_field" class="form-control" placeholder="ชื่อผู้ใช้งาน">
							<label for="password">รหัสผ่าน</label>
							<input type="password" name="password" id="small_field" class="form-control" placeholder="รหัสผ่าน">
							<br>
							<input type="checkbox" name="remember"> จดจำผู้ใช้งานตลอด</input>
							<div class="text center">
								<br>
								<input type="submit" value="Sign-in" class="btn btn-sm btn-default">
								<input type="reset" value="Reset" class="btn btn-sm btn-default">
							</div>
						</form>
						<br>
					</div>
					<div class="col-lg-3"></div>
				</div>
				<div class="row2">
					<div class="col-lg-12">
						<br>
						<div class="alert alert-danger">
							<?php echo validation_errors() ?>
							<?php
								if(isset($msg))
									echo '<p>'. $msg . '</p>';
							?>
						</div>
					</div>
				</div>

			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>