<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="breadcrumb bck theme color white">
				<div class="badge">
					<a href="/BookStore" class="active"><span class="glyphicon glyphicon-home"></span>&nbsp;Home</a>
					<b>&nbsp;>&nbsp;&nbsp;&nbsp;</b><span class="glyphicon glyphicon-pencil">&nbsp;</span>Register
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<div class="color theme">
					<h4><span class="glyphicon glyphicon-remove">&nbsp;</span><b>เข้าสู่ระบบเรียบร้อยแล้ว !</b></h4>
				</div>
				<div class="row2">
					<div class="col-lg-12">
						<div class="alert alert-success">
							<h4>
								<p><b>เข้าสู่ระบบเรียบร้อยแล้ว จะกลับสู่หน้าหลักภายใน 3 วินาที ถ้าไม่กลับ <a href="/BookStore">กรุณาคลิกที่นี้</a></b></p>
							</h4>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>

<script >
$(document).ready(function() {
	window.setTimeout(function() {
		window.location.href = '/BookStore';
	}, 3000);
});
</script>