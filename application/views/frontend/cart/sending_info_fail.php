<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
	<div class="row">
		<div class="col-lg-1"></div>
		<div class="col-lg-10">
			<div class="row-container" id="margin">
				<div class="color theme">
					<h4><span class="glyphicon glyphicon-remove">&nbsp;</span><b>เกิดข้อผิดพลาด</b></h4>
				</div>
				<div class="row2">
					<div class="col-lg-12">
						<div class="alert alert-danger">
							<?php echo validation_errors() ?>
							<?php
								if(isset($msg))
									echo '<p>'. $msg . '</p>';
							?>
						</div>
					</div>
				</div>
				<div class="rows">
					<div class="col-lg-12">
						<div class="text center">
							<a href="/BookStore/cart/checkout" class="btn btn-sm bck color color white ">กลับสู่หน้าข้อมูลตะกร้าสินค้า</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
</div>


<script>
$(document).ready(function() {
	$("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
});
</script>