<br>
<div class="col-lg-12">
	<?php if(count($cart) === 0) :?>
	<h4 class="text center">ยังไม่มีรายการสินค้าในตะกร้าสินค้า</h4>
<?php else : ?>
	<table class="table table-striped" width="100%">
		<thead>
			<th width="5%">#</th>
			<th width="43%">ชื่อหนังสือ<br></th>
			<th width="5%">ปก</th>
			<th width="7%">ราคา<br>(บาท)</th>
			<th width="20%">จำนวน / บันทึก</th>
			<th width="20%" class="text-center">ลบรายสินค้า</th>
		</thead>
		<?php $count = 1; ?>
		<tbody>
			<?php foreach ($cart as $book) : ?>
			<tr>
				<td><?php echo $count ?></td>
				<td><a href="/Book/Item/<?php echo $book['id'] ?>"><?php echo $book['name'] ?></a></td>
				<td>
					<a href="/BookStore/images/cover/<?php echo $book['options']['pic'] ?>"><span class="glyphicon glyphicon-picture"></span></a>
				</td>
				<td><?php echo $book['price'] ?></td>
				<td class="td_qty">
					<form action="/BookStore/cart/update" method="POST">
						<div class="col-lg-8" id="qty_div">
							<input type="number" name="qty" value="<?php echo $book['qty'] ?>" id="quantity_field" class="form-control">
						</div>
						<div class="col-lg-4">
							<input type="submit" value="บันทึก" class="btn btn-sm bck theme " id="update">
							<input type="hidden" name="row_id" value="<?php echo $book['rowid'] ?>" id="hidden_update">
						</div>
					</form>
				</td>
				<td>
					<div class="col-lg-1"></div>
					<div class="col-lg-5">
						<form action="/BookStore/cart/delete" method="POST">
							<input type="submit" value="ลบรายการ" class="btn btn-sm bck dark color white" id="delete">
							<input type="hidden" name="row_id" value="<?php echo $book['rowid'] ?>" id="hidden_delete">
						</form>
					</div>
					<div class="col-lg-1"></div>
				</td>
			</tr>
			<?php $count++; ?>
		<?php endforeach ?>
		<tr>
			<td colspan="4" class="text right">
				<b><span class="glyphicon glyphicon-book">&nbsp;</span>จำนวนหนังสือทั้งหมด : </b>
			</td>
			<td class="text right">
				<?php echo $this->cart->total_items() ?>
			</td>
			<td class="text center">
				<b>เล่ม</b>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text right">
				<b><span class="glyphicon glyphicon-credit-card">&nbsp;</span>ราคารวมทั้งหมด : </b>
			</td>
			<td class="text right">
				<?php echo $this->cart->total() ?>
			</td>
			<td class="text center">
				<b>บาท</b>
			</td>
		</tr>
	</tbody>
</table>
<?php endif; ?>
</div>
</div> <!-- ajax data-->