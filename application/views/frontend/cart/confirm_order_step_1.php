<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <aside class="nav_left_menu" style="padding=1%">
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;หมวดหมู่</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <?php if($this->session->userdata('member_logged_in') == NULL) :?>
                                        <form action="/BookStore/login" method="POST">
                                            <label for="username">ชื่อผู้ใช้งาน</label>
                                            <input type="text" name="username" id="small_field" class="form-control" placeholder="ชื่อผู้ใช้งาน">
                                            <label for="password">รหัสผ่าน</label>
                                            <input type="password" name="password" id="small_field" class="form-control" placeholder="รหัสผ่าน">
                                            <br>
                                            <input type="checkbox" name="remember"> จดจำผู้ใช้งานตลอด</input>
                                            <div class="text center">
                                                <br>
                                                <input type="submit" value="Sign-in" class="btn btn-sm btn-default">
                                                <input type="reset" value="Reset" class="btn btn-sm btn-default">
                                            </div>
                                        </form>
                                        <div class="text center">
                                            <br>
                                            <a href="<?php echo $site ?>/Register"><b> สมัครสมาชิก</b></a>
                                            <a href="<?php echo $site ?>/recoveryPassword"><b> ลืมรหัสผ่าน</b></a>
                                        </div>
                                        <?php else : ?>
                                        <?php $member = $this->session->userdata('member_logged_in')?>
                                        <p><b>ยินดีต้อนรับ !</b></p>
                                        <p class="text center italic"><?php echo $member['username'] ?></p>
                                        <br>
                                        <p><a href="<?php echo $site ?>/member/profile"><b>แก้ไขข้อมูลส่วนตัว</b></a></p>
                                        <p><a href="/BookStore/logout"><b>ออกจากระบบ</b></a></p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                       </aside>
                </div>

                <div class="col-lg-8">
                    <div class="panel" id="margin-top">
                        <div class="panel-heading bck color">
                            <h4><span class="glyphicon glyphicon-shopping-cart">&nbsp;</span>สถานที่ในการจัดส่ง</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="row-container" id="margin">
                        		<div class="sendinfo" style="padding-bottom: 1%;">
                        			<form action="<?php echo $site . '/checkout/summary' ?>" method="POST">
	                        			<div class="row2">
	                        				<br>
	                        				<div class="col-lg-4 text bold right"><b>Name ( ชื่อ )</b></div>
	                        				<div class="col-lg-4 text center">
	                        					<input type="text" name="name" id="small_field" class="form-control" value="<?php echo $member['name'] ?>">
	                        				</div>
	                        				<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
	                        			</div>
	                        			<div class="row2">
	                        				<br>
	                        				<div class="col-lg-4 text bold right"><b>Surname ( นามสกุล )</b></div>
	                        				<div class="col-lg-4 text center">
	                        					<input type="text" name="surname" id="small_field" class="form-control" value="<?php echo $member['surname'] ?>">
	                        				</div>
	                        				<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
	                        			</div>
	                        			<div class="row2">
	                        				<br>
	                        				<div class="col-lg-4 text bold right"><b>Address ( ที่อยู่ )</b></div>
	                        				<div class="col-lg-4 text center">
	                        					<textarea name="address" class="form-control" cols="30" rows="4"><?php echo $member['address'] ?></textarea>
	                        				</div>
	                        				<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
	                        			</div>

	                        			<div class="row2">
	                        				<br>
	                        				<div class="col-lg-4 text bold right"><b>Telephone No. ( เบอร์โทรศัพท์ )</b></div>
	                        				<div class="col-lg-4 text center">
	                        					<input type="text" name="tel_number" id="small_field" class="form-control" value="<?php echo $member['telNo'] ?>"> 							</div>
	                        					<div class="col-lg-4 text left bold">&nbsp;*** ข้อมูลที่ต้องการ ห้ามเว้นว่าง</div>
	                        			</div>
	                        			<div class="row2">
	                        				<br>
	                        				<div class="col-lg-12 text center">
												<input type="submit" value="ยืนยันข้อมูล" class="btn btn-sm bck color">
												<a href="/BookStore/cart">
													<span class="btn btn-sm btn-default">ยกเลิก</span>
												</a>
                                                <input type="hidden" name="status" value="2">
	                        				</div>
	                        			</div>
	                        		</form>
	                        	</div> <!-- sendinfo -->
	                        </div> <!-- Row container -->
	                    </div> <!-- panel body-->
	                </div> <!-- panel -->
	            </div> <!-- col / -->
                <div class="col-lg-1"></div>
            </div>
</div>

<script>
$(document).ready(function() {
	$("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
});
</script>