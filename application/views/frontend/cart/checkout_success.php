<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <aside class="nav_left_menu" style="padding=1%">
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;หมวดหมู่</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <?php $member = $this->session->userdata('member_logged_in')?>
                                        <p><b>ยินดีต้อนรับ !</b></p>
                                        <p class="text center italic"><?php echo $member['username'] ?></p>
                                        <br>
                                        <p><a href="<?php echo $site ?>/member/profile"><b>ข้อมูลส่วนตัว</b></a></p>
                                        <p><a href="/BookStore/logout"><b>ออกจากระบบ</b></a></p>
                                    </div>
                                </div>
                            </div>
                       </aside>
                </div>

                <div class="col-lg-8">
                    <div class="panel" id="margin-top">
                        <div class="panel-heading bck color">
                            <h4><span class="glyphicon glyphicon-shopping-cart">&nbsp;</span>ยืนยันข้อมูลทั้งหมด</h4>
                        </div>
                        <div class="panel-body">
                        	<div class="row-container" id="margin">
                        		<div class="sendinfo" style="padding-bottom: 1%;">
                        			<div class="row2">
                        				<div class="col-lg-12 color green">
                        					<br>
                        					<h4><b>บันทึกข้อมูลเสร็จสมบูรณ์</b></h4><br>
                        					หากมีข้อสงสัยเกี่ยวกับการชำระเงิน <a href="#"><b>กรุณาอ่านที่นี้</b></a> หรือ<br>
                        					<a href="/BookStore"><b>คลิกที่นี้</b></a> เพื่อกลับสู่หน้าหลัก
                        				</div>
                        			</div>
	                        	</div> <!-- sendinfo -->
	                        </div> <!-- Row container -->
	                    </div> <!-- panel body-->
	                </div> <!-- panel -->
	            </div> <!-- col / -->
                <div class="col-lg-1"></div>
            </div>
</div>

<script>
$(document).ready(function() {
	$("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
        items : 4,
        itemsDesktop : [1199,3],
        itemsDesktopSmall : [979,3]
    });
});
</script>