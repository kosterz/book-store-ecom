        <!-- Carousel -->
<div class="row-container">
            <div class="row">
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-10">
                    <div class="text-center bg">
                        <div id="carousel_hightlight">
                            <?php foreach ($carousel as $book) : ?>
                            <div class="item">
                                <a href="Book/Item/<?php echo $book['id'] ?>">
                                    <img src="<?php echo base_url() ?>images/cover/<?php echo $book['pic'] ?>" alt="<?php echo $book['title'] ?>" width="114" height="150">
                                </a>
                            </div>
                        <?php endforeach ?>
                        </div>
                    </div>
                </div>
            <div class="col-lg-1">&nbsp;</div>
            </div>
</div>
        <!-- nav bar 2 -->
<div class="row-container">
    <div class="row">
        <div class="col-lg-1">&nbsp;</div>
        <div class="col-lg-10 text-center bck dark" id="nav_padding">
            <div class="col-lg-3"><a href="#" class="active"><span class="glyphicon glyphicon-star"></span>&nbsp;Top Seller</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-book"></span>&nbsp;หนังสือ</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-barcode"></span>&nbsp;นิตยสาร</a></div>
            <div class="col-lg-3"><a href="#"><span class="glyphicon glyphicon-certificate"></span>&nbsp;New Entry</a></div>
        </div>
        <div class="col-lg-1">&nbsp;</div>
    </div>
</div>

<div class="row-container">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-2">
                    <aside class="nav_left_menu" style="padding=1%">
                            <div class="panel panel-default">
                                <div class="panel-heading bck color">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            <h4 class="text center" id="well_menu"><span class="glyphicon glyphicon-tags">&nbsp;หมวดหมู่</span></h4>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <?php if($this->session->userdata('member_logged_in') == NULL) :?>
                                        <form action="/BookStore/login" method="POST">
                                            <label for="username">ชื่อผู้ใช้งาน</label>
                                            <input type="text" name="username" id="small_field" class="form-control" placeholder="ชื่อผู้ใช้งาน">
                                            <label for="password">รหัสผ่าน</label>
                                            <input type="password" name="password" id="small_field" class="form-control" placeholder="รหัสผ่าน">
                                            <br>
                                            <input type="checkbox" name="remember"> จดจำผู้ใช้งานตลอด</input>
                                            <div class="text center">
                                                <br>
                                                <input type="submit" value="Sign-in" class="btn btn-sm btn-default">
                                                <input type="reset" value="Reset" class="btn btn-sm btn-default">
                                            </div>
                                        </form>
                                        <div class="text center">
                                            <br>
                                            <a href="<?php echo $site ?>/Register"><b> สมัครสมาชิก</b></a>
                                            <a href="<?php echo $site ?>/recoveryPassword"><b> ลืมรหัสผ่าน</b></a>
                                        </div>
                                        <?php else : ?>
                                        <?php $member = $this->session->userdata('member_logged_in')?>
                                        <p><b>ยินดีต้อนรับ !</b></p>
                                        <p class="text center italic"><?php echo $member['username'] ?></p>
                                        <br>
                                        <p><a href="<?php echo $site ?>/member/profile"><b>ข้อมูลส่วนตัว</b></a></p>
                                        <p><a href="<?php echo $site ?>/logout"><b>ออกจากระบบ</b></a></p>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                       </aside>
                </div>
                <div class="col-lg-8">
                    <div class="panel" id="margin-top">
                        <div class="panel-heading bck color">
                            <h4><span class="glyphicon glyphicon-shopping-cart">&nbsp;</span>รายการสินค้าในตะกร้าสินค้า</h4>
                        </div>
                        <!-- Item List -->
                        <div class="panel-body">
                            <div class="row-container" id="margin">
                                <div class="account_info" style="padding-bottom: 1%;">
                                    <div class="row2" id="ajax_content">
                                        <br>
                                        <div class="col-lg-12">
                                            <?php if(count($cart) === 0) :?>
                                                <h4 class="text center">ยังไม่มีรายการสินค้าในตะกร้าสินค้า</h4>
                                            <?php else : ?>
                                            <table class="table table-striped" width="100%">
                                                <thead>
                                                    <th width="5%">#</th>
                                                    <th width="43%">ชื่อหนังสือ<br></th>
                                                    <th width="5%">ปก</th>
                                                    <th width="7%">ราคา<br>(บาท)</th>
                                                    <th width="20%">จำนวน / บันทึก</th>
                                                    <th width="20%" class="text-center">ลบรายสินค้า</th>
                                                </thead>
                                                <?php $count = 1; ?>
                                                <tbody>
                                            <?php foreach ($cart as $book) : ?>
                                                <tr>
                                                    <td><?php echo $count ?></td>
                                                    <td><a href="/Book/Item/<?php echo $book['id'] ?>"><?php echo $book['name'] ?></a></td>
                                                    <td>
                                                        <a href="/BookStore/images/cover/<?php echo $book['options']['pic'] ?>"><span class="glyphicon glyphicon-picture"></span></a>
                                                    </td>
                                                    <td><?php echo $book['price'] ?></td>
                                                    <td class="td_qty">
                                                        <form action="/BookStore/cart/update" method="POST">
                                                            <div class="col-lg-8" id="qty_div">
                                                                <input type="number" name="qty" value="<?php echo $book['qty'] ?>" id="quantity_field" class="form-control">
                                                            </div>
                                                            <div class="col-lg-4">
                                                                <input type="submit" value="บันทึก" class="btn btn-sm bck theme " id="update">
                                                                <input type="hidden" name="row_id" value="<?php echo $book['rowid'] ?>" id="hidden_update">
                                                            </div>

                                                        </form>
                                                    </td>
                                                    <td>
                                                    <div class="col-lg-1"></div>
                                                            <div class="col-lg-5">
                                                                <form action="/BookStore/cart/delete" method="POST">
                                                                    <input type="submit" value="ลบรายการ" class="btn btn-sm bck dark color white" id="delete">
                                                                    <input type="hidden" name="row_id" value="<?php echo $book['rowid'] ?>" id="hidden_delete">
                                                                </form>
                                                            </div>
                                                            <div class="col-lg-1"></div>
                                                    </td>
                                                </tr>
                                                <?php $count++; ?>
                                            <?php endforeach ?>
                                                <tr>
                                                    <td colspan="4" class="text right">
                                                        <b><span class="glyphicon glyphicon-book">&nbsp;</span>จำนวนหนังสือทั้งหมด : </b>
                                                    </td>
                                                    <td class="text right">
                                                        <?php echo $this->cart->total_items() ?>
                                                    </td>
                                                    <td class="text center">
                                                        <b>เล่ม</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="4" class="text right">
                                                        <b><span class="glyphicon glyphicon-credit-card">&nbsp;</span>ราคารวมทั้งหมด : </b>
                                                    </td>
                                                    <td class="text right">
                                                        <?php echo $this->cart->total() ?>
                                                    </td>
                                                    <td class="text center">
                                                        <b>บาท</b>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <?php endif; ?>
                                        </div>
                                    </div> <!-- ajax data-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rows-container">
                        <div class="row2">
                    <?php if($this->session->userdata('member_logged_in') != NULL) :?>
                        <div class="col-lg-12 text right">
                            <form action="/BookStore/cart/checkout" method="POST">
                                <input type="submit" value="ยืนยันการสั่งสื้อ" class="btn btn-sm bck theme white">
                                <a href="/BookStore" class="btn btn-sm btn-default">กลับหน้าหลัก</a>
                                <input type="hidden" name="status" value="1">
                            </form>
                        </div>
                    <?php else : ?>
                        <div class="col-lg-12  text right">
                            <a href="/BookStore" class="btn btn-sm btn-default">กลับหน้าหลัก</a>
                            <br><br>
                            <span class="badge"><b>**** หมายเหตุ ในการยืนยันการสั่งซื้อ ต้องเข้าสู่ระบบก่อน</b></span>
                        </div>
                    <?php endif ?>
                        </div>
                    </div> <!-- Rows Container bottom button -->
                </div>
                <div class="col-lg-1"></div>
            </div>
</div>

<div class="modal fade" id="model_save_msg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bck dark">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">บันทึกข้อมูลเสร็จสมบูรณ์</h4>
            </div>
            <div class="modal-body text center">
                <h5>บันทึกข้อมูลเรียบร้อยแล้ว</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">ปิด</button>
            </div>
        </div> <!-- /.modal-content -->
    </div> <!-- /.modal-dialog -->
</div> <!-- .modal -->

<script>


    $(document).ready(function() {
        $("#carousel_hightlight").owlCarousel({
        autoPlay: 5000, //Set AutoPlay to 3 seconds
          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]
        });
    });

    $(document).on("change", "#quantity_field", function() {
        // var id = $(this).attr("id");
        var quantity = $(this).val();
        if($.isNumeric(quantity))
        {
            if(quantity <= 0)
            {
                $(this).val(1);
            }
            else
            {
                $(this).val(Math.floor(quantity));
            }
        }
        else
        {
            $(this).val(1);
        }
    });

    $(document).on("click", "#delete", function() {
        event.preventDefault();
        var rowid = $(this).siblings('#hidden_delete').val();
            $.ajax({
                url: "/BookStore/cart/delete",
                type: 'POST',
                data: {'row_id': rowid , 'ajax' : 'TRUE'},
                cache: false,
                success: function(data, textStatus, jqXHR) {
                    $('#ajax_content').empty();
                    $('#ajax_content').append(data);
                }
            });
        });

    $(document).on("click", "#update", function() {
        event.preventDefault();
        var rowid = $(this).siblings('#hidden_update').val();
        var qty = $(this).parent().siblings('#qty_div').children('input').val();
        // alert(rowid + '/n' + qty);
            $.ajax({
                url: "/BookStore/cart/update",
                type: 'POST',
                data: {'row_id': rowid , 'qty' : qty ,'ajax' : 'TRUE'},
                cache: false,
                success: function(data, textStatus, jqXHR) {
                     $("#model_save_msg").modal('show');
                    $('#ajax_content').empty();
                    $('#ajax_content').append(data);
                },
            });
        });
</script>