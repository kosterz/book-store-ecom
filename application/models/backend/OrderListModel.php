<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class OrderListModel extends CI_Model {

    // put your code here
    private $sql = 'tbl_book.*, (quantity * tbl_orderlist.price) AS total, quantity ,tbl_orderlist.price AS o_price';
    public function __construct() {
        parent::__construct();
    }

    public function listAll($limit, $start) {
        $rs = $this->db->get('tbl_orderList');
        return $rs->result_array();
    }

    public function findByBookID($id) {
        $query = $this->db->get_where('tbl_orderlist', array('book_id' => $id));
        $data = $query->result_array();
        $query->free_result();
        return $data;
    }

    public function findByOrderID($id) {
        $this->db->select($this->sql);
        $query = $this->db->get_where('tbl_orderlist', array('order_id' => $id));
        $data = $query->result_array();
        $query->free_result();
        return $data;
    }

    public function getBookFromOrderID($id) {
        $this->db->select($this->sql);
        $this->db->join('tbl_book', 'tbl_orderlist.book_id=tbl_book.id', 'inner');
        $query = $this->db->get_where('tbl_orderlist', array('order_id' => $id));
        $rs = $query->result_array();
        $query->free_result();
        return $rs;
    }

    public function get_sub_total($id)
    {
        $this->db->select('SUM(quantity * tbl_orderlist.price) AS total');
        $this->db->where('order_id',$id);
        $rs = $this->db->get('tbl_orderlist');
        return $rs->row_array();
    }

}
