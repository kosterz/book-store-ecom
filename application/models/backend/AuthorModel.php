<?php

/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
class AuthorModel extends CI_Model {

	// put your code here
	public function __construct() {
		parent::__construct ();
	}
	public function listAll() {
		$data = array ();
		$rs = $this->db->get ( 'tbl_author' );
		foreach ( $rs->result_array () as $row ) {
			$data [] = $row;
		}
		$rs->free_result ();
		return $data;
	}
	public function save($data) {
		$this->db->insert ( 'tbl_author', $data );
	}

	public function update($data, $id) {
		$this->db->where ( 'id', $id );
		$this->db->update ( 'tbl_author', $data );
	}

	public function findDuplicate($data)
	{
		$this->db->where($data);
		$rs = $this->db->get('tbl_author');
		if($rs->num_rows() > 0)
		{
			$rs->free_result();
			return TRUE;
		}
        return FALSE;
        $rs->free_result();
	}

	public function list_join($limit, $start)
	{
		$this->db->limit ( $limit, $start );
		$this->db->select('tbl_author.id ,name,surname,telNo, COUNT(tbl_write.book_id) AS total_book ');
		$this->db->join('tbl_write', 'tbl_author.id = tbl_write.author_id', 'left');
		$this->db->group_by('name');
		$authors = $this->db->get('tbl_author');
		return $authors->result_array();
	}

	public function findByID($id)
	{
		$this->db->where('id', $id);
		$rs = $this->db->get('tbl_author', 1);
		if($rs->num_rows() > 0)
		{
			return $rs->row_array();
		}
		else
		{
			return NULL;
		}
	}

	public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_author');
    }
}
