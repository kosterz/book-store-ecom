<?php


if (!defined('BASEPATH')) exit('No direct script access allowed');

class Staff_Model extends DataMapper {
	var $table = 'tbl_staff';
	var $has_one = array(
		"position" => array(
			'class' => 'position_model',
			'other_field' => 'position'
			)
		);
}