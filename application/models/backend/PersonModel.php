<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class PersonModel extends CI_Model {
	private $sql;
	public function __construct() {
		parent::__construct ();
		// $this->sql = 'tbl_book.id , title, detail, isbn, pic , page, price ,stock ,
		// 		cat_id, tbl_category.id AS cate_id , tbl_category.name AS category,
		// 		author_id, tbl_author.id AS a_id, tbl_author.name AS author';
	}
	public function save($data)
	{
		$this->db->insert ( 'tbl_person', $data );
		return $this->db->insert_id();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('tbl_person');
	}

	public function update($data,$id)
	{
		$this->db->where('id',$id);
		$this->db->update('tbl_person', $data);
	}
}

/**
 *END of file : PersonModel.php
 *
 * location at : ./models/backend/PersonModel.php
 */