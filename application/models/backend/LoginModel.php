<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LoginModel
 *
 * @author KOsTErZ
 */
class LoginModel extends CI_Model {
    private $sql = 'tbl_staff.id, tbl_person.name , surname, username ,
    address, telNo, email , personal_id , tbl_position.name AS position';
    public function __construct() {
        parent::__construct();
    }

    public function login($username, $password, $passcode) {
        $this->db->select($this->sql);
        $this->db->join('tbl_person','tbl_staff.person_id=tbl_person.id','inner');
        $this->db->join('tbl_position','tbl_staff.position_id=tbl_position.id','inner');
        $rs = $this->db->get_where('tbl_staff', array(
            'username' => $username,
            'password' => $password,
            'passcode' => $passcode));
        return $rs->result_array();
    }

}
