<?php

/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */

/**
 * Description of book
 *
 * @author KOsTErZ
 */
class BookModel extends CI_Model {

	// put your code here
	private $sql;
	public function __construct() {
		parent::__construct ();
		$this->sql = 'tbl_book.id , title, detail, isbn, pic , page, price ,stock ,
				cat_id, tbl_category.id AS cate_id , tbl_category.name AS category,date_add';
	}
	public function listAll($limit, $start) {
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_category', 'tbl_book.cat_id=tbl_category.id', 'inner' );
		$this->db->limit ( $limit, $start );
		$this->db->order_by('tbl_book.id', 'desc');
		$rs = $this->db->get ( 'tbl_book' );
		return $rs->result_array ();
	}
	public function save($data) {
		$this->db->insert ( 'tbl_book', $data );
		return $this->db->insert_id();
	}
	public function update($data, $id) {
		$this->db->where ( 'id', $id );
		$this->db->update ( 'tbl_book', $data );
	}
	public function findByID($id) {
		$data = array ();
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_category', 'tbl_book.cat_id=tbl_category.id', 'inner' );
		$query = $this->db->get_where ( 'tbl_book', array (
				'tbl_book.id' => $id
		) );
		if ($query->num_rows () > 0) {
			$data = $query->row_array ();
		}
		$query->free_result ();
		return $data;
	}
	public function delete($id) {
		$this->db->where ( 'id', $id );
		$this->db->delete ( 'tbl_book' );
	}
}
