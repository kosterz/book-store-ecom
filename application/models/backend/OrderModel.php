<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OrderModel
 *
 * @author KOsTErZ
 */
class OrderModel extends CI_Model {
    private $sql = 'tbl_order.id AS order_id , order_date, tbl_status.id AS status_id,tbl_status.name AS status,
                tbl_person.*,tbl_sendinfo.name AS sendinfo_name,tbl_sendinfo.surname AS sendinfo_surname,
                tbl_sendinfo.address AS sendinfo_address,tbl_sendinfo.telNo AS sendinfo_tel';
    public function __construct() {
        parent::__construct();

    }

    public function listAll($limit, $start) {
        $this->db->select('tbl_order.id , tbl_status.name AS status, tbl_member.username AS member, order_date');
        $this->db->join('tbl_status', 'tbl_order.status_id=tbl_status.id', 'inner');
        $this->db->join('tbl_member', 'tbl_order.member_id=tbl_member.id', 'inner');
        $this->db->limit($limit, $start);
        $rs = $this->db->get('tbl_order');
        if (count ( $rs ) > 0) {
            return $rs->result_array ();
        } else {
            return null;
        }
    }

    public function findByIDJoin($id) {
        $this->db->select($this->sql);
        $this->db->join('tbl_status', 'tbl_order.status_id=tbl_status.id', 'inner');
        $this->db->join('tbl_member', 'tbl_order.member_id=tbl_member.id', 'inner');
        $this->db->join('tbl_sendinfo', 'tbl_sendinfo.order_id=tbl_order.id', 'inner');
        $this->db->join('tbl_person', 'tbl_member.person_id=tbl_person.id', 'inner');
        $query = $this->db->get_where('tbl_order', array('tbl_order.id' => $id));
        $rs = $query->result_array();
        $query->free_result();
        return $rs;
    }

    public function findByID($id) {
        return $this->db->get_where('tbl_order', array('id' => $id))->row_array();
    }

    public function update($data, $id) {
        $this->db->where('id',$id);
        $this->db->update('tbl_order',$data);
    }

}
