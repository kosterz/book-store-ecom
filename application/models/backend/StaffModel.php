<?php

/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
/**
 * Description of StaffModel
 *
 * @author KOsTErZif (!defined('BASEPATH')) exit('No direct script access allowed');
 */
class StaffModel extends CI_Model {


	private $sql = 'tbl_staff.id, username, position_id, tbl_position.name AS position ,
	tbl_person.name, surname, address, telNo, email ,personal_id, person_id AS pid';
/**
 * Join ข้อมูลเดียวกัน
 * Select same statement
 */
	public function __construct() {
		parent::__construct ();
	}
	public function listAll($limit,$start) {
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_staff.person_id=tbl_person.id', 'inner' );
		$this->db->join ( 'tbl_position', 'tbl_staff.position_id=tbl_position.id', 'inner' );
		$this->db->limit ( $limit, $start );
		$rs = $this->db->get ( 'tbl_staff' );
		if (count ( $rs ) > 0) {
			return $rs->result_array ();
		} else {
			return null;
		}
	}
	public function update($data, $id) {
		$this->db->where ( 'id', $id );
		$this->db->update ( 'tbl_staff', $data );
	}

	public function findByID($id){
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_staff.person_id=tbl_person.id', 'inner' );
		$this->db->join ( 'tbl_position', 'tbl_staff.position_id=tbl_position.id', 'inner' );
		$this->db->where('tbl_staff.id',$id);
		$this->db->limit(1);
		$query = $this->db->get('tbl_staff');
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		return NULL;
	}

	public function save($data){
		$this->db->insert('tbl_staff',$data);
		return TRUE;
	}

	public function delete($id)
	{
		$this->db->where($id);
		$this->db->delete('tbl_staff');
		return $this->db->last_query();
	}

	public function findByPID($pid){
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_staff.person_id=tbl_person.id', 'inner' );
		$this->db->join ( 'tbl_position', 'tbl_staff.position_id=tbl_position.id', 'inner' );
		$this->db->where('tbl_staff.personal_id',$pid);
		$this->db->limit(1);
		$query = $this->db->get('tbl_staff');
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		return NULL;
	}
}
?>