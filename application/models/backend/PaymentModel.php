<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PaymentModel
 *
 * @author KOsTErZ
 */
class PaymentModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function listAll($limit,$start)
    {
    	$this->db->limit ( $limit, $start );
        $rs = $this->db->get('tbl_payment');
        return $rs->result_array();
    }

    public function findByID($id)
    {
    	$this->db->where('id' , $id);
    	$rs = $this->db->get('tbl_payment');
    	if($rs->num_rows() > 0)
    	{
    		return $rs->row_array();
    	}
    	else
    	{
    		return NULL;
    	}
    }

    public function delete($id)
    {
    	$this->db->where('id', $id);
    	$this->db->delete('tbl_payment');
    }

}
