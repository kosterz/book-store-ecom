<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class WriteModel extends CI_Model {
	public function __construct() {
		parent::__construct ();
	}

	public function save($data)
	{
		$this->db->insert_batch('tbl_write', $data);
	}

	public function findByBookID($id)
	{
		$this->db->where('book_id',$id);
		return $this->db->get('tbl_write')->result_array();
	}

	public function deleteByBookID($id)
	{
		$this->db->where('book_id',$id);
		$this->db->delete('tbl_write');
		return $this->db->last_query();
	}
}

?>