<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of CatagoryModel
 *
 * @author KOsTErZ
 */
class CategoryModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function listAll() {
        $data = array();
        $rs = $this->db->get('tbl_category');
        foreach ($rs->result_array() as $row) {
            $data[] = $row;
        }
        $rs->free_result();
        return $data;
    }

    public function save($data) {
        $this->db->set('name', $data);
        $this->db->insert('tbl_category');
    }

    public function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('tbl_category');
    }

    public function list_join($limit, $start) {
        $data = array();
        $this->db->limit ( $limit, $start );
        $this->db->select('tbl_category.id , name , COUNT(tbl_book.id) AS total_book');
        $this->db->join('tbl_book', 'tbl_category.id = tbl_book.cat_id', 'left');
        $this->db->group_by('tbl_category.id');
        $rs = $this->db->get('tbl_category');
        foreach ($rs->result_array() as $row) {
            $data[] = $row;
        }
        $rs->free_result();
        return $data;
    }

    public function findByID($id)
    {
        $this->db->select('name');
        $this->db->where('id', $id);
        $rs = $this->db->get('tbl_category', 1);
        // echo $this->db->last_query();
        // echo $rs->num_rows();
        return $rs->row_array();
    }

    public function update($data,$id)
    {
        $this->db->where('id', $id);
        $this->db->update('tbl_category', array('name' => $data));
    }
}
