<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of StatusModel
 *
 * @author KOsTErZ
 */
class StatusModel extends CI_Model {

    public function __construct() {
        parent::__construct();
    }

    public function listAll() {
        $data = array();
        $rs = $this->db->get('tbl_status');
        foreach ($rs->result_array() as $row) {
            $data[] = $row;
        }
        $rs->free_result();
        return $data;
    }

}
