<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberModel
 *
 * @author KOsTErZ
 */
class MemberModel extends CI_Model {

	private $sql = 'tbl_member.id, username, name, surname, address, telNo, email , person_id AS pid';

    public function __construct() {
        parent::__construct();
    }

    public function listAll($limit,$start) {
       $this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_member.person_id=tbl_person.id', 'inner' );
		$this->db->limit ( $limit, $start );
		$rs = $this->db->get ( 'tbl_member' );
		if (count ( $rs ) > 0) {
			return $rs->result_array ();
		} else {
			return null;
		}
    }

    public function findByID($id){
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_member.person_id=tbl_person.id', 'inner' );
		$this->db->where('tbl_member.id',$id);
		$this->db->limit(1);
		$query = $this->db->get('tbl_member');
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		return NULL;
	}

	public function delete($id)
	{
		$this->db->where($id);
		$this->db->delete('tbl_member');
		return $this->db->last_query();
	}

}
