<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class BookModel extends CI_Model {
	private $sql;
	public function __construct() {
		parent::__construct ();
		$this->sql = 'tbl_book.id , title, detail, isbn, pic , page, price ,stock ,
		cat_id, tbl_category.id AS cate_id , tbl_category.name AS category, date_add';
	}
	public function findByID($id) {
		$data = array ();
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_category', 'tbl_book.cat_id=tbl_category.id', 'inner' );
		$this->db->limit(1);
		$query = $this->db->get_where ( 'tbl_book', array (
			'tbl_book.id' => $id
			) );
		if ($query->num_rows () > 0) {
			$data = $query->row_array ();
		}
		$query->free_result ();
		return $data;
	}

	public function list_new_book($limit,$start = 0)
	{
		$this->db->limit($limit,$start);
		$this->db->order_by('date_add', 'desc');
		return $this->db->get('tbl_book')->result_array();
	}

	public function findAuthorByID($id)
	{
		$this->db->select('name,surname');
		$this->db->join('tbl_write', 'tbl_author.id = tbl_write.author_id', 'inner');
		$this->db->where('tbl_write.book_id', $id);
		$result = $this->db->get('tbl_author')->result_array();
		return $result;
	}

	public function list_by_category($limit, $start = 0, $id)
	{
		$this->db->limit($limit,$start);
		$this->db->order_by('date_add', 'desc');
		$this->db->where('cat_id', $id);
		return $this->db->get('tbl_book')->result_array();
	}

	public function count_row($id)
	{
		$this->db->where('cat_id', $id);
		return $this->db->get('tbl_book')->num_rows();
	}
	public function count_all_row()
	{
		return $this->db->get('tbl_book')->num_rows();
	}

	public function search($find_from, $text,$limit, $start)
	{
		$this->db->select('tbl_book.*');
		$this->db->limit($limit,$start);
		$this->db->join('tbl_write', 'tbl_book.id=tbl_write.book_id', 'inner');
		$this->db->join('tbl_author', 'tbl_write.author_id=tbl_author.id', 'inner');
		$this->db->like($find_from, $text);
		$this->db->order_by('date_add', 'desc');
		$this->db->group_by('tbl_book.id');
		$result = $this->db->get('tbl_book');
		// echo $this->db->last_query();
		if($result->num_rows() > 0)
		{
			return $result->result_array();
		}
		else
		{
			return NULL;
		}
	}

	public function count_search_row($find_from, $text)
	{
		$this->db->select('tbl_book.*');
		$this->db->join('tbl_write', 'tbl_book.id=tbl_write.book_id', 'inner');
		$this->db->join('tbl_author', 'tbl_write.author_id=tbl_author.id', 'inner');
		$this->db->like($find_from, $text);
		$this->db->order_by('date_add', 'desc');
		$this->db->group_by('tbl_book.id');
		$result = $this->db->get('tbl_book');
		// echo $this->db->last_query();
		if($result->num_rows() > 0)
		{
			return $result->num_rows;
		}
		else
		{
			return NULL;
		}
	}
}
?>