<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class OrderModel extends CI_Model {
    public function __construct() {
        parent::__construct();

    }

    public function save($member_id)
    {
    	$this->db->insert('tbl_order',array('member_id' => $member_id));
    	return $this->db->             insert_id();
    }

    public function save_order_list($data)
    {
    	$this->db->insert_batch('tbl_orderlist',$data);
    }

    public function list_order_not_pay($id)
    {
        $this->db->select('tbl_order.id, SUM(quantity * tbl_orderlist.price) AS total');
        $this->db->join('tbl_orderlist', 'tbl_order.id = tbl_orderlist.order_id', 'inner');
        $this->db->where('member_id', $id);
        $this->db->where('status_id', 1);
        $this->db->group_by("tbl_order.id");
        $result = $this->db->get('tbl_order')->result_array();
        if(count($result) == 0)
        {
            $result = NULL;
        }
        return $result;
    }

    public function list_all($id)
    {
        $this->db->select('tbl_order.id, tbl_status.name AS status ,status_id, order_date , SUM(quantity * tbl_orderlist.price) AS total');
        $this->db->join('tbl_orderlist', 'tbl_order.id = tbl_orderlist.order_id', 'inner');
        $this->db->join('tbl_status', 'tbl_order.status_id = tbl_status.id', 'inner');
        $this->db->where('member_id', $id);
        $this->db->group_by("tbl_order.id");
        $result = $this->db->get('tbl_order')->result_array();
        if(count($result) == 0)
        {
            $result = NULL;
        }
        return $result;
    }
}

 ?>