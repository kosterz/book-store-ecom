<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class SendinfoModel extends CI_Model {
		public function __construct() {
			parent::__construct();

		}

		public function save($data)
		{
			$this->db->insert('tbl_sendinfo', $data);
		}
	}

?>