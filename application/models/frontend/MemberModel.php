<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MemberModel
 *
 * @author KOsTErZ
 */
class MemberModel extends CI_Model {

	private $sql = 'tbl_member.id, username, password, name, surname, address, telNo, email , person_id AS pid';

    public function __construct() {
        parent::__construct();
    }

    public function findByID($id){
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_member.person_id=tbl_person.id', 'inner' );
		$this->db->where('tbl_member.id',$id);
		$this->db->limit(1);
		$query = $this->db->get('tbl_member');
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		return NULL;
	}

	   public function findByUser($username){
		$this->db->select ( $this->sql );
		$this->db->join ( 'tbl_person', 'tbl_member.person_id=tbl_person.id', 'inner' );
		$this->db->where('tbl_member.username',$username);
		$this->db->limit(1);
		$query = $this->db->get('tbl_member');
		if($query->num_rows() > 0)
		{
			return $query->row_array();
		}
		return NULL;
	}

	public function save($data){
		$this->db->insert('tbl_member',$data);
		return TRUE;
	}
	public function update($data,$id)
	{
		$this->db->where('tbl_member.id', $id);
		$this->db->update('tbl_person INNER JOIN tbl_member ON tbl_person.id=tbl_member.person_id', $data);
		return $this->db->last_query();
	}

	public function updatePW($data, $id)
	{
		$this->db->where('tbl_member.id', $id);
		$this->db->update('tbl_member', array('password' => $data));
		return $this->db->last_query();
	}
}
