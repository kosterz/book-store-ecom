<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @Entity
 * @Table(name="tbl_staff")
 */
class Staff_Model
{
    /**
     * @Id
     * @Column(type="integer")
     * @GeneratedValue
     */
    private $id;

    /**
     * @Column(type="integer", name="person_id", nullable=false)
     */
    private $person_id;

    /**
     * @Column(type="string", name="personal_id", nullable=false)
     */
    private $personal_id;

    /**
     * @Column(type="string", name="username", length=255)
     */
    private $username;

    /**
     * @Column(type="string", name="password", length=255)
     */
    private $password;

    /**
     * @Column(type="string", name="passcode", length=4)
     */
    private $passcode;

    /**
     * @Column(type="integer", name="position_id", unique=false, nullable=false)
     */
    private $position_id;

    /**
     * @param mixed $passcode
     */
    public function setPasscode($passcode)
    {
        $this->passcode = $passcode;
    }

    /**
     * @return mixed
     */
    public function getPasscode()
    {
        return $this->passcode;
    }

    /**
     * @param mixed $id
     */

    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $person_id
     */
    public function setPersonId($person_id)
    {
        $this->person_id = $person_id;
    }

    /**
     * @return mixed
     */
    public function getPersonId()
    {
        return $this->person_id;
    }

    /**
     * @param mixed $personal_id
     */
    public function setPersonalId($personal_id)
    {
        $this->personal_id = $personal_id;
    }

    /**
     * @return mixed
     */
    public function getPersonalId()
    {
        return $this->personal_id;
    }

    /**
     * @param mixed $position_id
     */
    public function setPositionId($position_id)
    {
        $this->position_id = $position_id;
    }

    /**
     * @return mixed
     */
    public function getPositionId()
    {
        return $this->position_id;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }


}
