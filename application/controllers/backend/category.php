<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller {

    private $title;
    private $site = '/BookStore/Backend/setting/category';

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'backend/CategoryModel',
            'backend/BookModel',
        ));
        $this->load->library(array(
            'form_validation',
        ));
        $this->title = 'Staff Tools -> การจัดการหมวดหมู่';

    }
    function index() {
    	$data['title'] = $this->title;
        // Set pagination
    	$this->pagination->initialize(array(
    		'base_url' => base_url() . '/Backend/setting/category',
    		'total_rows' => $this->db->count_all('tbl_category'),
    		'uri_segment' => 4,
            'first_url' => '0',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'first_link' => 'ไปหน้าแรก',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'last_link' => 'ไปหน้าสุดท้าย',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><li class="active"><a href="#"">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>'
    		));
        /*
         * List book
         * 1 parameter per_page value at config/pagination.php
         * 2 uri segment
         */
        // echo $this->pagination->create_links();
        $data['pagination_category']  = $this->pagination->create_links();
        $data['category_list'] = $this->CategoryModel->list_join($this->config->item('per_page'), $this->uri->segment(4));
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/category/show_category', $data);
        $this->load->view('template/footer_backend');
    }

    public function show_edit_category()
    {
    	$id = $this->uri->segment(4);
    	$data['title'] = $this->title;
    	$data['category'] = $this->CategoryModel->findByID($id);
    	$data['id'] = $id;
    	$this->load->view('template/header_backend', $data);
        $this->load->view('backend/category/edit_category', $data);
        $this->load->view('template/footer_backend');
    }

    public function update_category()
    {
    	$data['title'] = $this->title;
    	$id = $this->input->post('id');
    	$this->form_validation->set_rules('cate_name', 'ชื่อหมวดหมู่', 'required|is_unique[tbl_category.name]');
        if($this->input->post('status') != '1')
        {
        	$this->load->view('template/header_backend',$data);
            $this->load->view('backend/access_error', $data);
            $this->load->view('template/footer_backend');
        }
        else if ($this->form_validation->run() == FALSE)
        {
            $data ['site'] = $id;
            $this->load->view('template/header_backend',$data);
            $this->load->view('backend/category/update_fail', $data);
            $this->load->view('template/footer_backend');
        }
        else
        {
            $cate_name = $this->input->post('cate_name');
            $data ['cate_name'] = $cate_name;
            $data ['site'] = site_url() . 'Backend/setting/category';
            $this->CategoryModel->update($cate_name,$id);
            $this->load->view('template/header_backend',$data);
            $this->load->view('backend/category/update_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function show_delete_confirm()
    {
    	$id = $this->input->post('id');
    	$data['title'] = $this->title;
        if($this->input->post('status') != '1')
        {
        	$this->load->view('template/header_backend',$data);
            $this->load->view('backend/access_error', $data);
            $this->load->view('template/footer_backend');
        }
        else
        {
        	$this->CategoryModel->delete($id);
        	$this->load->view('template/header_backend',$data);
            $this->load->view('backend/category/delete_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function form_add_category() {
        $data['title'] = $this->title . ' -> Create new category';
        $data ['categoryList'] = $this->CategoryModel->listAll();
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/category/add_category', $data);
        $this->load->view('template/footer_backend');
    }

    public function add_category() {
        $this->form_validation->set_rules('cate_name', 'ชื่อหมวดหมู่', 'required|is_unique[tbl_category.name]');
        if ($this->form_validation->run() == FALSE) {
            $data ['site'] = $this->site.'/newCate';
            $this->load->view('template/header_backend');
            $this->load->view('backend/category/add_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
            $cate_name = $this->input->post('cate_name');
            $data['cate_name'] = $cate_name;
            $data['site'] = $this->site;
            $this->CategoryModel->save($cate_name);
            $this->load->view('template/header_backend');
            $this->load->view('backend/category/add_category_success', $data);
            $this->load->view('template/footer_backend');
        }
    }
}


 ?>