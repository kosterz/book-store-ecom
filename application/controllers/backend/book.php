<?php

/**
 * Description of book
 *
 * @author KOsTErZ
 *
 * Autocomplete for CodeIgniter
 * @property CI_Loader $load
 * @property CI_Form_validation $form_validation
 * @property CI_Input $input
 * @property CI_Email $email
 * @property CI_DB_active_record $db
 * @property CI_DB_forge $dbforge
 * @property CI_Table $table
 * @property CI_Session $session
 * @property CI_FTP $ftp
 * @property CI_Encrypt $encrypt
 * @property CI_Parser $parser
 *
 *
 * **/
class Book extends CI_Controller {

    private $title;
    private $site = '/BookStore/Backend/BookManage/';

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'backend/CategoryModel',
            'backend/BookModel',
            'backend/AuthorModel',
            'backend/OrderListModel',
            'backend/WriteModel',
        ));
        $this->load->helper(array(
            'date',
        ));
        $this->load->library(array(
            'form_validation',
            'upload',
        ));
        $this->title = 'Staff Tools -> การจัดการข้อมูลหนังสือ';

    }

    public function index() {
        $data['title'] = $this->title;
        // Set pagination
        $this->pagination->initialize(array(
            'base_url' => base_url() . '/backend/book/index',
            'total_rows' => $this->db->count_all('tbl_book'),
            'uri_segment' => 4,
            'first_url' => '0',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'first_link' => 'ไปหน้าแรก',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'last_link' => 'ไปหน้าสุดท้าย',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><li class="active"><a href="#"">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>'
            ));
        /*
         * List book
         * 1 parameter per_page value at config/pagination.php
         * 2 uri segment
         */
        // echo $this->pagination->create_links();
        $data['rs'] = $this->BookModel->listAll($this->config->item('per_page'), $this->uri->segment(4));
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/book/book_index', $data);
        $this->load->view('template/footer_backend');
    }

    public function update_book() {
        $id = $this->input->post('id');
        if ($this->form_validation->run('updateBook') == FALSE) {
            $data ['site'] = 'bookItem/' . $id;
            $data['title'] = $this->title . ' -> Edit Book';
            $data['id'] = $id;
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/update_fail', $data);
            $this->load->view('template/footer_backend');
        } else if ($this->upload->do_upload('pic' , TRUE)) {
            $upload_data = $this->upload->data();
            $file_name = $upload_data ['file_name'];
            // log_message ( 'info', print ($file_name)  );
            $pic = $file_name;
            $this->updateData($pic);
        } else if ("You did not select a file to upload." != $this->upload->display_errors('', '')) {
            $data ['error'] = array(
                'msg' => $this->upload->display_errors()
            );
            $data['title'] = $this->title . ' -> Edit Book';
            $data ['site'] = 'bookItem';
            $data['id'] = $id;
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/update_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
            $pic = $this->input->post('pic');
            $this->updateData($pic);
        }
    }

    /**
     * Edit book
     */
    public function show_book_detail() {
        $id = $this->uri->segment(4);
        $result = $this->BookModel->findByID($id); //Find Book by ID
        if (count($result) > 0) {
            $data['book'] = $result;
            $categories = $this->CategoryModel->listAll();
            $authors = $this->AuthorModel->listAll();
            $categorieList = array();
            $authorList = array();
            // Renew array option['id'] => name /////////////////////
            foreach ($categories as $category) {
                $categorieList[$category['id']] = $category['name'];
            }
            foreach ($authors as $author) {
                $authorList[$author['id']] = $author['name'] . ' ' . $author['surname'];
            }
            /////////////////////////////////////////////////////////
            $data['authorList'] = $this->WriteModel->findByBookID($id);
            $data['category'] = $categorieList;
            $data['author'] = $authorList;
            // log_message('info' , var_dump($data['book']));
            $data['title'] = $this->title . '-> Edit Book';

            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/edit_book', $data);
            $this->load->view('template/footer_backend');
        } else {
            $data['title'] = $this->title . '-> Edit Book';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/not_found', $data);
            $this->load->view('template/footer_backend');
        }
    }

    /**
     * Show add book form input
     */
    public function form_add_book() {
        $data['site'] = $this->site;
        $data ['categoryList'] = $this->CategoryModel->listAll();
        $data ['authorList'] = $this->AuthorModel->listAll();
        $data['title'] = $this->title . ' -> Create new book';
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/book/add_book', $data);
        $this->load->view('template/footer_backend');
    }

    /**
     * Show add category form
     */
    public function form_add_category() {
        $data['title'] = $this->title . ' -> Create new category';
        $data ['categoryList'] = $this->CategoryModel->listAll();
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/book/add_cate', $data);
        $this->load->view('template/footer_backend');
    }

    public function form_add_author() {
        $data['title'] = $this->title . ' -> Add new author';
        $data['return_to'] = $this->uri->segment(3);
        if($data['return_to'] === 'bookItem')
        {
            $data['book_id'] = $this->uri->segment(4);
        }
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/book/add_author');
        $this->load->view('template/footer_backend');
    }

    /**
     * add category
     */
    public function add_category() {
        $this->form_validation->set_rules('cate_name', 'ชื่อหมวดหมู่', 'required|is_unique[tbl_category.name]');
        if ($this->form_validation->run() == FALSE) {
            $data ['site'] = 'cateForm';
            $this->load->view('template/header_backend');
            $this->load->view('backend/category/add_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
            $cate_name = $this->input->post('cate_name');
            $data ['cate_name'] = $cate_name;
            $data ['site'] = site_url() . '/backend/book/form';
            $this->CategoryModel->save($cate_name);
            $this->load->view('template/header_backend');
            $this->load->view('backend/category/add_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function add_author() {
        $data['title'] = $this->title.' -> เพิ่มผู้แต่งหนังสือ';
        if($this->input->post('book_id')!= NULL)
        {
            $data ['site'] = site_url() . 'Backend/BookManage/bookItem/'. $this->input->post('book_id') . '/AddAuthor';
        }
        else
        {
            $data ['site'] = site_url() . 'Backend/BookManage/CreateNewBook/AddAuthor';
        }

        if ($this->form_validation->run('authorForm') === FALSE)
        {
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/author/add_fail', $data);
            $this->load->view('template/footer_backend');
        }
        else if ($this->AuthorModel->findDuplicate(
                array(
                    'name' => $this->input->post('name'),
                    'surname' => $this->input->post('surname')
                    )
                ) === TRUE
            )
        {
            $data['msg'] = "มีข้อมูลผู้แต่ง <br> ชื่อ - นามสกุล : "
            . $this->input->post('name') . ' ' . $this->input->post('surname') . ' อยู่ในระบบแล้ว';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/author/add_fail', $data);
            $this->load->view('template/footer_backend');
        }
        else
        {
            $author = array(
                'name' => $this->input->post('name'),
                'surname' => $this->input->post('surname'),
                'telNo' => $this->input->post('telNo'),
                'email' => $this->input->post('email'),
            );
            $data['title'] = $this->title;
            $data['author'] = $author;
            if($this->input->post('book_id')!= NULL)
            {
                $data ['site'] = site_url() . 'Backend/BookManage/bookItem/'. $this->input->post('book_id');
            }
            else
            {
                $data ['site'] = site_url() . 'Backend/BookManage/CreateNewBook/';
            }
            $this->AuthorModel->save($author);
            $this->load->view('template/header_backend');
            $this->load->view('backend/author/add_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

    /**
     * add book
     * 1.
     * Check form validate
     * 2. Check upload file success or fail
     * 2.1 if success save to db
     * 2.2 if not
     * 2.2.2 check error that doesn't cause by not select file - True -> show error
     * 2.2.3 error cause by not select file -> set file name = no_cover and save $data to db
     */
    public function add_book() {
        if ($this->form_validation->run('bookForm') == FALSE) {
            $data ['site'] = 'CreateNew';
            $data['title'] = $this->title . ' -> Create new book';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/add_fail', $data);
            $this->load->view('template/footer_backend');
        } else if ($this->upload->do_upload('pic',TRUE )) {
            $upload_data = $this->upload->data();
            $file_name = $upload_data ['file_name'];
            $pic = $file_name;
            $this->saveData($pic);
        } else if ("You did not select a file to upload." != $this->upload->display_errors('', '')) {
            $data ['error'] = array(
                'msg' => $this->upload->display_errors()
            );
            $data['title'] = $this->title . ' -> Create new book';
            $data ['site'] = 'CreateNewBook';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/add_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
            $pic = 'no_cover.png';
            $this->saveData($pic);
        }
    }

    public function saveData($pic) {
        $saveData = array(
            'title' => $this->input->post('title'),
            'cat_id' => $this->input->post('category'),
            'isbn' => $this->input->post('isbn'),
            'detail' => $this->input->post('detail'),
            'author_id' => 7,
            'page' => $this->input->post('page'),
            'price' => $this->input->post('price'),
            'date_add' => date("Y-m-d"),
            'stock' => $this->input->post('stock'),
            'pic' => $pic
        );
        $book_insert_id = $this->BookModel->save($saveData);
        // $book_insert_id = 30;
        $author_list = $this->input->post('author');
        // echo "<pre>"; print_r($author_list); echo "</pre>";
        $author_list = array_unique($author_list);
        $data_for_model = array();

        foreach ($author_list as $author) {
            array_push($data_for_model, array(
                'author_id' => $author,
                'book_id' => $book_insert_id
                )
            );
        }

        $this->WriteModel->save($data_for_model);
        $data['title'] = $this->title . ' -> Create new book';
        $data ['site'] = 'CreateNewBook';
        $data ['book'] = $saveData;
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/book/add_success', $data);
        $this->load->view('template/footer_backend');
    }
    public function updateData($pic) {
        $id = $this->input->post('id');
        $saveData = array(
            'title' => $this->input->post('title'),
            'cat_id' => $this->input->post('category'),
            'isbn' => $this->input->post('isbn'),
            'detail' => $this->input->post('detail'),
            'page' => $this->input->post('page'),
            'price' => $this->input->post('price'),
            'stock' => $this->input->post('stock'),
            'pic' => $pic
        );
        $this->BookModel->update($saveData, $id);
        $this->WriteModel->deleteByBookID($id); // Delete old data

        $author_list = $this->input->post('author');
        // echo "<pre>"; print_r($author_list); echo "</pre>";
        $author_list = array_unique($author_list);
        $data_for_model = array();

        foreach ($author_list as $author) {
            array_push($data_for_model, array(
                'author_id' => $author,
                'book_id' => $id
                )
            );
        }

        $this->WriteModel->save($data_for_model);

        $data['title'] = $this->title . ' -> Edit book';
        $data ['site'] = $this->site;
        $data ['id'] = $id;
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/book/update_success', $data);
        $this->load->view('template/footer_backend');
    }

    public function deleteItem() {
        $id = $this->uri->segment(4);
        $data['title'] = $this->title . ' -> Delete Book';
        $isOrderList = $this->OrderListModel->findByBookID($id);
        if (count($isOrderList) > 0) {
            $data['orderList'] = $isOrderList;
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/delete_detail_restrict', $data);
            $this->load->view('template/footer_backend');
        } else {
            $data['book'] = $this->BookModel->findByID($id);
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/delete_detail', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function delConfirm() {
        $id = $this->uri->segment(4);
        $isOrderList = $this->OrderListModel->findByBookID($id);
        $data['title'] = $this->title . ' -> Delete Book';
        if (count($isOrderList) > 0) {
            $data['orderList'] = $isOrderList;
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/delete_detail_restrict', $data);
            $this->load->view('template/footer_backend');
        } else {
            $this->BookModel->delete($id);
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/book/delete_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

    /**
     * Check logged in
     *
     * @return status
     */
    public function is_logged_in() {
        $loged_in = $this->session->userdata('logged_in');
        return $loged_in['status'];
    }

    /**
     * set data that call from addBook and call Model's save funtion
     *
     * @param [type] $pic
     */
}
