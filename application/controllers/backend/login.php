<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends CI_Controller {
    private $site = '/BookStore/Backend/Login/';
    private $title = 'เข้าสู่ระบบของพนักงาน -> สร้างบัญชีผู้ใช้ของพนักงานใหม่';

    public function __construct() {
        parent::__construct();
        $this->load->model(
            array(
                'backend/LoginModel',
                'backend/StaffModel',
            ));
        $this->load->helper(array('form', 'url'));
        $this->load->library(array('table', 'form_validation'));
    }

    public function index() {
        $this->load->view('backend/login/login_form');
    }

    public function submit() {
        if ($this->form_validation->run('staffLogin') == FALSE) {
            $data['msg'] = '';
            $this->load->view('backend/login/login_form', $data);
        } else {
            $user = $this->input->post('username');
            $pass = $this->input->post('password');
            $passc = $this->input->post('passcode');
            $rs = $this->LoginModel->login($user, $pass, $passc);
            $login = array();
            if (count($rs) == 1) {
                foreach ($rs as $row) {
                    $login = array(
                        'status' => TRUE,
                        'id' => $row['id'],
                        'username' => $row['username'],
                        'name' => $row['name'].' '.$row['surname'],
                        'address' => $row['address'],
                        'telNo' => $row['telNo'],
                        'email' => $row['email'],
                        'personal_id' => $row['personal_id'],
                        'position' => $row['position']
                        );
                }
                $this->session->set_userdata('logged_in',$login);
                redirect(site_url() . 'Backend/BookManage', 'refresh');
            } else {
                $data['msg'] = 'ไม่พบข้อมูลหรือ Username , รหัสผ่าน ไม่ตรงกัน';
                $this->load->view('backend/login/login_form', $data);

            }
        }
    }

    public function logout() {
        $this->session->unset_userdata('logged_in');
//        session_destroy();
        redirect(site_url() . 'Backend/Login', 'refresh');
    }

    public function show_create_account_form()
    {
        $data['site'] = $this->site . 'signup/submit';
        $data['title'] = $this->title;
        $this->load->view('backend/login/create_account',$data);
    }

    public function check_personal_id()
    {
        $data['title'] = $this->title;
        $data['site'] = $this->site . 'signup/submit';

        $this->form_validation->set_rules('personal_id', 'รหัสประจำตัวประชาชน', 'required|numeric');
        if($this->form_validation->run() === FALSE)
        {
            $data['msg'] = validation_errors();
            if( $data['msg'] === NULL OR $data['msg'] == '')
            {
                redirect("/Backend/Login","refresh");
                exit();
            }
            $this->load->view('backend/login/create_account',$data);
        }
        else
        {
            $pid = $this->input->post('personal_id');
            $staff = $this->StaffModel->findByPID($pid);
            if($staff != NULL)
            {
                if($staff['username'] === NULL OR $staff['username'] === '')
                {
                    $data['site'] = $this->site . 'signup/step2/submit';
                    $data['staff'] = $staff;
                    $this->load->view('backend/login/create_account_step2',$data);
                }
                else
                {
                    $data['msg'] = 'รหัสประจำตัวประชาชนนี้ได้กำหนดบัญชีผู้ใช้แล้ว';
                    $this->load->view('backend/login/create_account',$data);
                }
            }
            else
            {
                $data['msg'] = 'ไม่พบข้อมูลตามรหัสบัตรประชาชน กรุณาติดต่อผู้จัดการ';
                $this->load->view('backend/login/create_account',$data);
            }
        }
    }

    public function confirm_signup()
    {
        $data['title'] = $this->title;
        $id = $this->input->post('personal_id');
        $data['site'] = $this->site .'signup/step2/submit';

        $id = $this->input->post('id');
        if($this->form_validation->run('staffsignup') === FALSE )
        {
            $data['msg'] = validation_errors();
            if( $data['msg'] === NULL OR $data['msg'] == '')
            {
                redirect("/Backend/Login","refresh");
                exit();
            }
            $data['staff'] = $this->StaffModel->findByID($id);
            $this->load->view('backend/login/create_account_step2',$data);
        }
        else
        {
            $update_data = array(
                'username' => $this->input->post('username'),
                'password' => $this->input->post('password'),
                'passcode' => $this->input->post('passcode')
                );
            if($update_data['password'] === $this->input->post('confirm_password') &&
                $update_data['passcode'] === $this->input->post('confirm_passcode') )
            {
                $data['site'] = $this->site;
                $this->StaffModel->update($update_data,$id);
                $data['staff'] = $this->StaffModel->findByID($id);
                $this->load->view('backend/login/signup_success',$data);
            }
            else
            {
                $data['msg'] = 'รหัสยืนยันไม่ตรงกัน กรุณาลองใหม่อีกครั้ง';
                $data['staff'] = $this->StaffModel->findByID($id);
                $this->load->view('backend/login/create_account_step2',$data);
            }
        }
    }
//
}
