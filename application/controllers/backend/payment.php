<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller {

	private $title = 'Staff Tools -> การจัดการข้อมูลใบสั่งซื้อ';
	private $site = '/BookStore/Backend/PaymentManage/';

	function __construct() {
		parent::__construct();
		$this->load->model ( array (
			'backend/OrderModel',
			'backend/PaymentModel',
			'backend/OrderListModel',
			'backend/StatusModel',
			)
		);
	}

	function index() {
		$data['title'] = $this->title;
		$data['site'] = $this->site;
        // Set pagination
		$this->pagination->initialize(array(
			'base_url' => base_url() . '/Backend/PaymentManage',
			'total_rows' => $this->db->count_all('tbl_payment')
			));
        /*
         * List book
         * 1 parameter per_page value at config/pagination.php
         * 2 uri segment
         */
        $data['paymentList'] = $this->PaymentModel->listAll($this->config->item('per_page'), $this->uri->segment(4));
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/payment/payment_index', $data);
        $this->load->view('template/footer_backend');
	}

	public function show_payment_detail()
	{
		$data['title'] = $this->title;
		$data['site'] = $this->site;
		$id = $this->uri->segment(4);
		$result_payment = $this->PaymentModel->findByID($id);
		if($result_payment===NULL)
		{
			$this->load->view('template/header_backend', $data);
			$this->load->view('backend/payment/payment_not_found', $data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$order_id = $result_payment['order_id'];
			$result_order = $this->OrderModel->findByIDJoin($order_id);
			foreach ($result_order as $rs) {
                $data['order'] = array(
                    'id' => $rs['order_id'],
                    'order_date' => $rs['order_date'],
                    'status_id' => $rs['status_id'],
                    'status' => $rs['status'],
                );
                $data['member'] = array(
                    'name' => $rs['name'],
                    'surname' => $rs['surname'],
                    'address' => $rs['address'],
                    'telNo' => $rs['telNo'],
                    'email' => $rs['email'],
                );
                $data['sendinfo'] = array(
                    'name' => $rs['sendinfo_name'],
                    'surname' => $rs['sendinfo_surname'],
                    'address' => $rs['sendinfo_address'],
                    'telNo' => $rs['sendinfo_tel'],
                );
            }
            $data['payment'] = $result_payment;
			$data['total'] = $this->OrderListModel->get_sub_total($order_id);

			$this->load->view('template/header_backend', $data);
			$this->load->view('backend/payment/payment_detail', $data);
			$this->load->view('template/footer_backend');
		}
	}

	public function show_update_status()
	{
		$data['site'] = $this->site;
		$order_id = $this->input->post('order_id');
		$payment_id = $this->input->post('payment_id');
		$status_id = $this->input->post('status_id');
		$statues = $this->StatusModel->listAll();
		$statusList = array();
		foreach ($statues as $status) {
			$statusList[$status['id']] = $status['name'];
		}
		$data['payment'] = array(
			'id' => $payment_id,
			'order_id' => $order_id,
			);
		$data['status_id'] = $status_id;
		$data['statusList'] = $statusList;
		$data['title'] = $this->title . ' -> Update Status';
		$this->load->view('template/header_backend', $data);
		$this->load->view('backend/payment/update_status', $data);
		$this->load->view('template/footer_backend');
	}

	public function update_status()
	{
		$data['site'] = $this->site;
		$data['title'] = $this->title . ' -> Update Status';

        $data['order_id'] = $this->input->post('order_id');
        $data['payment_id'] = $this->input->post('payment_id');
        $update_data = array(
            'status_id' => $this->input->post('status')
        );

        if ( $data['order_id'] == '' OR  $data['order_id'] == NULL OR  $data['order_id'] === 0) {
            $data['error_msg'] = 'เกิดข้อผิดพลาดเนื่องจากพยายามเข้าถึงคำสั่งผิดวิธี';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/payment/update_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
        	$data['site'] .= 'detail/' . $data['payment_id'];

            $this->OrderModel->update($update_data, $data['order_id']);

            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/payment/update_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function show_delete_detail()
    {
    	$data['title'] = $this->title;
		$data['site'] = $this->site . 'deleteDetail/confirm';
		$id = $this->uri->segment(4);
		$result_payment = $this->PaymentModel->findByID($id);
		$order_id = $result_payment['order_id'];
		$data['total'] = $this->OrderListModel->get_sub_total($order_id);
		$data['payment'] = $result_payment;
		$this->load->view('template/header_backend', $data);
		$this->load->view('backend/payment/delete_detail', $data);
		$this->load->view('template/footer_backend');
    }

    public function delete_confirm()
    {
    	$id = $this->input->post('id');
    	$this->PaymentModel->delete($id);
    	$data['title'] = $this->title;
		$data['site'] = $this->site;
    	$this->load->view('template/header_backend', $data);
    	$this->load->view('backend/payment/delete_success', $data);
    	$this->load->view('template/footer_backend');
    }

    public function get_order_detail() {
        $id = $this->input->post('id');
        $result = $this->OrderModel->findByIDJoin($id);
        if (count($result) > 0) {
            foreach ($result as $rs) {
                $data['order'] = array(
                    'id' => $rs['order_id'],
                    'order_date' => $rs['order_date'],
                    'status_id' => $rs['status_id'],
                    'status' => $rs['status'],
                );
                $data['member'] = array(
                    'name' => $rs['name'],
                    'surname' => $rs['surname'],
                    'address' => $rs['address'],
                    'telNo' => $rs['telNo'],
                    'email' => $rs['email'],
                );
                $data['sendinfo'] = array(
                    'name' => $rs['sendinfo_name'],
                    'surname' => $rs['sendinfo_surname'],
                    'address' => $rs['sendinfo_address'],
                    'telNo' => $rs['sendinfo_tel'],
                );
            }
            $data['bookList'] = $this->OrderListModel->getBookFromOrderID($id);
            $this->load->view('backend/payment/order_detail_modal', $data);
        } else {
            $this->load->view('backend/order/detail_error', $data);
        }
    }
}


/**
 * END OF FILE : payment.php
 * location : controller/payment.php
 */