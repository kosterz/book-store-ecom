<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Order extends CI_Controller {

    private $title;
    private $site;

    public function __construct() {
        parent::__construct();
        $this->load->model(array(
            'backend/OrderModel',
            'backend/OrderListModel',
            'backend/StatusModel'
        ));
        $this->load->helper(array(
            'form',
            'url',
            'date',
        ));
        $this->load->library(array(
            'table',
            'pagination'
        ));
        $this->title = 'Staff Tools -> Order Manage';
        $this->site = '/BookStore/Backend/OrderManage/';
    }

    public function index() {
        $rs = $this->OrderModel->listAll($this->config->item('per_page'), $this->uri->segment(3));
        $data['title'] = $this->title;
        $this->pagination->initialize(array(
            'base_url' => base_url() . '/Backend/OrderManage/',
            'total_rows' => $this->db->count_all('tbl_order')
        ));
        $this->table->set_template(array(
            'table_open' => '<table width = "100%" border = "0" class = "table table-striped" id = "center" align = "center">'));
        $this->table->set_heading('ID', 'User', 'Status', 'วันที่สั่งซื้อ', 'เพิ่มเติม');
        if (count($rs) > 0) {

            foreach ($rs as $row) {
                $this->table->add_row($row['id'], $row['member'], $row['status'], $row['order_date'], '  <div class="text-center">
                                        <a href="/BookStore/Backend/OrderManage/orderItem/' . $row['id'] . '">
                                        <button type="button" class="btn-xs btn-primary detail" id="${ordList.id}">
                                        <span class="icon search">&nbsp;รายละเอียด</span>
                                        </button>
                                        </a>
                                         <a href="/BookStore/Backend/OrderManage/updateItem/' . $row['id'] . '">
                                        <button type="button" class="btn-xs btn-info delete" id="${ordList.id}">
                                        <span class="icon check">&nbsp;เปลี่ยนสถานะ</span>
                                        </button>
                                    </a>
                                        </div>');
            }
        } else {
            $this->table->add_row('ไม่มีรายการใบสั่งซื้อ');
        }
        $data['rs'] = $rs;
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/order/order_index', $data);
        $this->load->view('template/footer_backend');
    }

    public function orderItem() {
        $id = $this->uri->segment(4);
        $result = $this->OrderModel->findByIDJoin($id);
        if (count($result) > 0) {
            foreach ($result as $rs) {
                $data['order'] = array(
                    'id' => $rs['order_id'],
                    'order_date' => $rs['order_date'],
                    'status_id' => $rs['status_id'],
                    'status' => $rs['status'],
                );
                $data['member'] = array(
                    'name' => $rs['name'],
                    'surname' => $rs['surname'],
                    'address' => $rs['address'],
                    'telNo' => $rs['telNo'],
                    'email' => $rs['email'],
                );
                $data['sendinfo'] = array(
                    'name' => $rs['sendinfo_name'],
                    'surname' => $rs['sendinfo_surname'],
                    'address' => $rs['sendinfo_address'],
                    'telNo' => $rs['sendinfo_tel'],
                );
            }
            $data['bookList'] = $this->OrderListModel->getBookFromOrderID($id);
            $data['title'] = $this->title . ' -> Detail';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/order/order_detail', $data);
            $this->load->view('template/footer_backend');
        } else {
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/order/detail_error', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function updateItem() {
        $data['site'] = $this->site;
        $id = $this->uri->segment(4);
        $row = $this->OrderModel->findByID($id);
        if (count($row) > 0) {
            $statues = $this->StatusModel->listAll();
            $statusList = array();
            foreach ($statues as $status) {
                $statusList[$status['id']] = $status['name'];
            }
            $data['order'] = array(
                'id' => $row['id'],
                'member_id' => $row['member_id'],
                'status_id' => $row['status_id']
            );

            $data['statusList'] = $statusList;
            $data['title'] = $this->title . ' -> Update Status';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/order/update_status', $data);
            $this->load->view('template/footer_backend');
        } else {
            $data['error_msg'] = 'ไม่พบข้อมูลที่ต้องการ';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/order/update_fail', $data);
            $this->load->view('template/footer_backend');
        }
    }

    public function updateStatus() {
        $id = $this->input->post('id');
        $data['site'] = $this->site;
        $updateData = array(
            'status_id' => $this->input->post('status')
        );

        if ($id == '' OR $id == NULL OR $id == 0) {
            $data['error_msg'] = 'เกิดข้อผิดพลาดเนื่องจากพยายามเข้าถึงคำสั่งผิดวิธี';
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/order/update_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
            $this->OrderModel->update($updateData, $id);
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/order/update_success', $data);
            $this->load->view('template/footer_backend');
        }
    }

}
