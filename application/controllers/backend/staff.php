<?php
/**
 * @author KOsTErZ <nkosterz@gmail.com>
 */
class Staff extends CI_Controller{
	private $title = 'Staff Tools -> Profile';
	private $site = '/BookStore/Backend/StaffManage/';

	public function __construct()
	{
		parent::__construct ();
		$this->load->model ( array (
			'backend/StaffModel',
			'backend/PositionModel',
			'backend/PersonModel',
			) );
		$this->load->helper ( array (
			'form',
			'date',
			)
		);
		$this->load->library ( array (
			'form_validation',
			'unit_test',
			)
		);
	}

	public function index()
	{
		$data['title'] = $this->title;
		$data['site'] = $this->site;
        // Set pagination
		$this->pagination->initialize(array(
			'base_url' => base_url() . '/Backend/StaffManage',
			'total_rows' => $this->db->count_all('tbl_staff')
			));
        /*
         * List book
         * 1 parameter per_page value at config/pagination.php
         * 2 uri segment
         */
        $data['staffList'] = $this->StaffModel->listAll($this->config->item('per_page'), $this->uri->segment(4));
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/staff/staff_index', $data);
        $this->load->view('template/footer_backend');
    }

	public function show_delete_detail()
	{
		$data['title'] = $this->title;
		$data['site'] = $this->site;
		$id = $this->uri->segment(4);
		$staff = $this->StaffModel->findByID($id);

// Check return data ; protect random query url
		if($staff != NULL)
		{
			$data['staff'] = $staff;

			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/staff_delete_detail', $data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/staff_not_found');
			$this->load->view('template/footer_backend');
		}
	}

/**
 * Delete staff
 * 2 option
 * 1. Delete from tbl_person by person_id [cascade] effected to tbl_staff
 * 2. Delete only staff , person's data still store;
 * @return last_query()
 */
	public function delete_confirm()
	{
		$id = $this->input->post('id');
		$pid = $this->input->post('pid');
	// Delete cascade from parent table
		$data['str'] = $this->PersonModel->delete($pid);
		$data['title'] = $this->title;
		$data['site'] = $this->site;

		$this->load->view('template/header_backend',$data);
		$this->load->view('backend/staff/delete_success', $data);
		$this->load->view('template/footer_backend');
		// Delete staff only
		// $this->StaffModel->delete($id);
	}


	public function show_staff_detail()
	{
		$data['title'] = $this->title;
		$data['site'] = $this->site;
		$id = $this->uri->segment(4);
		$staff = $this->StaffModel->findByID($id);
// Check return data ; protect random query url
		if($staff != NULL)
		{
			$result = $this->PositionModel->listAll();
			$positionList = array();
			foreach ($result as $position) {
				$positionList[$position['id']] = $position['name'];
			}
			$data['positionList'] = $positionList;
			$data['staff'] = $staff;

			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/staff_detail', $data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/staff_not_found');
			$this->load->view('template/footer_backend');
		}
	}

/**
 * show_profile()
 * get data from session as profile[]
 * @return [type] [description]
 */
	public function show_profile()
	{
		if($this->is_logged_in()){
			$logged_in = $this->session->userdata ( 'logged_in' );
			$data['profile'] = array(
				'id' => $logged_in['id'],
				'name' => $logged_in['name'],
				'position' => $logged_in['position'],
				'address' => $logged_in['address'],
				'username' => $logged_in['username'],
				'personal_id' => $logged_in['personal_id'],
				'telNo' => $logged_in['telNo'],
				'email' => $logged_in['email'],
			);
			$data['title'] = $this->title;

			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/profile',$data);
			$this->load->view('template/footer_backend');
		}else{
			redirect ( site_url () . 'backend/login', 'refresh' );
		}
	}

	public function create_staff_account()
	{
		$id = $this->input->post('id');
		$post_data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'passcode' => $this->input->post('passcode')
			);

		if($this->input->post('password_confirm')===$post_data['username'] &&
			$this->input->post('passcode_confirm')===$post_data['passcode'])
		{
			$this->StaffModel->update($id);
			// Show Success;
		}
		else
		{
			// Show Error
		}
	}

	/**
	 * Update staff_profile
	 * @return [type] [description]
	 */
	public function update_staff_profile()
	{
		if ($this->form_validation->run ( 'staffChangePWD' ) == FALSE) {
			$data ['site'] = 'Profile';
			$data['title'] = $this->title;
			$this->load->view ( 'template/header_backend' ,$data);
			$this->load->view ( 'backend/staff/update_profile_fail', $data );
			$this->load->view ( 'template/footer_backend' );
		}else if($this->input->post('password') == $this->input->post('conf_pass')
			&& $this->input->post('passcode') == $this->input->post('conf_passcode')){
			$send = array(
				'password' => $this->input->post('password'),
				'passcode' => $this->input->post('passcode')
				);
			$data['title'] = $this->title;
			$this->load->view ( 'template/header_backend' ,$data);
			$this->load->view ( 'backend/staff/update_profile_success', $data );
			$this->load->view ( 'template/footer_backend' );
			$this->StaffModel->update($send, $this->session->userdata('logged_in')['id'] );
		}else{
			$data['not_match'] = 'รหัสยืนยันไม่ตรงกัน กรุณาลองใหม่อีกครั้ง';
			$data['title'] = $this->title;
			$this->load->view ( 'template/header_backend' ,$data);
			$this->load->view ( 'backend/staff/update_profile_fail', $data );
			$this->load->view ( 'template/footer_backend' );
		}
	}

	public function show_add_staff_form()
	{
		$data['site'] = $this->site;
		$result = $this->PositionModel->listAll();
		$positionList = array();
		foreach ($result as $position) {
			$positionList[$position['id']] = $position['name'];
		}
		$data['positionList'] = $positionList;
		$data['title'] = $this->title . ' -> Add new Staff';
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/staff/add_staff', $data);
        $this->load->view('template/footer_backend');
	}

	public function add_staff()
	{
		$data['site'] = $this->site . 'addStaff'; // Set href on error page for back to addStaff page		$data['title'] = $this->title;
		if($this->form_validation->run('add_staff') === FALSE )
		{
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/add_staff_error',$data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$person = array(
				'name' => $this->input->post('name'),
				'surname' => $this->input->post('surname'),
				'address' => $this->input->post('address'),
				'telNo' => $this->input->post('telNo'),
				'email' => $this->input->post('email')
				);
			// Add person to db add returning insert_id
			$id = $this->PersonModel->save($person);
			$data['site'] = $this->site;
			$staff = array(
				'personal_id' => $this->input->post('personal_id'),
				'position_id' => $this->input->post('position'),
				'person_id' => $id // -> from PersonModel->save @return insert_id
				);
			$this->StaffModel->save($staff);
			$data['id'] = $id;
			$data['staff'] = $staff;
			$data['person'] = $person;
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/add_staff_success',$data);
			$this->load->view('template/footer_backend');
		}
	}

	public function show_create_position()
	{
		$data['site']  = $this->site .'createPosition/submit';
		$data['title'] = $this->title;

		$this->load->view('template/header_backend',$data);
		$this->load->view('backend/staff/create_new_position',$data);
		$this->load->view('template/footer_backend');
	}

	public function create_position()
	{
		$data['title'] = $this->title;
		if($this->form_validation->run('form_create_position') == FALSE)
		{
			$data['site']  = $this->site . 'createPosition';
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/create_position_error',$data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$data['site']  = $this->site . 'addStaff';
			$this->PositionModel->save(array('name' => $this->input->post('position')));
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/staff/create_position_success',$data);
			$this->load->view('template/footer_backend');
		}
	}


	public function update_staff() {
        $id = $this->input->post('id');
        $pid = $this->input->post('pid');
        $data ['site'] =  $this->site .'detail/'.$id;
        $data['title'] = $this->title . ' -> แก้ไขรายละเอียดพนักงาน';
        if ($this->form_validation->run('form_edit_staff') == FALSE) {
            $data['id'] = $id;
            $this->load->view('template/header_backend', $data);
            $this->load->view('backend/staff/update_staff_fail', $data);
            $this->load->view('template/footer_backend');
        } else {
        	$person = array(
        		'name' => $this->input->post('name'),
        		'surname' => $this->input->post('surname'),
        		'telNo' => $this->input->post('telNo'),
        		'address' => $this->input->post('address'),
        		'email' => $this->input->post('email')
        		);
        	$staff = array(
        		'personal_id' => $this->input->post('personal_id'),
        		'position_id' => $this->input->post('position')
        		);
        	$this->PersonModel->update($person,$pid);
        	$this->StaffModel->update($staff,$id);

        	$this->load->view('template/header_backend', $data);
            $this->load->view('backend/staff/update_staff_success', $data);
            $this->load->view('template/footer_backend');
        }
    }
	/**
	 * Check logged in
	 *
	 * @return status
	 */
	public function is_logged_in()
	{
		$logged_in = $this->session->userdata ( 'logged_in' );
		return $logged_in['status'];
	}
}


/**
 * END OF FILE : Staff.php
 * Location : controller/staff.php
 */