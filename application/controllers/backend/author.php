<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

 class Author extends CI_Controller {

 	private $title;
 	private $site = '/BookStore/Backend/setting/author';

 	public function __construct() {
 		parent::__construct();
 		$this->load->model(array(
 			'backend/CategoryModel',
 			'backend/BookModel',
 			'backend/AuthorModel'
 			));
 		$this->load->library(array(
 			'form_validation',
 			));
 		$this->title = 'Staff Tools -> การจัดการผู้แต่งหนังสือ';
 	}

     function index() {
     	$data['title'] = $this->title;
        // Set pagination
     	$this->pagination->initialize(array(
     		'base_url' => base_url() . '/Backend/setting/author/page',
     		'total_rows' => $this->db->count_all('tbl_author'),
     		'uri_segment' => 5,
     		'first_url' => '0',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'first_link' => 'ไปหน้าแรก',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'last_link' => 'ไปหน้าสุดท้าย',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><li class="active"><a href="#"">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>'
     		));
        /*
         * List book
         * 1 parameter per_page value at config/pagination.php
         * 2 uri segment
         */
        $data['pagination_author']  = $this->pagination->create_links();
        $data['author_list'] = $this->AuthorModel->list_join($this->config->item('per_page'), $this->uri->segment(5));
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/author/show_author', $data);
        $this->load->view('template/footer_backend');
     }

     public function show_edit_author()
     {
     	$id = $this->uri->segment(4);
     	$data['title'] = $this->title;
     	$data['site'] = $this->site;
     	$author = $this->AuthorModel->findByID($id);
     	$data['id'] = $id;
     	if($author != NULL)
     	{
     		$data['author'] = $author;
     		$this->load->view('template/header_backend', $data);
     		$this->load->view('backend/author/edit_author', $data);
     		$this->load->view('template/footer_backend');
     	}
     	else
     	{
     		$this->load->view('template/header_backend', $data);
     		$this->load->view('backend/author/not_found', $data);
     		$this->load->view('template/footer_backend');
     	}
     }

     public function update_author()
     {
     	$data['title'] = $this->title;
     	$id = $this->input->post('id');
     	if($this->input->post('status') != '1')
     	{
     		$this->load->view('template/header_backend',$data);
     		$this->load->view('backend/access_error', $data);
     		$this->load->view('template/footer_backend');
     	}
     	else if ($this->form_validation->run('authorForm') === FALSE)
     	{

     		$data['site'] = $this->site.'/'.$id;
     		echo $data['site'];
     		$this->load->view('template/header_backend',$data);
     		$this->load->view('backend/author/update_fail', $data);
     		$this->load->view('template/footer_backend');
     	}
     	else
     	{
     		$cate_name = $this->input->post('cate_name');
     		$author = array(
     			'name' => $this->input->post('name'),
     			'surname' => $this->input->post('surname'),
     			'telNo' => $this->input->post('telNo'),
     			'email' => $this->input->post('email'),
     			);
     		$data ['author'] = $author;
     		$data ['site'] = $this->site;
     		$this->AuthorModel->update($author,$id);
     		$this->load->view('template/header_backend',$data);
     		$this->load->view('backend/author/update_success', $data);
     		$this->load->view('template/footer_backend');
     	}
     }

     public function show_delete_confirm()
    {
    	$id = $this->input->post('id');
    	$data['title'] = $this->title;
    	$data['site'] = $this->site;
        if($this->input->post('status') != '1')
        {
        	$this->load->view('template/header_backend',$data);
            $this->load->view('backend/access_error', $data);
            $this->load->view('template/footer_backend');
        }
        else
        {
        	$this->AuthorModel->delete($id);
        	$this->load->view('template/header_backend',$data);
            $this->load->view('backend/author/delete_success', $data);
            $this->load->view('template/footer_backend');
        }
    }
 }
 ?>