<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	private $site = "/BookStore/Backend/MemberManage/";
	private $title = "Staff Tools -> การจัดการข้อมูลสมาชิก";
	
    function __construct() {
        parent::__construct();
        $this->load->model(array(
        	'backend/MemberModel',
        	'backend/PersonModel',
        	)
        );
        // $this->load->helper('Helper Name');
    }

    public function index() {
    	$data['title'] = $this->title;
		$data['site'] = $this->site;
        // Set pagination
		$this->pagination->initialize(array(
			'base_url' => base_url() . '/Backend/MemberManage',
			'total_rows' => $this->db->count_all('tbl_member')
			));
        /*
         * List book
         * 1 parameter per_page value at config/pagination.php
         * 2 uri segment
         */
        $data['memberList'] = $this->MemberModel->listAll($this->config->item('per_page'), $this->uri->segment(4));
        $this->load->view('template/header_backend', $data);
        $this->load->view('backend/member/member_index', $data);
        $this->load->view('template/footer_backend');
    }

    public function show_member_detail(){
    	$data['title'] = $this->title . ' -> ข้อมูลสมาชิก';
		$data['site'] = $this->site;
		$id = $this->uri->segment(4);
		$member = $this->MemberModel->findByID($id);
// Check return data ; protect random query url
		if($member != NULL)
		{
			$data['member'] = $member;

			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/member/member_detail', $data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/member/member_not_found');
			$this->load->view('template/footer_backend');
		}
    }

    public function show_delete_detail()
	{
		$data['title'] = $this->title . ' -> ลบสมาชิก';
		$data['site'] = $this->site;
		$id = $this->uri->segment(4);
		$member = $this->MemberModel->findByID($id);

// Check return data ; protect random query url
		if($member != NULL)
		{
			$data['member'] = $member;

			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/member/member_delete_detail', $data);
			$this->load->view('template/footer_backend');
		}
		else
		{
			$this->load->view('template/header_backend',$data);
			$this->load->view('backend/member/member_not_found');
			$this->load->view('template/footer_backend');
		}
	}

	public function delete_confirm()
	{
		$id = $this->input->post('id');
		$pid = $this->input->post('pid');
	// Delete cascade from parent table
		$data['str'] = $this->PersonModel->delete($pid);
		$data['title'] = $this->title;
		$data['site'] = $this->site;

		$this->load->view('template/header_backend',$data);
		$this->load->view('backend/member/delete_success', $data);
		$this->load->view('template/footer_backend');
		// Delete staff only
		// $this->StaffModel->delete($id);
	}
}
?>