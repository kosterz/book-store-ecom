<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Register extends CI_Controller {
    private $title = 'ร้านขายหนังสือ :: Book Store from heaven';
    private $site = '/BookStore/Register';

    function __construct() {
        parent::__construct();
        $this->load->library(
        	array(
        	"form_validation",
        	)
        );
        $this->load->helper(array(
            'date',
        ));
        $this->load->model(array(
        	"frontend/MemberModel",
        	"frontend/PersonModel",
        	));
    }

    function submit_register_form() {
    	if( $this->form_validation->run('form_register_member') === FALSE
    		OR $this->input->post('password') != $this->input->post('confirm_password') )
    	{
    		if($this->input->post('password') != $this->input->post('confirm_password'))
    		{
    			$data['msg'] = "ข้อมูลช่องยืนยันรหัสผ่านไม่ตรงกัน";
    		}
    		$data['title'] = $this->title;
    		$data['site'] = $this->site;
    		$this->load->view('template/header_frontend', $data );
    		$this->load->view('frontend/register/register_fail' , $data);
    		$this->load->view('template/footer_frontend');
    	}
    	else
    	{
    		$data_for_person = array(
    			"name" => $this->input->post('name'),
    			"surname" => $this->input->post('surname'),
    			"address" => $this->input->post('address'),
    			"telNo" => $this->input->post('tel_number'),
    			"email" => $this->input->post('email')
    			);
    		$person_id = $this->PersonModel->save($data_for_person);
    		$data_for_member = array(
    			"username" => $this->input->post('username'),
    			"password" => $this->encrypt_password($this->input->post('password')),
    			"person_id" => $person_id,
    			"register_date" => date("Y-m-d H:i:s")
    			);
    		$this->MemberModel->save($data_for_member);

    		$data['title'] = $this->title;
    		$data['site'] = $this->site;
    		$this->load->view('template/header_frontend', $data );
    		$this->load->view('frontend/register/register_success' , $data);
    		$this->load->view('template/footer_frontend');
    	}
    }

    public function encrypt_password($password)
	{
		$options = [
		'cost' => 12,
		];
		return password_hash($password, PASSWORD_BCRYPT, $options);
	}
}


 ?>