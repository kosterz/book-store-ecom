<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Member extends CI_Controller {

	private $title = "ร้านขายหนังสือ => ข้อมูลสมาชิก";
	private $site = "/BookStore/member";
    function __construct() {
    	parent::__construct();
         $this->load->model(array(
            'frontend/BookModel',
            'frontend/CategoryModel',
            'frontend/MemberModel',
        ));
         $this->load->library(
         	array(
         		"form_validation",
         		)
         	);
    }

    function index() {
    	if($this->session->userdata('member_logged_in')!= NULL)
        {
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/member/profile', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            redirect('/BookStore', 'refresh');
        }
    }

    public function show_profile()
    {
    	if($this->session->userdata('member_logged_in')!= NULL)
        {
        	$data['member'] = $this->session->userdata('member_logged_in');
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/member/edit_profile', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            redirect('/BookStore', 'refresh');
        }
    }

    public function update_profile()
    {
    	$data['title'] = $this->title;
    	$data['site'] = $this->site;
    	if($this->input->post('status') != 1)
    	{
    		$this->load->view('template/header_frontend', $data);
    		$this->load->view('frontend/access_error', $data);
    		$this->load->view('template/footer_frontend');
    	}
    	else if($this->form_validation->run('form_update_member_profile') === FALSE)
    	{
    		$member = $this->session->userdata('member_logged_in');
    		$data['member'] = $member;
    		$data['carousel'] = $this->BookModel->list_new_book(12);
    		$this->load->view('template/header_frontend', $data);
    		$this->load->view('frontend/member/update_error', $data);
    		$this->load->view('template/footer_frontend');
    	}
    	else
    	{
    		$profile_data = array(
    			'name' => $this->input->post('name'),
    			'surname' => $this->input->post('surname'),
    			'address' => $this->input->post('address'),
    			'telNo' => $this->input->post('telNo'),
    			'email' => $this->input->post('email')
    			);
    		$member_id = $this->session->userdata('member_logged_in')['id'];

    		$member = $this->MemberModel->findByID($member_id);
    		$login = array(
                        'status' => TRUE,
                        'id' => $member['id'],
                        'username' => $member['username'],
                        'name' => $member['name'],
                        'surname' => $member['surname'],
                        'address' => $member['address'],
                        'telNo' => $member['telNo'],
                        'email' => $member['email'],
                        );
    		$this->session->set_userdata('member_logged_in',$login);
    		$member = $this->session->userdata('member_logged_in');
    		$data['member'] = $member;
    		$data['carousel'] = $this->BookModel->list_new_book(12);
    		$this->load->view('template/header_frontend', $data);
    		$this->load->view('frontend/member/update_success', $data);
    		$this->load->view('template/footer_frontend');
    	}
    }

    public function show_change_pw()
    {
        if($this->session->userdata('member_logged_in')!= NULL)
        {
            $data['member'] = $this->session->userdata('member_logged_in');
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/member/change_password', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            redirect('/BookStore', 'refresh');
        }
    }

    public function update_password()
    {
        $data['title'] = $this->title;
        $data['site'] = $this->site;
        $member = $this->session->userdata('member_logged_in');
        $data['member'] = $member;
        $data['carousel'] = $this->BookModel->list_new_book(12);

        if($this->input->post('status') != 1)
        {
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/access_error', $data);
            $this->load->view('template/footer_frontend');
        }
        else if($this->form_validation->run('form_change_password') === FALSE)
        {
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/member/password_update_error', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            $old_pw = $this->input->post('old_pw');
            $new_pw = $this->input->post('new_pw');
            $confirm_pw = $this->input->post('confirm_pw');
            $member_id = $this->session->userdata('member_logged_in')['id'];
            $password = $this->MemberModel->findByID($member_id)['password'];

            if(password_verify($old_pw,$password))
            {
                if($new_pw === $confirm_pw)
                {
                    $this->MemberModel->updatePW($this->encrypt_password($new_pw),$member_id);
                    $this->load->view('template/header_frontend', $data);
                    $this->load->view('frontend/member/change_password_success', $data);
                    $this->load->view('template/footer_frontend');
                }
                else
                {
                    $data['error_msg'] = 'รหัสผ่านใหม่กับยืนยันรหัสผ่านไม่ตรงกัน';
                    $this->load->view('template/header_frontend', $data);
                    $this->load->view('frontend/member/password_update_error', $data);
                    $this->load->view('template/footer_frontend');
                }
            }
            else
            {
                $data['error_msg'] = 'รหัสผ่านไม่ถูกต้อง';
                $this->load->view('template/header_frontend', $data);
                $this->load->view('frontend/member/password_update_error', $data);
                $this->load->view('template/footer_frontend');
            }
        }
    }

    public function encrypt_password($password)
    {
        $options = [
        'cost' => 12,
        ];
        return password_hash($password, PASSWORD_BCRYPT, $options);
    }
}

?>