<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main extends CI_Controller {

    private $title = 'ร้านขายหนังสือ';
    private $site = '/BookStore';

    function __construct() {
        parent::__construct();
         $this->load->model(array(
            'frontend/BookModel',
            'frontend/CategoryModel'
        ));
    }

    public function index() {
        $this->pagination->initialize(array(
            'per_page' => 6,
            'base_url' => base_url() . 'New/page/',
            'total_rows' => $this->BookModel->count_all_row(),
            'uri_segment' => 3,
            'first_url' => '0',
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'first_link' => 'ไปหน้าแรก',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'last_link' => 'ไปหน้าสุดท้าย',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><li class="active"><a href="#"">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>'
            ));
        $data['pagination_new_book'] = $this->pagination->create_links();

        $data['category_list'] = $this->CategoryModel->listAll();
        if($this->input->post('ajax'))
        {
            $data['site'] = $this->site;
            $start = $this->input->post('start');
            $data['book_new'] = $this->BookModel->list_new_book(6, $start);
            $this->load->view('frontend/main/ajax_book_new', $data);
        }
        else
        {
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['book_new'] = $this->BookModel->list_new_book(6, 0);
            $data['book_all'] = $this->BookModel->list_new_book(6, $this->uri->segment(3));
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/main/index', $data);
            $this->load->view('template/footer_frontend');
        }
    }

    public function show_register_form() {
        $data['title'] = $this->title;
        $data['site'] = $this->site . '/Register/submit';
        $this->load->view('template/header_frontend',$data);
        $this->load->view('frontend/register/register', $data);
        $this->load->view('template/footer_frontend',$data);
    }
}

?>