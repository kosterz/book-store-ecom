<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Order extends CI_Controller {

	private $title = "ร้านขายหนังสือ => ข้อมูลสมาชิก";
	private $site = "/BookStore/member";
	function __construct() {
		parent::__construct();
		$this->load->model(array(
			'frontend/BookModel',
			'frontend/CategoryModel',
			'frontend/MemberModel',
			'frontend/OrderModel',
			));
		$this->load->library(
			array(
				"form_validation",
				)
			);
	}

    function index() {
    	$member = $this->session->userdata('member_logged_in');
    	if($member != NULL)
        {
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $data['order_list'] = $this->OrderModel->list_all($member['id']);
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/order/order_history', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            redirect('/BookStore', 'refresh');
        }
    }
}

 ?>