<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends CI_Controller
{
	private $title = 'ร้านขายหนังสือ';
	private $site = "/BookStore/";
    function __construct()
    {
        parent::__construct();
        $this->load->model(array(
        	"frontend/BookModel",
            "frontend/OrderModel",
            "frontend/SendinfoModel",
        	));
        $this->load->library(
            array(
            "form_validation",
            )
        );
    }

    function index()
    {
        $data['carousel'] = $this->BookModel->list_new_book(12);
        $data['cart'] =  $this->cart->contents();
        $data['title'] = $this->title;
        $data['site'] = $this->site;
        $this->load->view('template/header_frontend', $data);
        $this->load->view('frontend/cart/show_cart', $data);
        $this->load->view('template/footer_frontend');
    }

    function add_product()
    {
    	$id = $this->input->post('id');
        $found = FALSE;
        $cart_content = $this->cart->contents();
        foreach ($cart_content as $item) {
            if($item['id'] === $id)
            {
                $data_cart_update = array(
                    'rowid' => $item['rowid'],
                    'qty' => $item['qty'] + 1,
                    );
                $this->cart->update($data_cart_update);
                $found = TRUE;
                break;
            }
        }

        if($found === FALSE)
        {
            $book = $this->BookModel->findByID($id);
            $data_cart = array(
            'id'      => $book['id'],
            'qty'     => 1,
            'price'   => $book['price'],
            'name'    => $book['title'],
            'options' => array('pic' => $book['pic']),
            );
            $this->cart->insert($data_cart);
        }

    // Check Is it come from AJAX;
        if($this->input->post('ajax'))
        {
            $this->load->view('frontend/cart/cart_info');
        }
        else
        {
            $this->cart->update($data_cart);
            redirect('/', 'refresh');
        }
    }

    function update_qty()
    {
            // Check where POST DATA come from ;
        if($this->input->post('ajax'))
        {
            $row_id = $this->input->post('row_id');
            $qty = $this->input->post('qty');
            $data_cart =  array(
               'rowid'   => $row_id,
               'qty'     => floor($qty),
               );
            $this->cart->update($data_cart);
            $data['cart'] =  $this->cart->contents();
            $this->load->view('frontend/cart/ajax_data', $data);
        }
        else
        {
            if($this->form_validation->run('form_update_qty') === FALSE)
            {
                $data['carousel'] = $this->BookModel->list_new_book(12);
                $data['cart'] =  $this->cart->contents();
                $data['title'] = $this->title;
                $data['site'] = $this->site;
                $this->load->view('template/header_frontend', $data);
                $this->load->view('frontend/cart/update_fail', $data);
                $this->load->view('template/footer_frontend');
            }
            else
            {
                $row_id = $this->input->post('row_id');
                $qty = $this->input->post('qty');
                $data_cart =  array(
                 'rowid'   => $row_id,
                 'qty'     => floor($qty),
                 );
                $this->cart->update($data_cart);
                redirect('/cart', 'refresh');
            }
        }
    }


    function delete_item()
    {
        $row_id = $this->input->post('row_id');
        $qty = 0;
        $data_cart =  array(
         'rowid'   => $row_id,
         'qty'     => $qty,
         );
        if($this->input->post('ajax'))
        {
            $this->cart->update($data_cart);
            $data['cart'] =  $this->cart->contents();
            $this->load->view('frontend/cart/ajax_data', $data);
        }
        else
        {
            $this->cart->update($data_cart);
            redirect('/cart', 'refresh');
        }
    }

    function show_sending_info()
    {
        $data['title'] = $this->title;
        $data['carousel'] = $this->BookModel->list_new_book(12);
        $data['member'] = $this->session->userdata('member_logged_in');
        $data['site'] = '/BookStore/cart';
        if($this->input->post('status') != '1')
        {
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/access_error', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/cart/confirm_order_step_1', $data);
            $this->load->view('template/footer_frontend');
        }

    }

    function summary_order()
    {
        $data['title'] = $this->title;
        $data['carousel'] = $this->BookModel->list_new_book(12);
        $data['site'] = '/BookStore/cart';
        if($this->input->post('status') != '2')
        {
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/access_error', $data);
            $this->load->view('template/footer_frontend');
        }
        else if($this->form_validation->run('form_sending_info') === FALSE)
        {
           $this->load->view('template/header_frontend', $data);
           $this->load->view('frontend/cart/sending_info_fail');
           $this->load->view('template/footer_frontend');
        }
        else
        {
            $data['sendingInfo'] = array(
                'name' => $this->input->post('name'),
                'surname' => $this->input->post('surname'),
                'address' => $this->input->post('address'),
                'telNo' => $this->input->post('tel_number')
                );
            $data['cart'] =  $this->cart->contents();
           $this->load->view('template/header_frontend', $data);
           $this->load->view('frontend/cart/confirm_order_step_2', $data);
           $this->load->view('template/footer_frontend');
        }
    }


    function confirm_order()
    {
        if($this->input->post('status') != '3')
        {
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/access_error', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            $member = $this->session->userdata('member_logged_in');
            $member_id = $member['id'];
            $cart = $this->cart->contents();
            $order_id = $this->OrderModel->save($member_id);
            $order_list = array();
            foreach ($cart as $book)
            {
                array_push($order_list, array(
                    'order_id' => $order_id,
                    'book_id' => $book['id'],
                    'quantity' => $book['qty'],
                    'price' => $book['price'] / $book['qty'],
                    )
                );
            }

            $sending_info = array(
                'name' => $this->input->post('name'),
                'surname' => $this->input->post('surname'),
                'address' => $this->input->post('address'),
                'telNo' => $this->input->post('telNo'),
                'order_id' => $order_id,
                );


            $this->OrderModel->save_order_list($order_list);
            $this->SendinfoModel->save($sending_info);
            $this->cart->destroy();

            $data['title'] = $this->title;
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['site'] = '/BookStore/';
            $this->load->view('template/header_frontend', $data);
            $this->load->view('frontend/cart/checkout_success', $data);
            $this->load->view('template/footer_frontend');
        }

    }
}
?>