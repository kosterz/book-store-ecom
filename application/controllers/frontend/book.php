<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Book extends CI_Controller {

	private $title = 'ร้านขายหนังสือ';
	private $site = '/BookStore';

	function __construct() {
		parent::__construct();
		$this->load->model(array(
			'frontend/BookModel',
			'frontend/CategoryModel'
			));
        $this->load->library(array(
            'form_validation',
        ));
	}

    public function show_detail()
    {
    	if($this->input->post('ajax'))
    	{

    	}
    	else
    	{
    		$id = $this->uri->segment(3);
    		$data['category_list'] = $this->CategoryModel->listAll();
    		$data['carousel'] = $this->BookModel->list_new_book(12);
    		$data['title'] = $this->title;
    		$data['site'] = $this->site;
    		$data['book'] = $this->BookModel->findByID($id);
    		$data['author'] = $this->BookModel->findAuthorByID($id);
            // echo '<pre>';
            // print_r($data['book']);
    		$data['cate_name'] = $data['book']['category'];
    		$this->load->view('template/header_frontend', $data);
    		$this->load->view('frontend/book/show_detail', $data);
    		$this->load->view('template/footer_frontend');
    	}
    }

    public function show_by_category()
    {
        $id = $this->uri->segment(3);
        $row = $this->BookModel->count_row($id);
        $base =  base_url() . 'show/category/'.$id.'/page/';
        $this->pagination->initialize(array(
            'per_page' => 6,
            'base_url' => $base,
            'total_rows' => $row,
            'uri_segment' => 5,
            'first_url' => 0,
            'first_tag_open' => '<li>',
            'first_tag_close' => '</li>',
            'first_link' => 'ไปหน้าแรก',
            'last_tag_open' => '<li>',
            'last_tag_close' => '</li>',
            'last_link' => 'ไปหน้าสุดท้าย',
            'next_tag_open' => '<li>',
            'next_tag_close' => '</li>',
            'prev_tag_open' => '<li>',
            'prev_tag_close' => '</li>',
            'cur_tag_open' => '<li class="active"><li class="active"><a href="#"">',
            'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
            'num_tag_open' => '<li>',
            'num_tag_close' => '</li>',
            ));
        $data['pagination_new_book'] = $this->pagination->create_links();

        $data['category_list'] = $this->CategoryModel->listAll();
        if($this->input->post('ajax'))
        {
            $data['site'] = $this->site;
            $start = $this->input->post('start');
            $data['book_new'] = $this->BookModel->list_new_book(6, $start);
            $this->load->view('frontend/main/ajax_book_new', $data);
        }
        else
        {
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['book_new'] = $this->BookModel->list_by_category(6, 0,$id);
            $data['book_all'] = $this->BookModel->list_by_category(6, $this->uri->segment(5),$id);
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/main/index', $data);
            $this->load->view('template/footer_frontend');
        }
    }

    public function show_by_search()
    {
       $find_from = $this->input->get('search_option');
       $search_text = $this->input->get('search_text');
        if($search_text == '' OR $search_text == NULL)
        {
            $data['category_list'] = $this->CategoryModel->listAll();
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/book/search_error', $data);
            $this->load->view('template/footer_frontend');
        }
        else
        {
            $find_from = $this->input->get('search_option');
            $search_text = $this->input->get('search_text');
            switch ($find_from) {
                case '1':
                    $find_from = 'tbl_book.title';
                    break;
                case '2':
                    $find_from = 'isbn';
                    break;
                case '3':
                    $find_from = 'tbl_author.name';
                    break;
                default:
                    $find_from = 'detail';
            }
            $row = $this->BookModel->count_search_row($find_from,$search_text); // นับจำนวน row เพื่อทำไปสร้าง pagination

            if($row != NULL)
            { // สร้างตัวแปรเก็บผลลัพท์
                $start = 0;
                if($this->input->get('page'))
                {
                    $start = ($this->input->get('page')-1) * 6;
                }
                $result = $this->BookModel->search($find_from,$search_text,6,$start);
            }
            else
            {
                $result = NULL;
                $row = 0;
            }
            $base =  base_url() . 'search?search_option='.$this->input->get('search_option').'&search_text='.$search_text;
            $this->pagination->initialize(array(
                'per_page' => 6,
                'base_url' => $base,
                'total_rows' => $row,
                'page_query_string' => TRUE,
                'query_string_segment' => 'page',
                'use_page_numbers' => TRUE,
                'first_url' => $base.'&page=1',
                'first_tag_open' => '<li>',
                'first_tag_close' => '</li>',
                'first_link' => 'ไปหน้าแรก',
                'last_tag_open' => '<li>',
                'last_tag_close' => '</li>',
                'last_link' => 'ไปหน้าสุดท้าย',
                'next_tag_open' => '<li>',
                'next_tag_close' => '</li>',
                'prev_tag_open' => '<li>',
                'prev_tag_close' => '</li>',
                'cur_tag_open' => '<li class="active"><li class="active"><a href="#"">',
                'cur_tag_close' => '<span class="sr-only">(current)</span></a></li>',
                'num_tag_open' => '<li>',
                'num_tag_close' => '</li>'
                ));
            $data['row'] = $row;
            $data['search_text'] = $search_text;
            $data['pagination_search'] = $this->pagination->create_links();
            $data['category_list'] = $this->CategoryModel->listAll();
            $data['carousel'] = $this->BookModel->list_new_book(12);
            $data['search_result'] = $result;
            $data['title'] = $this->title;
            $data['site'] = $this->site;
            $this->load->view('template/header_frontend',$data);
            $this->load->view('frontend/book/search_result', $data);
            $this->load->view('template/footer_frontend');
        }
    }
}
?>// END OF FILE book.php
