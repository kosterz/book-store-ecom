<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
    private $title = 'ร้านขายหนังสือ';
    private $site = '/BookStore/login';

    function __construct() {
        parent::__construct();
        $this->load->model(array(
        	"frontend/MemberModel",
        	"frontend/PersonModel",
        	));
        $this->load->library(
        	array(
        	"form_validation",
        	)
        );
    }

    function submit_login()
    {
    	if($this->form_validation->run('form_member_login') === FALSE)
    	{
    		$data['site'] = $this->site;
    		$data['title'] = $this->title;
    		$this->load->view('template/header_frontend', $data);
    		$this->load->view('frontend/login/login_fail',$data);
    		$this->load->view('template/footer_frontend');
    	}
    	else
    	{
    		$username = $this->input->post('username');
    		$password = $this->input->post('password');

    		$member = $this->MemberModel->findByUser($username);
    		if($member != NULL)
    		{
    			if(password_verify($password, $member['password']))
    			{
                    $login = array(
                        'status' => TRUE,
                        'id' => $member['id'],
                        'username' => $member['username'],
                        'name' => $member['name'],
                        'surname' => $member['surname'],
                        'address' => $member['address'],
                        'telNo' => $member['telNo'],
                        'email' => $member['email'],
                        );
                    $this->session->set_userdata('member_logged_in',$login);
                    $data['site'] = $this->site;
                    $data['title'] = $this->title;
                    $this->load->view('template/header_frontend', $data);
                    $this->load->view('frontend/login/login_success',$data);
                    $this->load->view('template/footer_frontend');
                }
    			else
    			{
                    $data['msg'] = 'ชื่อผู้ใช้งานกับรหัสผ่านไม่ตรงกัน';
                    $data['site'] = $this->site;
                    $data['title'] = $this->title;
                    $this->load->view('template/header_frontend', $data);
                    $this->load->view('frontend/login/login_fail',$data);
                    $this->load->view('template/footer_frontend');
    			}
    		}
    		else
    		{
                    $data['msg'] = 'ไม่พบชื่อผู้ใช้งานนี้ในระบบ';
                    $data['site'] = $this->site;
                    $data['title'] = $this->title;
                    $this->load->view('template/header_frontend', $data);
                    $this->load->view('frontend/login/login_fail',$data);
                    $this->load->view('template/footer_frontend');
    		}
    	}
    }

    public function logout()
    {
        $this->session->unset_userdata('member_logged_in');
        redirect('/', 'refresh');
    }
}

?>