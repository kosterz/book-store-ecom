<?php
	if (!defined('BASEPATH')) exit('No direct script access allowed');

	class Payment extends CI_Controller {

		private $title = 'ร้านขายหนังสือ';
		private $site = "/BookStore/payment";
		function __construct()
		{
			parent::__construct();
			$this->load->model(array(
				"frontend/BookModel",
				"frontend/OrderModel",
				"frontend/PaymentModel",
				));
			$this->load->library(
				array(
					"form_validation",
					)
				);
		}

		public function index()
		{
			$data['title'] = $this->title;
			$data['site'] = $this->site;

			if($this->session->userdata('member_logged_in') == NULL)
			{
				$data['carousel'] = $this->BookModel->list_new_book(12);
				$this->load->view('template/header_frontend', $data);
				$this->load->view('frontend/payment/not_login', $data);
				$this->load->view('template/footer_frontend');
			}
			else
			{
				$member = $this->session->userdata('member_logged_in');
				$order_list = $this->OrderModel->list_order_not_pay($member['id']);

				if($order_list == NULL)
				{
					$data['member'] = $member;
					$data['carousel'] = $this->BookModel->list_new_book(12);
					$this->load->view('template/header_frontend', $data);
					$this->load->view('frontend/payment/order_not_found', $data);
					$this->load->view('template/footer_frontend');
				}
				else
				{
					$orderList = array();
					foreach ($order_list as $order) {
						$orderList[$order['id']] = "รหัสใบสั่งซื้อ : ".$order['id'] . '          ราคารวม : ' . $order['total'];
					}
					$data['order_list'] = $orderList;
					$data['member'] = $member;
					$data['carousel'] = $this->BookModel->list_new_book(12);
					$this->load->view('template/header_frontend', $data);
					$this->load->view('frontend/payment/index', $data);
					$this->load->view('template/footer_frontend');
				}
			}
		}

		public function confirm_payment()
		{
			$data['title'] = $this->title;
			$data['site'] = $this->site;
			if($this->input->post('status') != '1')
			{
				$this->load->view('template/header_frontend', $data);
				$this->load->view('frontend/access_error', $data);
				$this->load->view('template/footer_frontend');
			}
			else if($this->form_validation->run('form_payment_info') === FALSE)
			{
				$member = $this->session->userdata('member_logged_in');
				$data['member'] = $member;
				$data['carousel'] = $this->BookModel->list_new_book(12);
				$this->load->view('template/header_frontend', $data);
				$this->load->view('frontend/payment/payment_error', $data);
				$this->load->view('template/footer_frontend');
			}
			else
			{
				$payment_info = array(
					'order_id' => $this->input->post('order_id'),
					'bank' => $this->input->post('bank_name'),
					'amount' => $this->input->post('amount'),
					'datetime_payment' => date('Y-m-d H:i',strtotime($this->input->post('datetime_payment')))
					);
				$this->PaymentModel->save($payment_info);
				$member = $this->session->userdata('member_logged_in');
				$data['payment_info'] = $payment_info;
				$data['member'] = $member;
				$data['carousel'] = $this->BookModel->list_new_book(12);
				$this->load->view('template/header_frontend', $data);
				$this->load->view('frontend/payment/payment_success', $data);
				$this->load->view('template/footer_frontend');
			}
		}
	}

?>