<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');

class Test extends CI_Controller {
	private $sql = 'tbl_staff.id, username, position_id, tbl_position.name AS position , tbl_person.name,
        surname, address, telNo, email';

	function __construct() {
		parent::__construct();
		$this->load->library('unit_test');
		$this->load->model('backend/StaffModel');
		$this->load->model(array(
			"frontend/BookModel",
			));
		// $this->db->select ( $this->sql );
		// $this->db->join ( 'tbl_person', 'tbl_staff.person_id=tbl_person.id', 'inner' );
		// $this->db->join ( 'tbl_position', 'tbl_staff.position_id=tbl_position.id', 'inner' );
	}

	function index() {
		$this->load->library('pagination');

		$config['base_url'] = base_url() . '/test/index/';
		$config['total_rows'] = 200;
		$config['per_page'] = 20;
		$this->pagination->initialize($config);
		echo $this->pagination->create_links();
	}


	function generateRandomPersonalID($length = 13) {
		$characters = '0123456789';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	function generateRandomString($length = 10) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $randomString;
	}

	public function save($data){
		$find = $this->db->where('personal_id',$data['staff']['personal_id'])->get('tbl_staff');
		if($find->num_rows() > 0)
		{
			return FALSE;
		}
		$this->db->insert('tbl_person',$data['person']);
		$data['staff']['person_id'] = $this->db->insert_id();
		$this->db->insert('tbl_staff',$data['staff']);
		return TRUE;
	}



}
