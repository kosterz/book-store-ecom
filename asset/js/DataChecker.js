var flagUser;
var flagPass;

function visibleControl(name, status) {
    if (status == "show") {
        // alert("name : "+name+"\nstatus : " + status);
        document.getElementById(name).style.visibility = "visible";
    } else if (status == "hide") {
        // alert("name : "+name+"\nstatus : " + status);
        document.getElementById(name).style.visibility = "hidden";
    }
}

function checkEmpty(inputField, parentNode) {
    if (inputField.value.length === 0) {
        var str = "กรุณากรอกข้อมูลด้วยค่ะ";
        document.getElementById(parentNode).innerHTML = str.fontcolor("red");
        document.getElementById(inputField).focus();
    } else if (parentNode !== "stockNotice" &&
            parentNode !== "priceNotice") {
        document.getElementById(parentNode).innerHTML = "";
    }
}

function checkValid(inputField, innerCase, innerID, inputID) {
    switch (innerCase) {
        case 1:
            var pattern = /[^0][0-9]/;
            if (!pattern.test(inputField.value)) {
                var str = "เป็นตัวเลขเท่านั้นคะ";
                document.getElementById(innerID).innerHTML = str.fontcolor('red');
                document.getElementById(inputID).focus();
//                window.setInterval(function() {
//                    document.getElementById(inputID).focus();
//                }, 1200);
            } else {
                document.getElementById(innerID).innerHTML = "";
//                var imgsrc = document.createElement("img");
//                imgsrc.setAttribute("src", "src/img/right.png");
//                document.getElementById("orderID").appendChild(imgsrc);
            }
            break;
        case 2:
            alert("case 2");
            var pattern = /.{5}/;
            if (!pattern.test(inputField.value)) {
                document.getElementById("innerBank").innerHTML = "เป็นตัวหนังสือเท่านั้นคะ";
            } else {
                document.getElementById("innerBank").innerHTML = "";
                var imgsrc = document.createElement("img");
                imgsrc.setAttribute("src", "src/img/right.png");
                document.getElementById("bankID").appendChild(imgsrc);

            }

            break;

        default:
            alert("default");

    }

}

function validateUsername(textField, innerID) {
    var pattern = /^[a-zA-Z]+[0-9]*$/;
    if (!pattern.test(textField.value)) {
        document.getElementById(innerID).innerHTML = "รูปแบบไม่ถูกต้อง";
        flagUser = false;
    } else {
        document.getElementById(innerID).innerHTML = "รูปแบบถูกต้อง";
        flagUser = true;
    }
}

function validatePassword(textField, innerID) {
    var pattern = /^\w{8,16}$/; // ตรวจว่าเป็นตัวหนังสือหรือตัวเลขจำนวน 8 - 16 ตัว
    if (!pattern.test(textField.value)) {
        document.getElementById(innerID).innerHTML = "รูปแบบไม่ถูกต้อง";
        flagPass = false;
    } else {
        document.getElementById(innerID).innerHTML = "รูปแบบถูกต้อง";
        flagPass = true;
    }
}

function radioProduct(name) {

    if (document.getElementById('r_insertProduct').checked) {
        var newChild = document.getElementById("addProductForm");
        var oldChild = document.getElementById("editProductForm");
        oldChild.style.visibility = "hidden";
        newChild.style.visibility = "visible";
    } else {
        var newChild = document.getElementById("editProductForm");
        var oldChild = document.getElementById("addProductForm");
        oldChild.style.visibility = "hidden";
        newChild.style.visibility = "visible";
    }
}

function addChild() {
    var list = document.createElement("li");
    var a = document.createElement("a");
    var txt = document.createTextNode("หน้าหลัก");

    a.appendChild(txt);
    a.title = "หน้าหลัก";
    a.href = "admin.php";
    list.appendChild(a);
    var parent = document.getElementById("adminMenu");
    parent.insertBefore(list, parent.childNodes[0]);
}
